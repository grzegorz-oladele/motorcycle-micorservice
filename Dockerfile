FROM amazoncorretto:17-alpine
EXPOSE 8200
WORKDIR /app
COPY motorcycle-service-server/target/motorcycle-service-server-0.0.1-SNAPSHOT.jar /app/motorcycle-service-server-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "motorcycle-service-server-0.0.1-SNAPSHOT.jar"]