package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleadapters.helper.MotorcycleApplicationHelper;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import static org.mockito.Mockito.*;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleSetUnactiveCommandPublisherAdapterTest {

    @InjectMocks
    private MotorcycleSetUnactiveCommandAdapter motorcycleSetUnactiveCommandAdapter;
    @Mock
    private MotorcycleApplicationHelper motorcycleApplicationHelper;
    @Mock
    private MotorcycleRepository motorcycleRepository;
    @Mock
    private MotorcycleMapper motorcycleMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleEntity = motorcycleEntity();
    }

    @Test
    void shouldSetMotorcycleAsUnactiveAndCallCheckBikerPermissionMethodAndSaveMethodAndPublishAllMethod() {
//        given
        var bikerId = BIKER_ID.toString();
        doNothing().when(motorcycleApplicationHelper).checkBikerPermission(bikerId, motorcycleAggregate);
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleSetUnactiveCommandAdapter.setUnactive(motorcycleAggregate);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }
}