package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleadapters.MotorcycleFixtures;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MotorcycleMapEngineCommandAdapterTest {

    @InjectMocks
    private MotorcycleMapEngineCommandAdapter motorcycleMapEngineCommandAdapter;
    @Mock
    private MotorcycleMapper motorcycleMapper;
    @Mock
    private MotorcycleRepository motorcycleRepository;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = MotorcycleFixtures.motorcycleAggregate();
        motorcycleEntity = MotorcycleFixtures.motorcycleEntity();
    }

    @Test
    void shouldCallSaveMethodOnMotorcycleRepositoryInterface() {
//        given
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleMapEngineCommandAdapter.map(motorcycleAggregate);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }
}