package pl.grzegorz.motorcycleadapters.out.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleCreatedEvent;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleAggregate;

@ExtendWith(MockitoExtension.class)
class MotorcycleCreateCommandPublisherAdapterTest {

    @InjectMocks
    private MotorcycleCreateCommandPublisherAdapter motorcycleCreateCommandPublisherAdapter;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.create.exchange}")
    private String motorcycleCreateExchange;
    @Value("${spring.rabbitmq.motorcycle.create.routing-key}")
    private String motorcycleCreateRoutingKey;

    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallConvertAndSendOnRabbitTemplateClass() {
//        given
//        when
        motorcycleCreateCommandPublisherAdapter.publish(motorcycleAggregate);
//        then
        verify(rabbitTemplate).convertAndSend(eq(motorcycleCreateExchange), eq(motorcycleCreateRoutingKey),
                any(MotorcycleCreatedEvent.class));
    }
}