package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleOwnerUpdateStatusAggregate;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleOwnerUpdateStatusEntity;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnershipExchangeProcessInitCommandAdapterTest {

    @InjectMocks
    private MotorcycleOwnerUpdateInitProcessAdapter motorcycleOwnershipExchangeProcessInitCommandAdapter;
    @Mock
    private MotorcycleOwnerUpdateStatusMapper motorcycleOwnerUpdateStatusMapper;
    @Mock
    private MotorcycleOwnerUpdateStatusRepository motorcycleOwnerUpdateInitiatorRepository;

    private MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate;
    private MotorcycleOwnerUpdateStatusEntity motorcycleOwnerUpdateStatusEntity;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateStatusAggregate = motorcycleOwnerUpdateStatusAggregate();
        motorcycleOwnerUpdateStatusEntity = motorcycleOwnerUpdateStatusEntity();
    }

    @Test
    void shouldCallSaveMethodOnMotorcycleOwnershipExchangeProcessRepositoryInterface() {
//        given
        when(motorcycleOwnerUpdateStatusMapper.toEntity(motorcycleOwnerUpdateStatusAggregate))
                .thenReturn(motorcycleOwnerUpdateStatusEntity);
//        when
        motorcycleOwnershipExchangeProcessInitCommandAdapter.init(motorcycleOwnerUpdateStatusAggregate);
//        then
        verify(motorcycleOwnerUpdateInitiatorRepository).save(motorcycleOwnerUpdateStatusEntity);
    }

}