package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import pl.grzegorz.motorcycleadapters.helper.MotorcycleApplicationHelper;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.BikerView;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.PageInfo;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcyclePageQueryAdapterTest {

    @InjectMocks
    private MotorcyclePageQueryAdapter motorcyclePageQueryAdapter;
    @Mock
    private MotorcycleViewRepository motorcycleViewRepository;
    @Mock
    private MotorcycleApplicationHelper motorcycleApplicationHelper;
    @Mock
    private MotorcycleViewMapper motorcycleViewMapper;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;

    private List<MotorcycleViewEntity> motorcycleViewEntities;
    private List<MotorcycleView> motorcycleViewList;
    private BikerView bikerView;
    private PageInfo pageInfo;
    private BikerAggregate bikerAggregate;

    @BeforeEach
    void setup() {
        motorcycleViewEntities = motorcycleEntities();
        motorcycleViewList = motorcycleViewList();
        bikerView = bikerView();
        pageInfo = PageInfo.builder()
                .actualPage(0)
                .pageSize(10)
                .totalPages(1)
                .totalRecordCount(2)
                .build();
        bikerAggregate = bikerAggregate();
    }

    @Test
    void shouldReturnResultViewOfMotorcycles() {
//        given
        var filter = new ResourceFilters(null, null);
        var pageRequest = PageRequest.of(0, 10);
        var motorcyclePage = new PageImpl<>(motorcycleViewEntities, pageRequest, 2);
        var bikerId = bikerView.id().toString();
        when(motorcycleApplicationHelper.getPageRequest(filter)).thenReturn(pageRequest);
        when(motorcycleViewMapper.toView(motorcyclePage.getContent().get(0 ))).thenReturn(motorcycleViewList.get(0));
        when(motorcycleViewRepository.findAll(pageRequest)).thenReturn(motorcyclePage);
        when(motorcycleApplicationHelper.toPageInfo(pageRequest, motorcyclePage)).thenReturn(pageInfo);
        when(motorcycleViewMapper.toView(motorcyclePage.getContent().get(0))).thenReturn(motorcycleViewList.get(0));
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
//        when
        var motorcycles = motorcyclePageQueryAdapter.findAll(filter);
//        then
        assertAll(
                () -> assertThat(motorcycles.pageInfo().actualPage(), is(0)),
                () -> assertThat(motorcycles.pageInfo().pageSize(), is(10)),
                () -> assertThat(motorcycles.pageInfo().totalPages(), is(1)),
                () -> assertThat(motorcycles.pageInfo().totalRecordCount(), is(2L)),
                () -> assertThat(motorcycles.results().size(), is(1)),
                () -> assertThat(motorcycles.results().get(0).brand(), is("Ducati")),
                () -> assertThat(motorcycles.results().get(0).model(), is("Panigale V4S")),
                () -> assertThat(motorcycles.results().get(0).vintage(), is(2022)),
                () -> assertThat(motorcycles.results().get(0).horsePower(), is(225)),
                () -> assertThat(motorcycles.results().get(0).capacity(), is(1199))
        );
    }
}