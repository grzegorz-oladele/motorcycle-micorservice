package pl.grzegorz.motorcycleadapters.out.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleMappedEngineEvent;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleAggregate;

@ExtendWith(MockitoExtension.class)
class MotorcycleMapEngineCommandPublisherAdapterTest {

    @InjectMocks
    private MotorcycleMapEngineCommandPublisherAdapter motorcycleMapEngineCommandPublisherAdapter;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.map-engine.exchange}")
    private String motorcycleMapEngineExchange;
    @Value("${spring.rabbitmq.motorcycle.map-engine.routing-key}")
    private String motorcycleMapEngineRoutingKey;

    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallConvertAndSendOnRabbitTemplateClass() {
//        given
//        when
        motorcycleMapEngineCommandPublisherAdapter.publish(motorcycleAggregate);
//        then
        verify(rabbitTemplate).convertAndSend(eq(motorcycleMapEngineExchange), eq(motorcycleMapEngineRoutingKey),
                any(MotorcycleMappedEngineEvent.class));
    }
}