package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleOwnerUpdateConfirmationStatusAggregate;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleOwnerUpdateConfirmationStatusEntity;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateConfirmationCreateCreateCommandAdapterTest {

    @InjectMocks
    private MotorcycleOwnerUpdateConfirmationConfirmCommandAdapter motorcycleOwnerUpdateConfirmationConfirmCommandAdapter;
    @Mock
    private MotorcycleOwnerUpdateConfirmationMapper motorcycleOwnerUpdateConfirmationMapper;
    @Mock
    private MotorcycleOwnerUpdateConfirmationRepository motorcycleOwnerupdateConfirmationRepository;

    private MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate;
    private MotorcycleOwnerUpdateConfirmationStatusEntity motorcycleOwnerUpdateConfirmationStatusEntity;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateConfirmationStatusAggregate = motorcycleOwnerUpdateConfirmationStatusAggregate();
        motorcycleOwnerUpdateConfirmationStatusEntity = motorcycleOwnerUpdateConfirmationStatusEntity();
    }

    @Test
    void shouldCallSaveMethodOnMotorcycleExchangeConfirmationRepositoryInterface() {
//        given
        when(motorcycleOwnerUpdateConfirmationMapper.toEntity(motorcycleOwnerUpdateConfirmationStatusAggregate))
                .thenReturn(motorcycleOwnerUpdateConfirmationStatusEntity);
//        when
        motorcycleOwnerUpdateConfirmationConfirmCommandAdapter.confirm(motorcycleOwnerUpdateConfirmationStatusAggregate);
//        then
        verify(motorcycleOwnerupdateConfirmationRepository).save(motorcycleOwnerUpdateConfirmationStatusEntity);
    }
}