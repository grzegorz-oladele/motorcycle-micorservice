package pl.grzegorz.motorcycleadapters.out.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleSetUnActiveEvent;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleAggregate;

@ExtendWith(MockitoExtension.class)
class MotorcycleSetUnactiveCommandPublisherAdapterTest {

    @InjectMocks
    private MotorcycleSetUnactiveCommandPublisherAdapter motorcycleSetUnactiveCommandPublisherAdapter;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.unactive.exchange}")
    private String motorcycleSetUnactiveExchange;
    @Value("${spring.rabbitmq.motorcycle.unactive.routing-key}")
    private String motorcycleSetUnactiveRoutingKey;

    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallConvertAndSendMethodOnRabbitTemplateClass() {
//        given
//        when
        motorcycleSetUnactiveCommandPublisherAdapter.publish(motorcycleAggregate);
//        then
        verify(rabbitTemplate).convertAndSend(eq(motorcycleSetUnactiveExchange), eq(motorcycleSetUnactiveRoutingKey),
                any(MotorcycleSetUnActiveEvent.class));
    }
}