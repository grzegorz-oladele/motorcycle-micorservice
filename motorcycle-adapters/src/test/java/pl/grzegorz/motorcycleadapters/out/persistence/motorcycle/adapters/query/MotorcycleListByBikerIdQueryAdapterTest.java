package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import pl.grzegorz.motorcycleadapters.helper.MotorcycleApplicationHelper;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.SimplifiedMotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.PageInfo;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleListByBikerIdQueryAdapterTest {

    @InjectMocks
    private MotorcycleListByBikerIdQueryAdapter motorcycleListByBikerIdQueryAdapter;
    @Mock
    private MotorcycleViewRepository motorcycleViewRepository;
    @Mock
    private MotorcycleViewMapper motorcycleViewMapper;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private MotorcycleApplicationHelper motorcycleApplicationHelper;

    private List<SimplifiedMotorcycleView> motorcycles;
    private Page<MotorcycleViewEntity> motorcycleEntityPage;
    private BikerAggregate bikerAggregate;
    private ResourceFilters resourceFilters;
    private PageInfo pageInfo;

    @BeforeEach
    void setup() {
        motorcycles = motorcycles();
        motorcycleEntityPage = motorcycleEntitiePage();
        bikerAggregate = bikerAggregate();
        resourceFilters = ResourceFilters.empty();
        pageInfo = PageInfo.builder()
                .pageSize(10)
                .totalPages(1)
                .totalRecordCount(2)
                .actualPage(0)
                .build();
    }

    @Test
    void shouldReturnListOfMotorcyclesWhenBikerWillBeExistedInDb() {
//        given
        var bikerId = BIKER_ID.toString();
        var pageRequest = PageRequest.of(resourceFilters.page(), resourceFilters.size());
        when(motorcycleViewRepository.existsBikerByBikerId(UUID.fromString(bikerId))).thenReturn(Boolean.TRUE);
        when(motorcycleApplicationHelper.getPageRequest(resourceFilters)).thenReturn(pageRequest);
        when(motorcycleViewRepository.findAllByBikerId(UUID.fromString(bikerId), pageRequest)).thenReturn(motorcycleEntityPage);
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
        when(motorcycleViewMapper.toSimplifiedViewList(motorcycleEntityPage.getContent())).thenReturn(motorcycles);
        when(motorcycleApplicationHelper.toPageInfo(pageRequest, motorcycleEntityPage)).thenReturn(pageInfo);
//        when
        var motorcyclesByBikerId = motorcycleListByBikerIdQueryAdapter.findAllByBiker(bikerId, resourceFilters);
//        then
        assertAll(
                () -> assertThat(motorcyclesByBikerId.owner().id().toString(), is(bikerId)),
                () -> assertThat(motorcyclesByBikerId.owner().username(), is("test username")),
                () -> assertThat(motorcyclesByBikerId.owner().firstName(), is("test first name")),
                () -> assertThat(motorcyclesByBikerId.owner().email(), is("test email")),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().actualPage(), is(0)),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().totalPages(), is(1)),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().totalRecordCount(), is(2L)),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().pageSize(), is(10)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).brand(), is("Ducati")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).model(), is("Panigale V4S")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).capacity(), is(1199)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).horsePower(), is(225)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).vintage(), is(2022)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).brand(), is("BMW")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).model(), is("S1000RR")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).capacity(), is(999)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).horsePower(), is(208)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).vintage(), is(2021))
        );
    }
}