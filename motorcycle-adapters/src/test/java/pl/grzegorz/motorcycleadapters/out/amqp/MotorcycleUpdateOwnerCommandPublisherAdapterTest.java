package pl.grzegorz.motorcycleadapters.out.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleUpdatedOwnerEvent;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleAggregate;

@ExtendWith(MockitoExtension.class)
class MotorcycleUpdateOwnerCommandPublisherAdapterTest {

    @InjectMocks
    private MotorcycleUpdateOwnerCommandPublisherAdapter motorcycleUpdateOwnerCommandPublisherAdapter;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.update-owner.exchange}")
    private String updateOwnerExchange;
    @Value("${spring.rabbitmq.motorcycle.update-owner.routing-key}")
    private String updateOwnerRoutingKey;

    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallConvertAndSendOnRabbitTemplateClass() {
//        given
        var ownerTransferorId = UUID.randomUUID();
//        when
        motorcycleUpdateOwnerCommandPublisherAdapter.publish(motorcycleAggregate, ownerTransferorId);
//        then
        verify(rabbitTemplate).convertAndSend(eq(updateOwnerExchange), eq(updateOwnerRoutingKey),
                any(MotorcycleUpdatedOwnerEvent.class));
    }
}