package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleadapters.out.persistence.biker.query.BikerViewMapper;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleAggregate;
import static pl.grzegorz.motorcycleadapters.MotorcycleFixtures.motorcycleViewEntity;

@ExtendWith(MockitoExtension.class)
class MotorcycleByIdQueryAdapterTest {

    @InjectMocks
    private MotorcycleByIdQueryAdapter motorcycleByIdQueryAdapter;
    @Mock
    private MotorcycleViewRepository motorcycleViewRepository;
    @Mock
    private BikerViewMapper bikerViewMapper;
    @Mock
    private MotorcycleViewMapper motorcycleViewMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleViewEntity motorcycleViewEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleViewEntity = motorcycleViewEntity();
    }

    @Test
    void shouldReturnMotorcycleAggregateWhenMotorcycleWillBeExistedInDb() {
//        given
        var motorcycleId = motorcycleViewEntity.id();
        var bikerId = motorcycleViewEntity.bikerId();
        when(motorcycleViewRepository.findById(motorcycleId)).thenReturn(Optional.of(motorcycleViewEntity));
        when(motorcycleViewMapper.toDomain(motorcycleViewEntity)).thenReturn(motorcycleAggregate);
//        when
        var foundMotorcycleAggregate =
                motorcycleByIdQueryAdapter.getMotorcycleById(motorcycleId.toString(), bikerId.toString());
//        then
        assertAll(
                () -> assertThat(foundMotorcycleAggregate.id(), is(motorcycleAggregate.id())),
                () -> assertThat(foundMotorcycleAggregate.bikerId(), is(motorcycleAggregate.bikerId()))
        );
    }
}