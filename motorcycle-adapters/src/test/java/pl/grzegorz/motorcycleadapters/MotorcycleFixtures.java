package pl.grzegorz.motorcycleadapters;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query.MotorcycleViewEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command.MotorcycleOwnerUpdateConfirmationStatusEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateStatusEntity;
import pl.grzegorz.motorcycleapplication.ports.view.BikerView;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.ports.view.SimplifiedMotorcycleView;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MotorcycleFixtures {

    private static final UUID MOTORCYCLE_AGGREGATE_ID = UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6");
    public static final UUID BIKER_ID = UUID.fromString("ede813ee-5960-45a7-9f9c-8cb4a3d8ba38");
    public static final UUID RECIPIENT_ID = UUID.fromString("378680f7-ecd7-43e5-9184-837919c7c678");
    private static final UUID EXCHANGE_INIT_PROCESS_ID = UUID.fromString("abfd3440-4d45-43b5-a3c2-0a523720cfde");
    private static final UUID UPDATE_CONFIRMATION_STATUS_ID = UUID.fromString("26802971-1f17-4041-86e7-ca4505f2a658");


    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_AGGREGATE_ID)
                .withBikerId(BIKER_ID)
                .withModel("Ducati")
                .withBrand("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static Page<MotorcycleViewEntity> motorcycleEntitiePage() {
        return new PageImpl<>(List.of(motorcycleViewEntity(), secondMotorcycleViewEntity()));
    }

    public static List<MotorcycleViewEntity> motorcycleEntities() {
        return Collections.singletonList(motorcycleViewEntity());
    }

    public static MotorcycleViewEntity motorcycleViewEntity() {
        return MotorcycleViewEntity.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static MotorcycleEntity motorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static MotorcycleEntity secondMotorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBikerId(BIKER_ID)
                .withModel("BMW ")
                .withBrand("S1000RR")
                .withCapacity(999)
                .withHorsePower(208)
                .withVintage(2021)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static MotorcycleViewEntity secondMotorcycleViewEntity() {
        return MotorcycleViewEntity.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBikerId(BIKER_ID)
                .withModel("BMW ")
                .withBrand("S1000RR")
                .withCapacity(999)
                .withHorsePower(208)
                .withVintage(2021)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static List<SimplifiedMotorcycleView> motorcycles() {
        return Arrays.asList(firstMotorcycle(), secondMotorcycle());
    }

    public static SimplifiedMotorcycleView firstMotorcycle() {
        return SimplifiedMotorcycleView.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static SimplifiedMotorcycleView secondMotorcycle() {
        return SimplifiedMotorcycleView.builder()
                .withId(UUID.fromString("ac9d82be-2571-42bf-b380-3aea6b2223bb"))
                .withBrand("BMW")
                .withModel("S1000RR")
                .withCapacity(999)
                .withHorsePower(208)
                .withVintage(2021)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static List<MotorcycleView> motorcycleViewList() {
        return Arrays.asList(firstMotorcycleView(), secondMotorcycleView());
    }

    public static BikerView bikerView() {
        return BikerView.builder()
                .withId(BIKER_ID)
                .withEmail("test email")
                .withFirstName("test first name")
                .withUserName("test username")
                .build();
    }

    private static MotorcycleView firstMotorcycleView() {
        return MotorcycleView.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    private static MotorcycleView secondMotorcycleView() {
        return MotorcycleView.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBrand("BMW")
                .withModel("S1000RR")
                .withCapacity(999)
                .withHorsePower(208)
                .withVintage(2021)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withEmail("test email")
                .withFirstName("test first name")
                .withUserName("test username")
                .build();
    }

    public static MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate() {
        return MotorcycleOwnerUpdateStatusAggregate.builder()
                .withId(EXCHANGE_INIT_PROCESS_ID)
                .withMotorcycleId(MOTORCYCLE_AGGREGATE_ID)
                .withOwnerTransferorId(BIKER_ID)
                .withRecipientId(RECIPIENT_ID)
                .withProcessCompleted(Boolean.FALSE)
                .build();
    }

    public static MotorcycleOwnerUpdateStatusEntity motorcycleOwnerUpdateStatusEntity() {
        return MotorcycleOwnerUpdateStatusEntity.builder()
                .withId(EXCHANGE_INIT_PROCESS_ID)
                .withMotorcycleId(MOTORCYCLE_AGGREGATE_ID)
                .withOwnerTransferorId(BIKER_ID)
                .withRecipientId(RECIPIENT_ID)
                .withProcessCompleted(Boolean.FALSE)
                .build();
    }

    public static MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate() {
        return MotorcycleOwnerUpdateConfirmationStatusAggregate.builder()
                .withId(UPDATE_CONFIRMATION_STATUS_ID)
                .withRecipientId(RECIPIENT_ID)
                .withConfirmationToken(UUID.randomUUID().toString())
                .withExchangeProcessId(EXCHANGE_INIT_PROCESS_ID)
                .withIsConfirm(Boolean.TRUE)
                .build();
    }

    public static MotorcycleOwnerUpdateConfirmationStatusEntity motorcycleOwnerUpdateConfirmationStatusEntity() {
        return MotorcycleOwnerUpdateConfirmationStatusEntity.builder()
                .withId(UPDATE_CONFIRMATION_STATUS_ID)
                .withRecipientId(RECIPIENT_ID)
                .withConfirmationToken(UUID.randomUUID().toString())
                .withExchangeProcessId(EXCHANGE_INIT_PROCESS_ID)
                .withIsConfirm(Boolean.TRUE)
                .build();
    }
}