package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateFinishProcessPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleOwnerUpdateFinishProcessAdapter implements MotorcycleOwnerUpdateFinishProcessPort {

    private final MotorcycleOwnerUpdateStatusMapper motorcycleOwnerUpdateStatusMapper;
    private final MotorcycleOwnerUpdateStatusRepository motorcycleOwnerUpdateInitiatorRepository;

    @Override
    public void finish(MotorcycleOwnerUpdateStatusAggregate exchangeProcessAggregate) {
        var exchangeProcessEntity = motorcycleOwnerUpdateStatusMapper.toEntity(exchangeProcessAggregate);
        motorcycleOwnerUpdateInitiatorRepository.save(exchangeProcessEntity);
        log.info("Finishing motorcycle exchange process using id -> [{}]", exchangeProcessEntity.motorcycleId());
    }
}