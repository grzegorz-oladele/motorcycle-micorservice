package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

@Component
class MotorcycleExchangeConfirmationQueryMapper {

    public MotorcycleOwnerUpdateConfirmationStatusAggregate toDomain(MotorcycleExchangeConfirmationQueryEntity confirmationQueryEntity) {
        return MotorcycleOwnerUpdateConfirmationStatusAggregate.builder()
                .withId(confirmationQueryEntity.id())
                .withExchangeProcessId(confirmationQueryEntity.exchangeProcessId())
                .withRecipientId(confirmationQueryEntity.recipientId())
                .withConfirmationToken(confirmationQueryEntity.confirmationToken())
                .withIsConfirm(confirmationQueryEntity.isConfirm())
                .build();
    }
}