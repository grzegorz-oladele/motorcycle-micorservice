package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MotorcycleOwnerUpdateConfirmationRepository extends JpaRepository<MotorcycleOwnerUpdateConfirmationStatusEntity, UUID> {
}