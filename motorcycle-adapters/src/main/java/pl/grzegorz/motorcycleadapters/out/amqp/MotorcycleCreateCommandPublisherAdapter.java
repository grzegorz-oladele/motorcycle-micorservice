package pl.grzegorz.motorcycleadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleCreatePublisherPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleCreatedEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleCreateCommandPublisherAdapter implements MotorcycleCreatePublisherPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.create.exchange}")
    private String motorcycleCreateExchange;
    @Value("${spring.rabbitmq.motorcycle.create.routing-key}")
    private String motorcycleCreateRoutingKey;

    @Override
    public void publish(MotorcycleAggregate motorcycleAggregate) {
        var createdEvent = MotorcycleCreatedEvent.of(motorcycleAggregate);
        rabbitTemplate.convertAndSend(motorcycleCreateExchange, motorcycleCreateRoutingKey, createdEvent);
        log.info("Send event -> [{}] to exchange -> [{}]", createdEvent, motorcycleCreateExchange);
    }
}