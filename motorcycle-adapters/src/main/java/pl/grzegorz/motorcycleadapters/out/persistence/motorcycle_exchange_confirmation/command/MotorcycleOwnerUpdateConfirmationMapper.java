package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

@Component
class MotorcycleOwnerUpdateConfirmationMapper {

    public MotorcycleOwnerUpdateConfirmationStatusEntity toEntity(MotorcycleOwnerUpdateConfirmationStatusAggregate confirmationAggregate) {
        return MotorcycleOwnerUpdateConfirmationStatusEntity.builder()
                .withId(confirmationAggregate.id())
                .withExchangeProcessId(confirmationAggregate.exchangeProcessId())
                .withRecipientId(confirmationAggregate.recipientId())
                .withConfirmationToken(confirmationAggregate.confirmationToken())
                .withIsConfirm(confirmationAggregate.isConfirm())
                .build();
    }
}