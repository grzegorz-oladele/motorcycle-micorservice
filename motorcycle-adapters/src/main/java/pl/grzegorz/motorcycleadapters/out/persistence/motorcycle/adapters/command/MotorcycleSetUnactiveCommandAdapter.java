package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.helper.MotorcycleApplicationHelper;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleSetUnactiveCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleSetUnactiveCommandAdapter implements MotorcycleSetUnactiveCommandPort {

    private final MotorcycleApplicationHelper applicationHelper;
    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void setUnactive(MotorcycleAggregate motorcycleAggregate) {
        applicationHelper.checkBikerPermission(motorcycleAggregate.bikerId().toString(), motorcycleAggregate);
        motorcycleAggregate.setUnActive();
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Motorcycle with id -> [{}] set as unactive", motorcycleAggregate.id());
    }
}