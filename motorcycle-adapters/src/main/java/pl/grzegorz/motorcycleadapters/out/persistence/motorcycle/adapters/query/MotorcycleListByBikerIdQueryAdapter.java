package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.helper.MotorcycleApplicationHelper;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.BikerSimplifiedView;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.ports.view.SimplifiedMotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;
import pl.grzegorz.motorcycledomain.exception.BikerNotFoundException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class MotorcycleListByBikerIdQueryAdapter implements MotorcycleListByBikerIdQueryPort {

    private final MotorcycleViewRepository motorcycleViewRepository;
    private final MotorcycleViewMapper motorcycleViewMapper;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleApplicationHelper motorcycleApplicationHelper;

    @Override
    public MotorcycleListByBikerIdView findAllByBiker(String bikerId, ResourceFilters filters) {
        if (Boolean.FALSE.equals(motorcycleViewRepository.existsBikerByBikerId(UUID.fromString(bikerId)))) {
            throw new BikerNotFoundException(String.format(
                    ExceptionMessage.BIKER_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), bikerId
            ));
        }
        var pageable = motorcycleApplicationHelper.getPageRequest(filters);
        var motorcycleEntityPage = motorcycleViewRepository.findAllByBikerId(UUID.fromString(bikerId), pageable);
        return MotorcycleListByBikerIdView.builder()
                .withOwner(BikerSimplifiedView.toView(bikerByIdQueryPort.getById(bikerId)))
                .withResults(ResultViewInfo.<SimplifiedMotorcycleView>builder()
                        .pageInfo(motorcycleApplicationHelper.toPageInfo(pageable, motorcycleEntityPage))
                        .results(motorcycleViewMapper.toSimplifiedViewList(motorcycleEntityPage.get()
                                .collect(Collectors.toList())))
                        .build())
                .build();
    }
}