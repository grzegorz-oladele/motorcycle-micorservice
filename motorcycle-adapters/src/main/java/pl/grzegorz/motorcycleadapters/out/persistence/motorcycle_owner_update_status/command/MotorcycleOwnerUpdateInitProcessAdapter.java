package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleOwnerUpdateInitProcessAdapter implements MotorcycleOwnerUpdateInitProcessPort {

    private final MotorcycleOwnerUpdateStatusMapper motorcycleOwnerUpdateStatusMapper;
    private final MotorcycleOwnerUpdateStatusRepository motorcycleOwnerUpdateInitiatorRepository;

    @Override
    public void init(MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate) {
        var exchangeProcessEntity = motorcycleOwnerUpdateStatusMapper.toEntity(motorcycleOwnerUpdateStatusAggregate);
        motorcycleOwnerUpdateInitiatorRepository.save(exchangeProcessEntity);
        log.info("Initializing ownership exchange process for motorcycle using id -> [{}] between biker using " +
                        "id -> [{}] and biker using id -> [{}]", motorcycleOwnerUpdateStatusAggregate.motorcycleId(),
                motorcycleOwnerUpdateStatusAggregate.ownerTransferorId(), motorcycleOwnerUpdateStatusAggregate.recipientId());
    }
}