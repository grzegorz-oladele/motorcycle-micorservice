package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

@Component
class MotorcycleOwnerUpdateStatusQueryMapper {

    public MotorcycleOwnerUpdateStatusAggregate toDomain(MotorcycleOwnerUpdateStatusViewEntity exchangeProcessViewEntity) {
        return MotorcycleOwnerUpdateStatusAggregate.builder()
                .withId(exchangeProcessViewEntity.id())
                .withMotorcycleId(exchangeProcessViewEntity.motorcycleId())
                .withOwnerTransferorId(exchangeProcessViewEntity.ownerTransferorId())
                .withRecipientId(exchangeProcessViewEntity.recipientId())
                .withProcessCompleted(exchangeProcessViewEntity.processCompleted())
                .build();
    }
}