package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MotorcycleOwnerUpdateStatusRepository extends JpaRepository<MotorcycleOwnerUpdateStatusEntity, UUID> {
}