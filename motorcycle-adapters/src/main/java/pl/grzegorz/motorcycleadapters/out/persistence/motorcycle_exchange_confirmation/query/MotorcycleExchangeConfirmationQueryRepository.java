package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface MotorcycleExchangeConfirmationQueryRepository extends JpaRepository<MotorcycleExchangeConfirmationQueryEntity, UUID> {

    Optional<MotorcycleExchangeConfirmationQueryEntity> findByConfirmationToken(String confirmationToken);
}