package pl.grzegorz.motorcycleadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerEnableCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerEnableCommandAdapter implements BikerEnableCommandPort {

    private final BikerMapper bikerMapper;
    private final BikerRepository bikerRepository;

    @Override
    public void enable(BikerAggregate bikerAggregate) {
        bikerAggregate.enable();
        var bikerEntity = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(bikerEntity);
        log.info("Enable biker using id -> [{}]", bikerAggregate.id());
    }
}