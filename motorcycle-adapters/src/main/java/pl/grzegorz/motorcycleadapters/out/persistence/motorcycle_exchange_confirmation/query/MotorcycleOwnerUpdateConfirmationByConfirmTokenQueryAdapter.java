package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.query.MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.exception.ExchangeConfirmationNotFoundException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

@Service
@RequiredArgsConstructor
class MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryAdapter implements MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort {

    private final MotorcycleExchangeConfirmationQueryMapper motorcycleExchangeConfirmationQueryMapper;
    private final MotorcycleExchangeConfirmationQueryRepository motorcycleExchangeConfirmationQueryRepository;

    @Override
    public MotorcycleOwnerUpdateConfirmationStatusAggregate findByToken(String token) {
        return motorcycleExchangeConfirmationQueryRepository.findByConfirmationToken(token)
                .map(motorcycleExchangeConfirmationQueryMapper::toDomain)
                .orElseThrow(() -> new ExchangeConfirmationNotFoundException(String.format(
                        ExceptionMessage.EXCHANGE_CONFIRMATION_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), token)));
    }
}