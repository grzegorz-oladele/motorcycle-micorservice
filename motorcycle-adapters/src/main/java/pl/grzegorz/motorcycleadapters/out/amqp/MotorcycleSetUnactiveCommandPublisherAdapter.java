package pl.grzegorz.motorcycleadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleSetUnactivePublisherPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleSetUnActiveEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleSetUnactiveCommandPublisherAdapter implements MotorcycleSetUnactivePublisherPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.unactive.exchange}")
    private String motorcycleSetUnactiveExchange;
    @Value("${spring.rabbitmq.motorcycle.unactive.routing-key}")
    private String motorcycleSetUnactiveRoutingKey;

    @Override
    public void publish(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleSetUnactiveEvent = MotorcycleSetUnActiveEvent.of(motorcycleAggregate);
        rabbitTemplate.convertAndSend(motorcycleSetUnactiveExchange, motorcycleSetUnactiveRoutingKey,
                motorcycleSetUnactiveEvent);
        log.info("Send event -> [{}] to exchange -> [{}]", motorcycleSetUnactiveEvent, motorcycleSetUnactiveExchange);
    }
}