package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleEntity;

import java.util.UUID;

@Repository
interface MotorcycleViewRepository extends JpaRepository<MotorcycleViewEntity, UUID> {

    Page<MotorcycleViewEntity> findAllByBikerId(UUID bikerId, Pageable pageable);

    Boolean existsByIdAndBikerId(UUID motorcycleId, UUID bikerId);

    @Query("SELECT CASE WHEN COUNT(m) > 0 THEN true ELSE false END " +
            "FROM MotorcycleViewEntity m WHERE m.bikerId = :bikerId")
    Boolean existsBikerByBikerId(UUID bikerId);
}