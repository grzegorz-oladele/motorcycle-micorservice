package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleMapper;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleRepository;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.MotorcycleNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class MotorcycleByIdQueryAdapter implements MotorcycleByIdQueryPort {

    private final MotorcycleViewRepository motorcycleViewRepository;
    private final MotorcycleViewMapper motorcycleViewMapper;

    @Override
    public MotorcycleAggregate getMotorcycleById(String motorcycleId, String bikerId) {
        return motorcycleViewRepository.findById(UUID.fromString(motorcycleId))
                .map(motorcycleViewMapper::toDomain)
                .orElseThrow(() -> new MotorcycleNotFoundException(String.format("Motorcycle with id [%s] not found",
                        motorcycleId)));
    }
}