package pl.grzegorz.motorcycleadapters.out.persistence.biker.query;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerNotFoundException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerByIdQueryAdapter implements BikerByIdQueryPort {

    private final BikerViewMapper bikerViewMapper;
    private final BikerViewRepository bikerViewRepository;

    @Override
    public BikerAggregate getById(String bikerId) {
        return bikerViewRepository.findById(UUID.fromString(bikerId))
                .map(bikerViewMapper::toDomain)
                .orElseThrow(() -> new BikerNotFoundException(
                        String.format(ExceptionMessage.BIKER_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), bikerId)
                ));
    }
}