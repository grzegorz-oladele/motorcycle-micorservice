package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationCreateCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleOwnerUpdateConfirmationCreateCreateCommandAdapter implements MotorcycleOwnerUpdateConfirmationCreateCommandPort {

    private final MotorcycleOwnerUpdateConfirmationMapper motorcycleOwnerUpdateConfirmationMapper;
    private final MotorcycleOwnerUpdateConfirmationRepository motorcycleOwnerupdateConfirmationRepository;

    @Override
    public MotorcycleOwnerUpdateConfirmationStatusAggregate create(UUID exchangeProcessId, UUID recipientId) {
        var exchangeConfirmationAggregate = MotorcycleOwnerUpdateConfirmationStatusAggregate.create(exchangeProcessId, recipientId);
        var exchangeConformationEntity = motorcycleOwnerUpdateConfirmationMapper.toEntity(exchangeConfirmationAggregate);
        motorcycleOwnerupdateConfirmationRepository.save(exchangeConformationEntity);
        log.info("Creating confirm for process using id -> [{}] with recipient using id -> [{}]", exchangeProcessId, recipientId);
        return exchangeConfirmationAggregate;
    }
}