package pl.grzegorz.motorcycleadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleMapEnginePublisherPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleMappedEngineEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleMapEngineCommandPublisherAdapter implements MotorcycleMapEnginePublisherPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.map-engine.exchange}")
    private String motorcycleMapEngineExchange;
    @Value("${spring.rabbitmq.motorcycle.map-engine.routing-key}")
    private String motorcycleMapEngineRoutingKey;

    @Override
    public void publish(MotorcycleAggregate motorcycleAggregate) {
        var mappedEngineEvent = MotorcycleMappedEngineEvent.of(motorcycleAggregate);
        rabbitTemplate.convertAndSend(motorcycleMapEngineExchange, motorcycleMapEngineRoutingKey, mappedEngineEvent);
        log.info("Send event -> [{}] to exchange -> [{}]", mappedEngineEvent, motorcycleMapEngineExchange);
    }
}