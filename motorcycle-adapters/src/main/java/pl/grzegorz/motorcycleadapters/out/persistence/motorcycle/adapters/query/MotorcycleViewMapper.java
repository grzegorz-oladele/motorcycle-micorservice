package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.ports.view.SimplifiedMotorcycleView;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class MotorcycleViewMapper {

    public MotorcycleAggregate toDomain(MotorcycleViewEntity motorcycleEntity) {
        return MotorcycleAggregate.builder()
                .withId(motorcycleEntity.id())
                .withBikerId(motorcycleEntity.bikerId())
                .withBrand(motorcycleEntity.brand())
                .withModel(motorcycleEntity.model())
                .withCapacity(motorcycleEntity.capacity())
                .withHorsePower(motorcycleEntity.horsePower())
                .withVintage(motorcycleEntity.vintage())
                .withSerialNumber(motorcycleEntity.serialNumber())
                .withMotorcycleClass(motorcycleEntity.motorcycleClass())
                .withCreatedAt(motorcycleEntity.createdAt())
                .withModifiedAt(motorcycleEntity.modifiedAt())
                .withIsActive(motorcycleEntity.isActive())
                .build();
    }

    public List<SimplifiedMotorcycleView> toSimplifiedViewList(List<MotorcycleViewEntity> motorcycles) {
        return motorcycles
                .stream()
                .map(this::toSimplifiedView)
                .toList();
    }

    private SimplifiedMotorcycleView toSimplifiedView(MotorcycleViewEntity motorcycle) {
        return SimplifiedMotorcycleView.builder()
                .withId(motorcycle.id())
                .withBrand(motorcycle.brand())
                .withModel(motorcycle.model())
                .withCapacity(motorcycle.capacity())
                .withVintage(motorcycle.vintage())
                .withSerialNumber(motorcycle.serialNumber())
                .withHorsePower(motorcycle.horsePower())
                .withMotorcycleClass(motorcycle.motorcycleClass())
                .build();
    }

    public MotorcycleView toView(MotorcycleViewEntity motorcycle) {
        return MotorcycleView.builder()
                .withId(motorcycle.id())
                .withBrand(motorcycle.brand())
                .withModel(motorcycle.model())
                .withCapacity(motorcycle.capacity())
                .withVintage(motorcycle.vintage())
                .withHorsePower(motorcycle.horsePower())
                .withSerialNumber(motorcycle.serialNumber())
                .withMotorcycleClass(motorcycle.motorcycleClass())
                .withMotorcycleClass(motorcycle.motorcycleClass())
                .withCreatedAt(motorcycle.createdAt())
                .withModifiedAt(motorcycle.modifiedAt())
                .build();
    }
}