package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface MotorcycleOwnerUpdateStatusQueryRepository extends JpaRepository<MotorcycleOwnerUpdateStatusViewEntity, UUID> {
}