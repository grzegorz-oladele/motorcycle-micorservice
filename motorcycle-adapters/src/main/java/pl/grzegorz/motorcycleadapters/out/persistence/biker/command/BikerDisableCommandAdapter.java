package pl.grzegorz.motorcycleadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerDisableCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerDisableCommandAdapter implements BikerDisableCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void disable(BikerAggregate bikerAggregate) {
        bikerAggregate.disable();
        var bikerEntity = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(bikerEntity);
        log.info("Disable biker using id -> [{}]", bikerEntity.id());
    }
}