package pl.grzegorz.motorcycleadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleUpdateOwnerPublisherPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.event.MotorcycleUpdatedOwnerEvent;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleUpdateOwnerCommandPublisherAdapter implements MotorcycleUpdateOwnerPublisherPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.motorcycle.update-owner.exchange}")
    private String updateOwnerExchange;
    @Value("${spring.rabbitmq.motorcycle.update-owner.routing-key}")
    private String updateOwnerRoutingKey;

    @Override
    public void publish(MotorcycleAggregate motorcycleAggregate, UUID ownerTransferorId) {
        var updatedOwnerEvent = MotorcycleUpdatedOwnerEvent.of(motorcycleAggregate)
                .toBuilder()
                .withPreviousOwnerId(ownerTransferorId)
                .build();
        rabbitTemplate.convertAndSend(updateOwnerExchange, updateOwnerRoutingKey, updatedOwnerEvent);
        log.info("Send event -> [{}] to exchange -> [{}]", updatedOwnerEvent, updateOwnerExchange);
    }
}