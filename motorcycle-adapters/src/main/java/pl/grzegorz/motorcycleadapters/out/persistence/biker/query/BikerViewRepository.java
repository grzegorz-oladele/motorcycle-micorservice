package pl.grzegorz.motorcycleadapters.out.persistence.biker.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BikerViewRepository extends JpaRepository<BikerViewEntity, UUID> {
}