package pl.grzegorz.motorcycleadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerCreateCommandAdapter implements BikerCreateCommandPort {

    private final BikerMapper bikerMapper;
    private final BikerRepository bikerRepository;

    @Override
    public void create(BikerAggregate bikerAggregate) {
        var bikerEntity = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(bikerEntity);
        log.info("Create biker using id -> [{}]", bikerEntity.id());
    }
}