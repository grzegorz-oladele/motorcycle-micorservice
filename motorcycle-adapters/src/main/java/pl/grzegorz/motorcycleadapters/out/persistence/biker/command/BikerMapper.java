package pl.grzegorz.motorcycleadapters.out.persistence.biker.command;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

@Component
public class BikerMapper {

    public BikerEntity toEntity(BikerAggregate bikerAggregate) {
        return BikerEntity.builder()
                .withId(bikerAggregate.id())
                .withUserName(bikerAggregate.userName())
                .withFirstName(bikerAggregate.firstName())
                .withLastName(bikerAggregate.lastName())
                .withEmail(bikerAggregate.email())
                .withEnable(bikerAggregate.isActive())
                .withPhoneNumber(bikerAggregate.phoneNumber())
                .withDateOfBirth(bikerAggregate.dateOfBirth())
                .withCreatedAt(bikerAggregate.createdAt())
                .withModifiedAt(bikerAggregate.modifiedAt())
                .build();
    }
}