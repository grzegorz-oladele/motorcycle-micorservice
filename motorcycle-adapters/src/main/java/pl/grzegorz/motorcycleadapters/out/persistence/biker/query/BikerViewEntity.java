package pl.grzegorz.motorcycleadapters.out.persistence.biker.query;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "bikers_view")
@Getter
@Builder(setterPrefix = "with", toBuilder = true)
@Accessors(fluent = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class BikerViewEntity {

    @Id
    @Column(nullable = false, unique = true)
    private UUID id;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean enable;
    private String phoneNumber;
    private LocalDate dateOfBirth;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
}
