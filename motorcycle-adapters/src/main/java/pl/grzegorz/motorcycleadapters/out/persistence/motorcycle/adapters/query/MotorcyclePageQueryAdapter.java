package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.helper.MotorcycleApplicationHelper;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcyclePageQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.BikerView;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;

import java.util.Collections;

@Service
@RequiredArgsConstructor
class MotorcyclePageQueryAdapter implements MotorcyclePageQueryPort {

    private final MotorcycleViewRepository motorcycleViewRepository;
    private final MotorcycleApplicationHelper motorcycleApplicationHelper;
    private final MotorcycleViewMapper motorcycleViewMapper;
    private final BikerByIdQueryPort bikerByIdQueryPort;

    @Override
    public ResultViewInfo<MotorcycleView> findAll(ResourceFilters filter) {
        var pageRequest = motorcycleApplicationHelper.getPageRequest(filter);
        var motorcyclePage = motorcycleViewRepository.findAll(pageRequest);
        if (motorcyclePage.getContent().isEmpty()) {
            return ResultViewInfo.<MotorcycleView>builder()
                    .pageInfo(motorcycleApplicationHelper.toEmptyPageInfo())
                    .results(Collections.emptyList())
                    .build();
        }
        return ResultViewInfo.<MotorcycleView>builder()
                .pageInfo(motorcycleApplicationHelper.toPageInfo(pageRequest, motorcyclePage))
                .results(motorcyclePage.getContent()
                        .stream()
                        .map(motorcycleViewEntity -> motorcycleViewMapper.toView(motorcycleViewEntity).toBuilder()
                                .withOwner(getBikerById(motorcycleViewEntity.bikerId().toString()))
                                .build())
                        .toList())
                .build();
    }

    private BikerView getBikerById(String bikerId) {
        return BikerView.toView(bikerByIdQueryPort.getById(bikerId));
    }
}