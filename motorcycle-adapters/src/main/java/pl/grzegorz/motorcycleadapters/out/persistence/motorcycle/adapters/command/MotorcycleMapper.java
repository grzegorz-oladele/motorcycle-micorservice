package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

@Component
public class MotorcycleMapper {

    public MotorcycleEntity toEntity(MotorcycleAggregate aggregate) {
        return MotorcycleEntity.builder()
                .withId(aggregate.id())
                .withBikerId(aggregate.bikerId())
                .withBrand(aggregate.brand())
                .withModel(aggregate.model())
                .withCapacity(aggregate.capacity())
                .withHorsePower(aggregate.horsePower())
                .withVintage(aggregate.vintage())
                .withSerialNumber(aggregate.serialNumber())
                .withMotorcycleClass(aggregate.motorcycleClass())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .withIsActive(aggregate.isActive())
                .build();
    }
}