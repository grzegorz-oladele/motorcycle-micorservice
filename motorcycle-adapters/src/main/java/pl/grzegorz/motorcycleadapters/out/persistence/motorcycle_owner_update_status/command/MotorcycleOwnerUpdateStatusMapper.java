package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command;

import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

@Component
public class MotorcycleOwnerUpdateStatusMapper {

    public MotorcycleOwnerUpdateStatusEntity toEntity(MotorcycleOwnerUpdateStatusAggregate aggregate) {
        return MotorcycleOwnerUpdateStatusEntity.builder()
                .withId(aggregate.id())
                .withMotorcycleId(aggregate.motorcycleId())
                .withRecipientId(aggregate.recipientId())
                .withOwnerTransferorId(aggregate.ownerTransferorId())
                .withProcessCompleted(aggregate.processCompleted())
                .build();
    }
}