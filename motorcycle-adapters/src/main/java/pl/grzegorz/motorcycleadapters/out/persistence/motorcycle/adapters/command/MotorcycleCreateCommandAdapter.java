package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleCreateCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleCreateCommandAdapter implements MotorcycleCreateCommandPort {

    private final MotorcycleMapper motorcycleMapper;
    private final MotorcycleRepository motorcycleRepository;

    @Override
    public UUID save(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Create motorcycle using id -> [{}]", motorcycleEntity.id());
        return motorcycleEntity.id();
    }
}