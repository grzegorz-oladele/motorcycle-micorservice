package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.query.MotorcycleOwnerUpdateStatusByIdQueryPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;
import pl.grzegorz.motorcycledomain.exception.MotorcycleOwnerUpdateProcessNotFoundException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class MotorcycleOwnerUpdateStatusByIdQueryAdapter implements MotorcycleOwnerUpdateStatusByIdQueryPort {

    private final MotorcycleOwnerUpdateStatusQueryRepository exchangeProcessQueryRepository;
    private final MotorcycleOwnerUpdateStatusQueryMapper exchangeProcessQueryMapper;

    @Override
    public MotorcycleOwnerUpdateStatusAggregate findByProcessId(String processId) {
        return exchangeProcessQueryRepository.findById(UUID.fromString(processId))
                .map(exchangeProcessQueryMapper::toDomain)
                .orElseThrow(() -> new MotorcycleOwnerUpdateProcessNotFoundException(String.format(
                        ExceptionMessage.MOTORCYCLE_EXCHANGE_PROCESS_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), processId)));
    }
}