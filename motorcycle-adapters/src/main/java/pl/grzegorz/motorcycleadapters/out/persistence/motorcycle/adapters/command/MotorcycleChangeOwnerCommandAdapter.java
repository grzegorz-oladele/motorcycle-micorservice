package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleChangeOwnerCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleChangeOwnerCommandAdapter implements MotorcycleChangeOwnerCommandPort {

    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void changeOwner(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Updating owner with id -> [{}] for motorcycle with id -> [{}]", motorcycleEntity.bikerId(),
                motorcycleEntity.id());
    }
}