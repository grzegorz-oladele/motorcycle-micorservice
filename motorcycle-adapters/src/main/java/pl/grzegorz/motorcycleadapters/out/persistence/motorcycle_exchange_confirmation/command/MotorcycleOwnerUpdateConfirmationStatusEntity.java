package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.UUID;

@Entity
@Table(name = "motorcycle_exchange_confirmations")
@Getter
@Builder(setterPrefix = "with", toBuilder = true)
@Accessors(fluent = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class MotorcycleOwnerUpdateConfirmationStatusEntity {

    @Id
    @Column(nullable = false, unique = true)
    private UUID id;
    private UUID exchangeProcessId;
    private UUID recipientId;
    private String confirmationToken;
    private Boolean isConfirm;
}