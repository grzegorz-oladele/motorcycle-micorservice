package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationConfirmCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleOwnerUpdateConfirmationConfirmCommandAdapter implements MotorcycleOwnerUpdateConfirmationConfirmCommandPort {

    private final MotorcycleOwnerUpdateConfirmationMapper confirmationMapper;
    private final MotorcycleOwnerUpdateConfirmationRepository confirmationRepository;

    @Override
    public void confirm(MotorcycleOwnerUpdateConfirmationStatusAggregate confirmationAggregate) {
        var confirmationEntity = confirmationMapper.toEntity(confirmationAggregate);
        confirmationRepository.save(confirmationEntity);
        log.info("The exchange was confirmed by biker using id -> [{}]", confirmationAggregate.recipientId());
    }
}