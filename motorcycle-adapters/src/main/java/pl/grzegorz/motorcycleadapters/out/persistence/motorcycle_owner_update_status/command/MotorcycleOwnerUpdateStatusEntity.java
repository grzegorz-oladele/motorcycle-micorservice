package pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.UUID;

@Entity
@Table(name = "motorcycle_ownership_history")
@Getter
@Accessors(fluent = true)
@Builder(setterPrefix = "with")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class MotorcycleOwnerUpdateStatusEntity {

    @Id
    @Column(nullable = false, unique = true)
    private UUID id;
    private UUID motorcycleId;
    private UUID ownerTransferorId;
    private UUID recipientId;
    private Boolean processCompleted;
}