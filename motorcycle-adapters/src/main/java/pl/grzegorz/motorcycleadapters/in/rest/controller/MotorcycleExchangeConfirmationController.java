package pl.grzegorz.motorcycleadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleExchangeConfirmationApi;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_confirmation.MotorcycleOwnerUpdateConfirmationConfirmUseCase;

@RestController
@RequiredArgsConstructor
class MotorcycleExchangeConfirmationController implements MotorcycleExchangeConfirmationApi {

    private final MotorcycleOwnerUpdateConfirmationConfirmUseCase confirmationConfirmCommandUseCase;

    @Override
    public ResponseEntity<Void> confirm(String confirmationToken) {
        confirmationConfirmCommandUseCase.confirm(confirmationToken);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}