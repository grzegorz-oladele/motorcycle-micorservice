package pl.grzegorz.motorcycleadapters.in.amqp.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerUpdateDto;
import pl.grzegorz.motorcycleadapters.in.amqp.handler.BikerUpdateCommandHandler;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerUpdateCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerUpdateCommandHandlerImpl implements BikerUpdateCommandHandler {

    private final BikerUpdateCommandUseCase bikerUpdateCommandUseCase;

    @Override
    public void handle(BikerUpdateDto bikerUpdateDto) {
        bikerUpdateCommandUseCase.update(bikerUpdateDto.toUpdateCommand());
    }
}