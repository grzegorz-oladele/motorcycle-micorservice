package pl.grzegorz.motorcycleadapters.in.amqp.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerEnableDto;
import pl.grzegorz.motorcycleadapters.in.amqp.handler.BikerEnableCommandHandler;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerEnableCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerEnableCommandHandlerImpl implements BikerEnableCommandHandler {

    private final BikerEnableCommandUseCase bikerEnableCommandUseCase;

    @Override
    public void handle(BikerEnableDto bikerEnableDto) {
        bikerEnableCommandUseCase.enable(bikerEnableDto.id());
    }
}