package pl.grzegorz.motorcycleadapters.in.amqp.dto;

import java.io.Serializable;
import java.util.UUID;

public record BikerEnableDto(
        UUID id
) implements Serializable {
}