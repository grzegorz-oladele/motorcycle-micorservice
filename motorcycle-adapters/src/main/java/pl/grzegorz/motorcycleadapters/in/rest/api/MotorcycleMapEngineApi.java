package pl.grzegorz.motorcycleadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MapEngineDto;

@RequestMapping("/motorcycles")
public interface MotorcycleMapEngineApi {

    @PatchMapping("/{motorcycleId}/bikers/{bikerId}/map-engine")
    ResponseEntity<Void> mapEngine(
            @PathVariable("motorcycleId") String motorcycleId,
            @PathVariable("bikerId") String bikerId,
            @RequestBody MapEngineDto mapEngineDto
    );
}