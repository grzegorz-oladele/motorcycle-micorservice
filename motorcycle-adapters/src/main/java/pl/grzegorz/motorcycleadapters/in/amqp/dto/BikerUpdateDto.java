package pl.grzegorz.motorcycleadapters.in.amqp.dto;

import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerUpdateCommandUseCase.BikerUpdateCommand;

import java.io.Serializable;
import java.util.UUID;

public record BikerUpdateDto(
        UUID id,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber
) implements Serializable {

    public BikerUpdateCommand toUpdateCommand() {
        return BikerUpdateCommand.builder()
                .withId(id)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withEmail(email)
                .withEnable(enable)
                .withPhoneNumber(phoneNumber)
                .build();
    }
}