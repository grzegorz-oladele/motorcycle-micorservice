package pl.grzegorz.motorcycleadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleOwnershipExchangeProcessInitApi;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessUseCase;

@RestController
@RequiredArgsConstructor
class MotorcycleOwnershipExchangeProcessInitController implements MotorcycleOwnershipExchangeProcessInitApi {

    private final MotorcycleOwnerUpdateInitProcessUseCase motorcycleOwnerUpdateInitProcessUseCase;

    @Override
    public ResponseEntity<String> initExchangeProcess(String motorcycleId, String ownerId, String recipientId) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(motorcycleOwnerUpdateInitProcessUseCase.init(motorcycleId, ownerId, recipientId));
    }
}
