package pl.grzegorz.motorcycleadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;

@RequestMapping("/motorcycles")
public interface MotorcycleByIdApi {

    @GetMapping("/{motorcycleId}/bikers/{bikerId}")
    ResponseEntity<MotorcycleView> getMotorcycleById(
            @PathVariable("motorcycleId") String motorcycleId,
            @PathVariable("bikerId") String bikerId
    );
}