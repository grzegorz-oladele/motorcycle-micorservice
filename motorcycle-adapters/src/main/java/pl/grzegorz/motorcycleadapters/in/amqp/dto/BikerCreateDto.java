package pl.grzegorz.motorcycleadapters.in.amqp.dto;

import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerCreateCommandUseCase.BikerCreateCommand;

import java.io.Serializable;
import java.util.UUID;

public record BikerCreateDto(
        UUID id,
        String username,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber,
        String dateOfBirth
) implements Serializable {

    public BikerCreateCommand toCreateCommand() {
        return BikerCreateCommand.builder()
                .withId(id)
                .withUsername(username)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withEmail(email)
                .withEnable(enable)
                .withPhoneNumber(phoneNumber)
                .withDateOfBirth(dateOfBirth)
                .build();
    }
}