package pl.grzegorz.motorcycleadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/motorcycles")
public interface MotorcycleSetUnactiveApi {

    @PutMapping("/{motorcycleId}/bikers/{bikerId}")
    ResponseEntity<Void> setMotorcycleAsUnActive(
            @PathVariable("motorcycleId") String motorcycleId,
            @PathVariable("bikerId") String bikerId
    );
}