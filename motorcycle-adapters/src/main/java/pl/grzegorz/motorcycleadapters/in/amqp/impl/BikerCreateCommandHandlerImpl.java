package pl.grzegorz.motorcycleadapters.in.amqp.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerCreateDto;
import pl.grzegorz.motorcycleadapters.in.amqp.handler.BikerCreateCommandHandler;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerCreateCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerCreateCommandHandlerImpl implements BikerCreateCommandHandler {

    private final BikerCreateCommandUseCase bikerCreateCommandUseCase;

    @Override
    public void create(BikerCreateDto bikerCreateDto) {
        bikerCreateCommandUseCase.create(bikerCreateDto.toCreateCommand());
    }
}