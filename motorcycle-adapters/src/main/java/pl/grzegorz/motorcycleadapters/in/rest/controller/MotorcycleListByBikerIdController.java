package pl.grzegorz.motorcycleadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleListByBikerIdApi;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcycleListByBikerByIdQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

@RestController
@RequiredArgsConstructor
class MotorcycleListByBikerIdController implements MotorcycleListByBikerIdApi {

    private final MotorcycleListByBikerByIdQueryUseCase motorcycleListByBikerByIdQueryUseCase;

    @Override
    public ResponseEntity<MotorcycleListByBikerIdView> getAllByBikerId(String bikerId, ResourceFilters filter) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(motorcycleListByBikerByIdQueryUseCase.findAllByBikerId(bikerId, filter));
    }
}