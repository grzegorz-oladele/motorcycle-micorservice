package pl.grzegorz.motorcycleadapters.in.amqp.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerEnableDto;
import pl.grzegorz.motorcycleadapters.in.amqp.handler.BikerDisableCommandHandler;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerDisableCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerDisableCommandHandlerImpl implements BikerDisableCommandHandler {

    private final BikerDisableCommandUseCase bikerDisableCommandUseCase;

    @Override
    public void handle(BikerEnableDto bikerEnableDto) {
        bikerDisableCommandUseCase.disable(bikerEnableDto.id());
    }
}