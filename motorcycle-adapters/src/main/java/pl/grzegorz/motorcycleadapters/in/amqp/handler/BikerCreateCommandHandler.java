package pl.grzegorz.motorcycleadapters.in.amqp.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerCreateDto;

public interface BikerCreateCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.biker.create.queue}")
    void create(BikerCreateDto bikerCreateDto);
}