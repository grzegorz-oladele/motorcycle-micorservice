package pl.grzegorz.motorcycleadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleMapEngineApi;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MapEngineDto;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleMapEngineUseCase;

@RestController
@RequiredArgsConstructor
public class MotorcycleMapEngineController implements MotorcycleMapEngineApi {

    private final MotorcycleMapEngineUseCase motorcycleMapEngineUseCase;

    @Override
    public ResponseEntity<Void> mapEngine(String motorcycleId, String bikerId, MapEngineDto mapEngineDto) {
        motorcycleMapEngineUseCase.map(motorcycleId, bikerId, mapEngineDto.toMapEngineCommand());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}