package pl.grzegorz.motorcycleadapters.in.amqp.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerUpdateDto;

public interface BikerUpdateCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.biker.update.queue}")
    void handle(BikerUpdateDto bikerUpdateDto);
}