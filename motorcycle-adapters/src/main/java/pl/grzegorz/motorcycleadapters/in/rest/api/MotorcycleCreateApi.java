package pl.grzegorz.motorcycleadapters.in.rest.api;

import jakarta.validation.Valid;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MotorcycleDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.UUID;

@RequestMapping("/motorcycles")
public interface MotorcycleCreateApi {

    @PostMapping("/{bikerId}/bikers")
    ResponseEntity<UUID> createMotorcycle(
            @PathVariable("bikerId") String bikerId,
            @Valid @RequestBody MotorcycleDto motorcycleDto
    );
}