package pl.grzegorz.motorcycleadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/motorcycles")
public interface MotorcycleOwnershipExchangeProcessInitApi {

    @PostMapping("/{motorcycleId}/owners/{ownerId}/recipients/{recipientId}/exchange-init")
    ResponseEntity<String> initExchangeProcess(@PathVariable("motorcycleId") String motorcycleId,
                                             @PathVariable("ownerId") String ownerId,
                                             @PathVariable("recipientId") String recipientId);
}