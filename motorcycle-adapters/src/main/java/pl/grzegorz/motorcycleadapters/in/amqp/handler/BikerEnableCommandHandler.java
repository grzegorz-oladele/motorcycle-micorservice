package pl.grzegorz.motorcycleadapters.in.amqp.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerEnableDto;

public interface BikerEnableCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.biker.enable.queue}")
    void handle(BikerEnableDto bikerEnableDto);
}