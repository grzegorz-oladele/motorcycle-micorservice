package pl.grzegorz.motorcycleadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleCreateApi;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MotorcycleDto;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleCreateCommandUseCase;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class MotorcycleCreateController implements MotorcycleCreateApi {

    private final MotorcycleCreateCommandUseCase motorcycleCreateCommandUseCase;

    @Override
    public ResponseEntity<UUID> createMotorcycle(String bikerId, MotorcycleDto motorcycleDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(motorcycleCreateCommandUseCase.create(motorcycleDto.toCreateCommand(bikerId)));
    }
}