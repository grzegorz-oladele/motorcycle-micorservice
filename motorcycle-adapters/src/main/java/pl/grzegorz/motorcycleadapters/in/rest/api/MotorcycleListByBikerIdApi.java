package pl.grzegorz.motorcycleadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

@RequestMapping("/motorcycles")
public interface MotorcycleListByBikerIdApi {

    @GetMapping("/{bikerId}/bikers")
    ResponseEntity<MotorcycleListByBikerIdView> getAllByBikerId(
            @PathVariable("bikerId") String bikerId,
            ResourceFilters filter
    );
}
