package pl.grzegorz.motorcycleadapters.in.rest.controller;

import org.springframework.http.HttpStatus;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleByIdApi;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcycleByIdQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;

@RestController
@RequiredArgsConstructor
public class MotorcycleByIdController implements MotorcycleByIdApi {

    private final MotorcycleByIdQueryUseCase motorcycleByIdQueryUseCase;

    @Override
    public ResponseEntity<MotorcycleView> getMotorcycleById(String motorcycleId, String bikerId) {
        return ResponseEntity.status(HttpStatus.OK).body(motorcycleByIdQueryUseCase.findById(motorcycleId, bikerId));
    }
}