package pl.grzegorz.motorcycleadapters.in.amqp.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.motorcycleadapters.in.amqp.dto.BikerEnableDto;

public interface BikerDisableCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.biker.disable.queue}")
    void handle(BikerEnableDto bikerEnableDto);
}