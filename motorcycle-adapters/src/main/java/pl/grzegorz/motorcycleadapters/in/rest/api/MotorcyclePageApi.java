package pl.grzegorz.motorcycleadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;

@RequestMapping("/motorcycles")
public interface MotorcyclePageApi {

    @GetMapping
    ResponseEntity<ResultViewInfo<MotorcycleView>> findAll(ResourceFilters filter);
}