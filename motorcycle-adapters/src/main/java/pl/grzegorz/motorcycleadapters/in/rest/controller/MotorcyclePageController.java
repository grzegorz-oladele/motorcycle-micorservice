package pl.grzegorz.motorcycleadapters.in.rest.controller;

import org.springframework.http.HttpStatus;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcyclePageApi;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcyclePageQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

@RestController
@RequiredArgsConstructor
public class MotorcyclePageController implements MotorcyclePageApi {

    private final MotorcyclePageQueryUseCase motorcyclePageQueryUseCase;

    @Override
    public ResponseEntity<ResultViewInfo<MotorcycleView>> findAll(ResourceFilters filter) {
        return ResponseEntity.status(HttpStatus.OK).body(motorcyclePageQueryUseCase.findAll(filter));
    }
}