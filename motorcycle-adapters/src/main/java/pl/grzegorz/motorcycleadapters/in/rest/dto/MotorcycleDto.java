package pl.grzegorz.motorcycleadapters.in.rest.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleCreateCommandUseCase.MotorcycleCreateCommand;
import pl.grzegorz.motorcycledomain.exception.InvalidVintageException;

import java.time.LocalDate;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleDto(
        @NotBlank(message = "Brand must not be null")
        String brand,
        @NotBlank(message = "Model must not be null")
        String model,
        int capacity,
        int horsePower,
        int vintage,
        @NotBlank(message = "Motorcycle class must not be blank")
        String motorcycleClass,
        @NotBlank(message = "Serial number must not be blank")
        String serialNumber
) {

    public MotorcycleCreateCommand toCreateCommand(String bikerId) {
        return MotorcycleCreateCommand.builder()
                .withBikerId(UUID.fromString(bikerId))
                .withBrand(brand)
                .withModel(model)
                .withCapacity(capacity)
                .withHorsePower(horsePower)
                .withVintage(checkVintageValue(vintage))
                .withMotorcycleClass(motorcycleClass.toUpperCase())
                .withSerialNumber(serialNumber)
                .build();
    }

    private int checkVintageValue(int vintage) {
        if (vintage > LocalDate.now().getYear()) {
            throw new InvalidVintageException("Vintage value cannot be greater than current year");
        }
        return vintage;
    }
}