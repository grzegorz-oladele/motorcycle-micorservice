package pl.grzegorz.motorcycleadapters.in.rest.controller;

import org.springframework.http.HttpStatus;
import pl.grzegorz.motorcycleadapters.in.rest.api.MotorcycleSetUnactiveApi;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleSetUnactiveCommandUseCase;

@RestController
@RequiredArgsConstructor
public class MotorcycleSetUnactiveController implements MotorcycleSetUnactiveApi {

    private final MotorcycleSetUnactiveCommandUseCase motorcycleSetUnActiveCommandUseCase;

    @Override
    public ResponseEntity<Void> setMotorcycleAsUnActive(String motorcycleId, String bikerId) {
        motorcycleSetUnActiveCommandUseCase.setUnactive(motorcycleId, bikerId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}