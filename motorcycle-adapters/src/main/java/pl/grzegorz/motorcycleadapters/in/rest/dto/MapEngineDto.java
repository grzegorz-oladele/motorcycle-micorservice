package pl.grzegorz.motorcycleadapters.in.rest.dto;

import lombok.Builder;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleMapEngineUseCase.MotorcycleMapEngineCommand;

@Builder(setterPrefix = "with")
public record MapEngineDto(
        int capacity,
        int horsePower
) {

    public MotorcycleMapEngineCommand toMapEngineCommand() {
        return MotorcycleMapEngineCommand.builder()
                .withHorsePower(horsePower)
                .withCapacity(capacity)
                .build();
    }
}