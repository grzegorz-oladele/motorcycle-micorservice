package pl.grzegorz.motorcycleadapters.exception.handler;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import pl.grzegorz.motorcycledomain.exception.*;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
class GlobalExceptionHandler {

    private final HttpServletRequest request;

    @ExceptionHandler({MotorcycleNotFoundException.class, BikerNotFoundException.class,
            HttpClientErrorException.NotFound.class, ExchangeConfirmationNotFoundException.class,
            MotorcycleOwnerUpdateProcessNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorResponse handleMotorcycleNotFoundException(DomainException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, InvalidMotorcycleClassException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleMethodArgumentNotValidExceptionException(Exception exception) {
        if (exception instanceof MethodArgumentNotValidException) {
            return ErrorResponse.getErrorResponse(getDefaultErrorMessage((MethodArgumentNotValidException) exception),
                    getCurrentPath(), HttpStatus.BAD_REQUEST.value());
        }
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({BikerPermissionException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    ErrorResponse handleBikerPermissionException(DomainException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.FORBIDDEN.value());
    }

    @ExceptionHandler({InvalidVintageException.class, DataIntegrityViolationException.class, BikerAlreadyOwnerException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    ErrorResponse handleConflictUseCaseException(Exception exception) {
        if (exception instanceof MotorcycleAlreadyUnactiveException ||
                exception instanceof BikerAlreadyOwnerException) {
            return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.CONFLICT.value());
        }
        var errorMessage = resolveErrorMessageByException(exception);
        return ErrorResponse.getErrorResponse(errorMessage, getCurrentPath(),
                HttpStatus.CONFLICT.value());
    }

    @ExceptionHandler({BikerActiveException.class, MotorcycleAlreadyUnactiveException.class})
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    ErrorResponse handleBikerActiveException(Exception exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(),
                HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    private String resolveErrorMessageByException(Exception exception) {
        String errorMessage = null;
        if (exception instanceof DataIntegrityViolationException) {
            var exceptionMessage = exception.getMessage();
            var startPattern = "(";
            var endPattern = ") already exists.";

            int startIndex = exceptionMessage.indexOf(startPattern);
            int endIndex = exceptionMessage.indexOf(endPattern);

            if (startIndex != -1 && endIndex != -1) {
                errorMessage = String.format("Motorcycle with %s",
                        exceptionMessage.substring(startIndex, endIndex + endPattern.length())
                                .replace("(", "")
                                .replace(")", ""));
            } else {
                errorMessage = exception.getMessage();
            }
        }
        return errorMessage;
    }

    private String getDefaultErrorMessage(MethodArgumentNotValidException exception) {
        return exception.getAllErrors().get(0).getDefaultMessage();
    }

    private String getCurrentPath() {
        return request.getServletPath();
    }
}