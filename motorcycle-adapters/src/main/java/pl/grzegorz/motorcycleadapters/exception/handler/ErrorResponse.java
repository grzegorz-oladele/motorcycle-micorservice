package pl.grzegorz.motorcycleadapters.exception.handler;

import lombok.AccessLevel;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder(access = AccessLevel.PRIVATE)
record ErrorResponse(
        String message,
        String path,
        int responseCode,
        LocalDateTime timestamp
) {

    static ErrorResponse getErrorResponse(String message, String path, int responseCode) {
        return ErrorResponse.builder()
                .message(message)
                .path(path)
                .responseCode(responseCode)
                .timestamp(LocalDateTime.now())
                .build();
    }
}