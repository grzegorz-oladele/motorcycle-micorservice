package pl.grzegorz.motorcycleadapters.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.query.MotorcycleViewEntity;
import pl.grzegorz.motorcycleapplication.resources.PageInfo;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerPermissionException;
import pl.grzegorz.motorcycledomain.exception.MotorcycleAlreadyUnactiveException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

import java.util.Objects;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MotorcycleApplicationHelper {

    public Pageable getPageRequest(ResourceFilters filters) {
        if (Objects.isNull(filters.page()) || Objects.isNull(filters.size())) {
            filters = ResourceFilters.empty();
        }
        return PageRequest.of(filters.page(), filters.size());
    }

    public PageInfo toPageInfo(Pageable pageable, Page<MotorcycleViewEntity> motorcyclePage) {
        return PageInfo.builder()
                .actualPage(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .totalPages(motorcyclePage.getTotalPages())
                .totalRecordCount(motorcyclePage.getTotalElements())
                .build();
    }

    public PageInfo toEmptyPageInfo() {
        return PageInfo.builder()
                .actualPage(0)
                .pageSize(10)
                .totalPages(0)
                .totalRecordCount(0)
                .build();
    }

    public void checkBikerPermission(String bikerId, MotorcycleAggregate motorcycleAggregate) {
        checkIsMotorcycleUnactive(motorcycleAggregate);
        checkBikerIsOwner(bikerId, motorcycleAggregate);
    }

    private void checkIsMotorcycleUnactive(MotorcycleAggregate motorcycleAggregate) {
        if (Boolean.FALSE.equals(motorcycleAggregate.isActive())) {
            throw new MotorcycleAlreadyUnactiveException(
                    String.format(ExceptionMessage.MOTORCYCLE_ALREADY_UNACTIVE_EXCEPTION_MESSAGE.getMessage(),
                            motorcycleAggregate.id()));
        }
    }

    private void checkBikerIsOwner(String bikerId, MotorcycleAggregate motorcycleAggregate) {
        if (!bikerId.equals(motorcycleAggregate.bikerId().toString())) {
            throw new BikerPermissionException(
                    String.format(ExceptionMessage.BIKER_PERMISSION_EXCEPTION_MESSAGE.getMessage(), bikerId,
                            motorcycleAggregate.id().toString()));
        }
    }
}