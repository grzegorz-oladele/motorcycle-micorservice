# motorcycle-microservice



## Description
The motorcycle-microservice is responsible for aggregating motorcycles owned by users of the application. It allows 
adding, aggregating, filtering, and displaying details of each motorcycle. It closely collaborates with the 
biker-microservice and retrieves information about the owner when invoking the relevant endpoint.

## Technologies Used

- [ ] Spring Boot 3.x.x
- [ ] Hibernate
- [ ] PostgreSQL
- [ ] JUnit
- [ ] Testcontainers
- [ ] RabbitMQ

## Example Usage
To demonstrate the usage of the motorcycle-microservice, consider the following example:

- [ ] Endpoint: <span style="color:green">GET</span> /motorcycles?page=0&size=10
    - Description: Retrieve details of motorcycles in the application.
    - Response: Returns a list of motorcycles with their details, including the associated owner information.
  
- [ ] Endpoint: <span style="color:green">GET</span> /motorcycles/`{motorcyclesId}`/bikers/`{bikerId}`
    - Description: Retrieve details of a specific motorcycle based on the ID
    - Parameters: 
        - `{motorcycleId}` - The ID of the motorcycle
        - `{bikerId}` - The ID of the owner of motorcycle
    - Response: Returns the details of the specified motorcycle, including the associated owner information.
  
- [ ] Endpoint: <span style="color:green">GET</span> /motorcycles/`{motorcycleId}`/bikers/`{bikerId}`/exists
    - Description: Retrieve information about motorcycle exists in the application
    - Parameters:
        - `{motorcycleId}` - The ID of the motorcycle
        - `{bikerId}` - The ID of the owner of motorcycle
    - Response: Returns a true/false information about motorcycle exists in the application
  
- [ ] Endpoint: <span style="color:green">GET</span> /motorcycles/`{bikerId}`/bikers/
    - Description: Retrieve details of motorcycles in the application by biker (owner) ID
    - Parameters:
        - `{bikerId}` - The ID of the owner of motorcycle
    - Response: Returns motorcycles belonging to the motorcyclist in question
  
- [ ] Endpoint: <span style="color:orange">POST</span>/motorcycles/`{bikerId}`/bikers
    - Description: Add a new motorcycle to the application.
    - Request Body: Provide the necessary motorcycle information
    - Parameters:
      - `{bikerId}` - The ID of the owner of motorcycle
    - Response: Returns the ID of the newly created motorcycle.

- [ ] Endpoint: <span style="color:purple">PATCH</span>/motorcycles/`{motorcycleId}`/bikers/`{bikerId}`
    - Description: Update motorcycle in the application
    - Request Body: Provide the necessary motorcycle information
    - Parameters:
      - `{motorcycleId}` - The ID of the motorcycle
      - `{bikerId}` - The ID of the owner of motorcycle

- [ ] Endpoint: <span style="color:royalblue">PUT</span>/motorcycles/`{motorcycleId}`/bikers/`{bikerId}`
    - Description: Set unactive motorcycle in the application
    - Parameters:
        - `{motorcycleId}` - The ID of the motorcycle
        - `{bikerId}` - The ID of the owner of motorcycle
  
## Architecture
The project follows Domain-Driven Design (DDD) and event storming principles. It consists of four modules:

- [ ] **server**: Responsible for application startup, integration tests, and management of environment variables.
- [ ] **domain**: The core module that contains key objects and dedicated methods for working with those objects.
- [ ] **application**: Exposes application use cases.
- [ ] **adapters**: Handles communication with external systems and other microservices within the cluster.

## Custom Pipeline
This repository includes a custom pipeline that automates the build, testing, and deployment process for the application
The pipeline is designed to perform the following steps:

1. Build and test:
    - [ ] The pipeline triggers a build process using Maven to compile and package the application.
    - [ ] It runs the tests to ensure the code quality and functionality.
    - [ ] If the build and tests pass successfully, the pipeline proceeds to the next step.
    - [ ] In case the testing process fails, the pipeline terminates, and the test logs are saved in the newly created
      **errors.txt** file

2. Commit and push:
    - [ ] The pipeline commits and pushes the changes to the Git repository on GitLab.
    - [ ] It includes the commit message provided as an argument when running the pipeline.

3. Docker image build and push:
    - [ ] The pipeline builds a Docker image of the application.
    - [ ] It tags the Docker image with the first 6 characters of the current commit's hash.
    - [ ] If the optional -d (docker) argument is present when running the pipeline, it automatically pushes the Docker
      image to the Docker Hub repository.

***Example command execution:*** `motorcycle-build -cm"commit message" -d`

Before each pipeline execution, the script checks the existence of the pipeline folder and removes it if it already
exists. This ensures a clean environment for the pipeline to run successfully.

Please note that the pipeline assumes the necessary dependencies, such as Maven, Git, and Docker, are properly
installed and configured on the build environment.

## Project Status
The motorcycle-microservice is actively being actively developed. We are constantly working on adding new features including:
- [ ] implementation of the CQRS pattern
- [ ] addition and implementation of Liquibase library
- [ ] implementation of security using the Spring Security framework
- [ ] increasing test coverage of key application functionalities

# Author
**Grzegorz Oladele**

Thank you for your interest in the motorcycle-microservice project! If you have any questions or feedback, please
feel free to contact us.
