package pl.grzegorz.motorcycledomain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;
import pl.grzegorz.motorcycledomain.data.biker.BikerCreateData;
import pl.grzegorz.motorcycledomain.data.biker.BikerUpdateData;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleCreateData;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleMapEngineData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    private static final UUID BIKER_ID = UUID.fromString("c90fd34d-1ae5-4ae4-8ccd-2eadf7e01fcd");
    private static final UUID MOTORCYCLE_ID = UUID.fromString("6044a914-bf11-4cc4-bc81-6f74d5840db5");
    private static final UUID MOTORCYCLE_OWNER_UPDATE_CONFIRMATION_STATUS_ID =
            UUID.fromString("5356b69d-b806-4f6f-acaf-52585f98de6d");
    private static final UUID MOTORCYCLE_OWNER_EXCHANGE_PROCESS_ID =
            UUID.fromString("59b1a7ea-1df4-47ca-9f5f-6e51e1ab1554");
    private static final UUID RECIPIENT_ID = UUID.fromString("0ce44550-50e3-45fe-98eb-7b88636e56aa");

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withIsActive(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static BikerAggregate disableBikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withIsActive(Boolean.FALSE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static BikerCreateData bikerCreateData() {
        return BikerCreateData.builder()
                .withId(BIKER_ID)
                .withUsername("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23).toString())
                .build();
    }

    public static BikerUpdateData bikerUpdateData() {
        return BikerUpdateData.builder()
                .withId(BIKER_ID)
                .withUsername("biker_123")
                .withFirstName("Patryk")
                .withLastName("Patrykowski")
                .withEmail("patryk@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("465-039-375")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withSerialNumber("IBUWFCE782BJKCWE")
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static MotorcycleCreateData motorcycleCreateData() {
        return MotorcycleCreateData.builder()
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withSerialNumber("IBUWFCE782BJKCWE")
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT.toString())
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static MotorcycleMapEngineData motorcycleMapEngineData() {
        return MotorcycleMapEngineData.builder()
                .withHorsePower(235)
                .withCapacity(1300)
                .build();
    }

    public static MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate() {
        return MotorcycleOwnerUpdateConfirmationStatusAggregate.builder()
                .withId(MOTORCYCLE_OWNER_UPDATE_CONFIRMATION_STATUS_ID)
                .withExchangeProcessId(MOTORCYCLE_OWNER_EXCHANGE_PROCESS_ID)
                .withRecipientId(RECIPIENT_ID)
                .withConfirmationToken(UUID.randomUUID().toString())
                .withIsConfirm(Boolean.FALSE)
                .build();
    }

    public static MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate() {
        return MotorcycleOwnerUpdateStatusAggregate.builder()
                .withId(MOTORCYCLE_OWNER_EXCHANGE_PROCESS_ID)
                .withMotorcycleId(MOTORCYCLE_ID)
                .withOwnerTransferorId(BIKER_ID)
                .withRecipientId(RECIPIENT_ID)
                .withProcessCompleted(Boolean.FALSE)
                .build();
    }
}