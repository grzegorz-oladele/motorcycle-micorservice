package pl.grzegorz.motorcycledomain.aggregates;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static pl.grzegorz.motorcycledomain.Fixtures.motorcycleOwnerUpdateStatusAggregate;

class MotorcycleOwnerUpdateStatusAggregateTest {

    private MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateStatusAggregate = motorcycleOwnerUpdateStatusAggregate();
    }

    @Test
    void shouldCreateMotorcycleOwnerUpdateStatusAggregateObject() {
//        given
        var motorcycleId = motorcycleOwnerUpdateStatusAggregate.motorcycleId().toString();
        var ownerId = motorcycleOwnerUpdateStatusAggregate.ownerTransferorId().toString();
        var recipientId = motorcycleOwnerUpdateStatusAggregate.recipientId().toString();
//        when
        var aggregate = MotorcycleOwnerUpdateStatusAggregate.initUpdateProcess(motorcycleId, ownerId, recipientId);
//        then
        assertAll(
                () -> assertThat(aggregate.id(), Matchers.instanceOf(UUID.class)),
                () -> assertThat(aggregate.motorcycleId(), is(UUID.fromString(motorcycleId))),
                () -> assertThat(aggregate.ownerTransferorId(), is(UUID.fromString(ownerId))),
                () -> assertThat(aggregate.recipientId(), is(UUID.fromString(recipientId))),
                () -> assertThat(aggregate.processCompleted(), is(Boolean.FALSE))
        );
    }

    @Test
    void shouldFinishProcess() {
//        given
        var exchangeProcessId = motorcycleOwnerUpdateStatusAggregate.id().toString();
//        when
        motorcycleOwnerUpdateStatusAggregate.finishUpdateProcess(exchangeProcessId);
//        then
        assertThat(motorcycleOwnerUpdateStatusAggregate.processCompleted(), is(Boolean.TRUE));
    }
}