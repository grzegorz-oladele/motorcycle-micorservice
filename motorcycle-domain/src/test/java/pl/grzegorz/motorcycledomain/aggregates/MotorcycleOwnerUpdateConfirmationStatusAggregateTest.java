package pl.grzegorz.motorcycledomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static pl.grzegorz.motorcycledomain.Fixtures.motorcycleOwnerUpdateConfirmationStatusAggregate;

class MotorcycleOwnerUpdateConfirmationStatusAggregateTest {

    private MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateConfirmationStatusAggregate = motorcycleOwnerUpdateConfirmationStatusAggregate();
    }

    @Test
    void shouldCreateMotorcycleOwnerUpdateConfirmationStatusAggregateObject() {
//        given
        var exchangeProcessId = motorcycleOwnerUpdateConfirmationStatusAggregate.exchangeProcessId();
        var recipientId = motorcycleOwnerUpdateConfirmationStatusAggregate.recipientId();
//        when
        var aggregate = MotorcycleOwnerUpdateConfirmationStatusAggregate.create(exchangeProcessId, recipientId);
//        then
        assertAll(
                () -> assertThat(aggregate.id(), instanceOf(UUID.class)),
                () -> assertThat(aggregate.exchangeProcessId(), is(exchangeProcessId)),
                () -> assertThat(aggregate.recipientId(), is(recipientId)),
                () -> assertThat(aggregate.confirmationToken(), instanceOf(String.class)),
                () -> assertThat(aggregate.isConfirm(), is(Boolean.FALSE))
        );
    }

    @Test
    void shouldConfirmExchangeProcess() {
//        given
//        when
        motorcycleOwnerUpdateConfirmationStatusAggregate.confirm();
//        then
        assertThat(motorcycleOwnerUpdateConfirmationStatusAggregate.isConfirm(), is(Boolean.TRUE));
    }
}