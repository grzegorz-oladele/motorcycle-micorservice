package pl.grzegorz.motorcycledomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleCreateData;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleMapEngineData;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static pl.grzegorz.motorcycledomain.Fixtures.*;

class MotorcycleAggregateTest {

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleCreateData motorcycleCreateData;
    private MotorcycleMapEngineData motorcycleMapEngineData;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleCreateData = motorcycleCreateData();
        motorcycleMapEngineData = motorcycleMapEngineData();
    }

    @Test
    void shouldCreateMotorcycleAggregateObject() {
//        given
//        when
        var aggregate = MotorcycleAggregate.create(motorcycleCreateData);
//        then
        assertAll(
                () -> assertThat(aggregate.id(), instanceOf(UUID.class)),
                () -> assertThat(aggregate.bikerId(), is(motorcycleCreateData.bikerId())),
                () -> assertThat(aggregate.brand(), is(motorcycleCreateData.brand())),
                () -> assertThat(aggregate.model(), is(motorcycleCreateData.model())),
                () -> assertThat(aggregate.capacity(), is(motorcycleCreateData.capacity())),
                () -> assertThat(aggregate.horsePower(), is(motorcycleCreateData.horsePower())),
                () -> assertThat(aggregate.vintage(), is(motorcycleCreateData.vintage())),
                () -> assertThat(aggregate.serialNumber(), is(motorcycleCreateData.serialNumber())),
                () -> assertThat(aggregate.motorcycleClass(), is(MotorcycleClass.valueOf(motorcycleCreateData.motorcycleClass()))),
                () -> assertThat(aggregate.isActive(), is(Boolean.TRUE))
        );
    }

    @Test
    void shouldMapEngine() {
//        given
//        when
        motorcycleAggregate.mapEngine(motorcycleMapEngineData);
//        then
        assertAll(
                () -> assertThat(motorcycleAggregate.capacity(), is(motorcycleMapEngineData.capacity())),
                () -> assertThat(motorcycleAggregate.horsePower(), is(motorcycleMapEngineData.horsePower()))
        );
    }

    @Test
    void shouldSetAsUnactive() {
//        given
//        when
        motorcycleAggregate.setUnActive();
//        then
        assertThat(motorcycleAggregate.isActive(), is(Boolean.FALSE));
    }

    @Test
    void shouldChangeOwner() {
//        given
        var newOwnerId = UUID.randomUUID();
        var previousOwnerId = motorcycleAggregate.bikerId();
//        when
        motorcycleAggregate.changeOwner(newOwnerId);
//        then
        assertAll(
                () -> assertThat(motorcycleAggregate.bikerId(), is(newOwnerId)),
                () -> assertThat(motorcycleAggregate.bikerId(), is(not(previousOwnerId)))
        );
    }
}