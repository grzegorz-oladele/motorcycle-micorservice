package pl.grzegorz.motorcycledomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.motorcycledomain.data.biker.BikerCreateData;
import pl.grzegorz.motorcycledomain.data.biker.BikerUpdateData;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static pl.grzegorz.motorcycledomain.Fixtures.*;

class BikerAggregateTest {

    private BikerAggregate bikerAggregate;
    private BikerAggregate disableBikerAggregate;
    private BikerCreateData bikerCreateData;
    private BikerUpdateData bikerUpdateData;

    @BeforeEach
    void setup() {
        bikerAggregate = bikerAggregate();
        disableBikerAggregate = disableBikerAggregate();
        bikerCreateData = bikerCreateData();
        bikerUpdateData = bikerUpdateData();
    }

    @Test
    void shouldCreateNewBikerAggregateObject() {
//        given
//        when
        var aggregate = BikerAggregate.create(bikerCreateData);
//        then
        assertAll(
                () -> assertThat(aggregate.id(), is(bikerCreateData.id())),
                () -> assertThat(aggregate.userName(), is(bikerCreateData.username())),
                () -> assertThat(aggregate.firstName(), is(bikerCreateData.firstName())),
                () -> assertThat(aggregate.lastName(), is(bikerCreateData.lastName())),
                () -> assertThat(aggregate.email(), is(bikerCreateData.email())),
                () -> assertThat(aggregate.isActive(), is(bikerCreateData.enable())),
                () -> assertThat(aggregate.phoneNumber(), is(bikerCreateData.phoneNumber())),
                () -> assertThat(aggregate.dateOfBirth(), is(LocalDate.parse(bikerCreateData.dateOfBirth())))
        );
    }

    @Test
    void shouldUpdateBikerAggregate() {
//        given
//        when
        var updatedAggregate = BikerAggregate.update(bikerUpdateData);
//        then
        assertAll(
                () -> assertThat(updatedAggregate.id(), is(bikerUpdateData.id())),
                () -> assertThat(updatedAggregate.firstName(), is(bikerUpdateData.firstName())),
                () -> assertThat(updatedAggregate.lastName(), is(bikerUpdateData.lastName())),
                () -> assertThat(updatedAggregate.userName(), is(bikerUpdateData.username())),
                () -> assertThat(updatedAggregate.phoneNumber(), is(bikerUpdateData.phoneNumber())),
                () -> assertThat(updatedAggregate.isActive(), is(bikerUpdateData.enable())),
                () -> assertThat(updatedAggregate.email(), is(bikerUpdateData.email())),
                () -> assertThat(updatedAggregate.dateOfBirth(), is(bikerUpdateData.dateOfBirth()))
        );
    }

    @Test
    void shouldEnableBiker() {
//        given
//        when
        disableBikerAggregate.enable();
//        then
        assertThat(disableBikerAggregate.isActive(), is(Boolean.TRUE));
    }

    @Test
    void shouldDisableBiker() {
//        given
//        when
        bikerAggregate.disable();
//        then
        assertThat(bikerAggregate.isActive(), is(Boolean.FALSE));
    }
}