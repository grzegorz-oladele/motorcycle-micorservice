package pl.grzegorz.motorcycledomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(setterPrefix = "with")
public class MotorcycleOwnerUpdateStatusAggregate {

    private UUID id;
    private UUID motorcycleId;
    private UUID ownerTransferorId;
    private UUID recipientId;
    private Boolean processCompleted;

    public static MotorcycleOwnerUpdateStatusAggregate initUpdateProcess(String motorcycleId, String ownerId, String recipientId) {
        return MotorcycleOwnerUpdateStatusAggregate.builder()
                .withId(UUID.randomUUID())
                .withMotorcycleId(UUID.fromString(motorcycleId))
                .withOwnerTransferorId(UUID.fromString(ownerId))
                .withRecipientId(UUID.fromString(recipientId))
                .withProcessCompleted(Boolean.FALSE)
                .build();
    }

    public void finishUpdateProcess(String exchangeProcessId) {
        this.processCompleted = Boolean.TRUE;
    }
}