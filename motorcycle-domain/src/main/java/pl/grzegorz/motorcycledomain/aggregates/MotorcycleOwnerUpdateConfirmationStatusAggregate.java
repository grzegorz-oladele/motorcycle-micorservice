package pl.grzegorz.motorcycledomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(setterPrefix = "with")
public class MotorcycleOwnerUpdateConfirmationStatusAggregate {

    private UUID id;
    private UUID exchangeProcessId;
    private UUID recipientId;
    private String confirmationToken;
    private Boolean isConfirm;

    public static MotorcycleOwnerUpdateConfirmationStatusAggregate create(UUID exchangeProcessId, UUID recipientId) {
        return MotorcycleOwnerUpdateConfirmationStatusAggregate.builder()
                .withId(UUID.randomUUID())
                .withExchangeProcessId(exchangeProcessId)
                .withRecipientId(recipientId)
                .withConfirmationToken(createConfirmToken(recipientId))
                .withIsConfirm(Boolean.FALSE)
                .build();
    }

    public void confirm() {
        this.isConfirm = Boolean.TRUE;
    }

    private static String createConfirmToken(UUID recipientId) {
        return String.format("%s_%s", UUID.randomUUID(), recipientId);
    }
}