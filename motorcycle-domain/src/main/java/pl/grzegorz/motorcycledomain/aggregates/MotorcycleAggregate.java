package pl.grzegorz.motorcycledomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleCreateData;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleMapEngineData;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class MotorcycleAggregate {

    private UUID id;
    private UUID bikerId;
    private String brand;
    private String model;
    private Integer capacity;
    private Integer horsePower;
    private Integer vintage;
    private String serialNumber;
    private MotorcycleClass motorcycleClass;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private Boolean isActive;

    public static MotorcycleAggregate create(MotorcycleCreateData createData) {
        return MotorcycleAggregate.builder()
                .withId(UUID.randomUUID())
                .withBikerId(createData.bikerId())
                .withBrand(createData.brand())
                .withModel(createData.model())
                .withCapacity(createData.capacity())
                .withHorsePower(createData.horsePower())
                .withVintage(createData.vintage())
                .withSerialNumber(createData.serialNumber())
                .withMotorcycleClass(MotorcycleClass.valueOf(createData.motorcycleClass().toUpperCase()))
                .withCreatedAt(createData.createdAt())
                .withModifiedAt(createData.modifiedAt())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public void mapEngine(MotorcycleMapEngineData mapEngineData) {
        this.capacity = mapEngineData.capacity();
        this.horsePower = mapEngineData.horsePower();
        this.modifiedAt = LocalDateTime.now();
    }

    public void setUnActive() {
        this.isActive = Boolean.FALSE;
    }

    public void changeOwner(UUID newOwnerId) {
        this.bikerId = newOwnerId;
    }
}