package pl.grzegorz.motorcycledomain.event;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.NonNull;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

@Builder(access = AccessLevel.PRIVATE)
public record MotorcycleSetUnActiveEvent(
        @NonNull UUID id,
        @NonNull UUID bikerId
) implements DomainEvent {

    public static MotorcycleSetUnActiveEvent of(MotorcycleAggregate aggregate) {
        return MotorcycleSetUnActiveEvent.builder()
                .id(aggregate.id())
                .bikerId(aggregate.bikerId())
                .build();
    }
}