package pl.grzegorz.motorcycledomain.event;

import java.util.UUID;

public interface DomainEvent {
    UUID id();
}