package pl.grzegorz.motorcycledomain.event;

import lombok.Builder;
import lombok.NonNull;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleCreatedEvent(
        @NonNull UUID id,
        UUID bikerId,
        String brand,
        String model,
        Integer capacity,
        Integer horsePower,
        Integer vintage,
        String serialNumber,
        MotorcycleClass motorcycleClass
) implements DomainEvent {

    public static MotorcycleCreatedEvent of(MotorcycleAggregate aggregate) {
        return MotorcycleCreatedEvent.builder()
                .withId(aggregate.id())
                .withBikerId(aggregate.bikerId())
                .withBrand(aggregate.brand())
                .withModel(aggregate.model())
                .withCapacity(aggregate.capacity())
                .withHorsePower(aggregate.horsePower())
                .withVintage(aggregate.vintage())
                .withSerialNumber(aggregate.serialNumber())
                .withMotorcycleClass(aggregate.motorcycleClass())
                .build();
    }
}