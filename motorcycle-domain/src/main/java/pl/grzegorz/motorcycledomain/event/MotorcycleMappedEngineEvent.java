package pl.grzegorz.motorcycledomain.event;

import lombok.Builder;
import lombok.NonNull;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleMappedEngineEvent(
        @NonNull UUID id,
        UUID bikerId,
        int capacity,
        int horsePower
        ) {

    public static MotorcycleMappedEngineEvent of(MotorcycleAggregate motorcycleAggregate) {
        return MotorcycleMappedEngineEvent.builder()
                .withId(motorcycleAggregate.id())
                .withBikerId(motorcycleAggregate.bikerId())
                .withCapacity(motorcycleAggregate.capacity())
                .withHorsePower(motorcycleAggregate.horsePower())
                .build();
    }
}