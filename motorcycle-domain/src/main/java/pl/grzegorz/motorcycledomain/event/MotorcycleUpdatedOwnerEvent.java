package pl.grzegorz.motorcycledomain.event;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with", toBuilder = true)
public record MotorcycleUpdatedOwnerEvent(
        UUID motorcycleId,
        UUID newOwnerId,
        UUID previousOwnerId
) {

    public static MotorcycleUpdatedOwnerEvent of(MotorcycleAggregate motorcycleAggregate) {
        return MotorcycleUpdatedOwnerEvent.builder()
                .withMotorcycleId(motorcycleAggregate.id())
                .withNewOwnerId(motorcycleAggregate.bikerId())
                .build();
    }
}