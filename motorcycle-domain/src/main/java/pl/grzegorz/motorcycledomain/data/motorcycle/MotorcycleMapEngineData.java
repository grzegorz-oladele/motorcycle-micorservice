package pl.grzegorz.motorcycledomain.data.motorcycle;

import lombok.Builder;

@Builder(setterPrefix = "with")
public record MotorcycleMapEngineData(
        int capacity,
        int horsePower
) {
}