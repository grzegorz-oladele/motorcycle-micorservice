package pl.grzegorz.motorcycledomain.data.biker;

import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record BikerUpdateData(
        UUID id,
        String firstName,
        String lastName,
        String username,
        String phoneNumber,
        Boolean enable,
        String email,
        LocalDate dateOfBirth,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}