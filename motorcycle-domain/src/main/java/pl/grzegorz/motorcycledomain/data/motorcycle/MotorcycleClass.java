package pl.grzegorz.motorcycledomain.data.motorcycle;

public enum MotorcycleClass {

    SUPERSPORT,
    NAKED,
    HYPERNAKED,
    TOURING,
    CRUISER,
    POWER_CRUISER,
    CHOPPER,
    ADVENTURE,
    SUPER_MOTO,
    SUPER_BIKE,
    CAFE_RACER
}