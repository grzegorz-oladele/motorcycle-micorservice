package pl.grzegorz.motorcycledomain.data.biker;

import lombok.Builder;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record BikerCreateData(
        UUID id,
        String username,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber,
        String dateOfBirth,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}