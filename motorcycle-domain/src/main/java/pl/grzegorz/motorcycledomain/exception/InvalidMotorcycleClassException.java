package pl.grzegorz.motorcycledomain.exception;

public class InvalidMotorcycleClassException extends DomainException {

    public InvalidMotorcycleClassException(String message) {
        super(message);
    }
}
