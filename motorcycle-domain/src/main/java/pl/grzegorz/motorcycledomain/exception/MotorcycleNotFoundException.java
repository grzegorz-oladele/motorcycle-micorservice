package pl.grzegorz.motorcycledomain.exception;

public class MotorcycleNotFoundException extends DomainException {

    public MotorcycleNotFoundException(String message) {
        super(message);
    }
}
