package pl.grzegorz.motorcycledomain.exception;

public class BikerActiveException extends DomainException {

    public BikerActiveException(String message) {
        super(message);
    }
}
