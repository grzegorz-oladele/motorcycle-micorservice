package pl.grzegorz.motorcycledomain.exception;

public class BikerNotFoundException extends DomainException {

    public BikerNotFoundException(String message) {
        super(message);
    }
}