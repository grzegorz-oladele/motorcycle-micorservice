package pl.grzegorz.motorcycledomain.exception;

public class BikerPermissionException extends DomainException {

    public BikerPermissionException(String message) {
        super(message);
    }
}