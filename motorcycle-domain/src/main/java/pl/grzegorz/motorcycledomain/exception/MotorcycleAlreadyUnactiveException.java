package pl.grzegorz.motorcycledomain.exception;

public class MotorcycleAlreadyUnactiveException extends DomainException {
    public MotorcycleAlreadyUnactiveException(String message) {
        super(message);
    }
}