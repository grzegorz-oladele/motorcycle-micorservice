package pl.grzegorz.motorcycledomain.exception.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {

    BIKER_PERMISSION_EXCEPTION_MESSAGE("Biker using id -> [%s] hasn't permission to the motorcycle with id -> [%s]"),
    BIKER_NOT_FOUND_EXCEPTION_MESSAGE("Biker using id -> [%s] not found"),
    BIKER_ACTIVE_EXCEPTION_MESSAGE("You cannot execute this command because biker using id -> [%s] is unactive"),
    INVALID_MOTORCYCLE_CLASS_EXCEPTION_MESSAGE("Motorcycle class is not correct. You need to choose from this classes:" +
            " SUPERSPORT, NAKED, HYPERNAKED, TOURING, CRUISER, POWER_CRUISER, CHOPPER, ADVENTURE, SUPER_MOTO, SUPER_BIKE," +
            "CAFE_RACER"),
    MOTORCYCLE_ALREADY_UNACTIVE_EXCEPTION_MESSAGE("Motorcycle with id [%s] is already unactive"),
    BIKER_ALREADY_OWNER_EXCEPTION_MESSAGE("Biker using id -> [%s] is already owner of motorcycle using id -> [%s]"),
    EXCHANGE_CONFIRMATION_NOT_FOUND_EXCEPTION_MESSAGE("Confirmation using token -> [%s] not found"),
    MOTORCYCLE_EXCHANGE_PROCESS_NOT_FOUND_EXCEPTION_MESSAGE("Exchange process with id -> [%s] not found"),
    CONFIRMATION_PROCESS_ALREADY_FINISH_EXCEPTION_MESSAGE("Confirmation process updating owner of motorcycle " +
            "using id -> [%s] for biker using id -> [%s] is already finish");

    private final String message;
}