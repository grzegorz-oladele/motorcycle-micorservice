package pl.grzegorz.motorcycledomain.exception;

public class ConfirmationProcessAlreadyFinishException extends DomainException{

    public ConfirmationProcessAlreadyFinishException(String message) {
        super(message);
    }
}
