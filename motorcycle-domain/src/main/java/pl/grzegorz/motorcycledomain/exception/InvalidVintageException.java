package pl.grzegorz.motorcycledomain.exception;

public class InvalidVintageException extends DomainException {

    public InvalidVintageException(String message) {
        super(message);
    }
}