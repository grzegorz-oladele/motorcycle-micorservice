package pl.grzegorz.motorcycledomain.exception;

public class MotorcycleOwnerUpdateProcessNotFoundException extends DomainException {

    public MotorcycleOwnerUpdateProcessNotFoundException(String message) {
        super(message);
    }
}
