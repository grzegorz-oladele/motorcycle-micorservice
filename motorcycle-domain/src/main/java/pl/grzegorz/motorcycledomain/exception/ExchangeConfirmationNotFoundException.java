package pl.grzegorz.motorcycledomain.exception;

public class ExchangeConfirmationNotFoundException extends DomainException {

    public ExchangeConfirmationNotFoundException(String message) {
        super(message);
    }
}