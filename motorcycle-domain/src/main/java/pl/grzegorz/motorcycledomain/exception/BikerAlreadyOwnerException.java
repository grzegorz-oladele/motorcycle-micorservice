package pl.grzegorz.motorcycledomain.exception;

public class BikerAlreadyOwnerException extends DomainException {

    public BikerAlreadyOwnerException(String message) {
        super(message);
    }
}