package pl.grzegorz.motorcycleserviceserver.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerCreateCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerDisableCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerEnableCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerUpdateCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_confirmation.MotorcycleOwnerUpdateConfirmationConfirmUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleCreateCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleMapEngineUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleSetUnactiveCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcycleByIdQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcycleListByBikerByIdQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcyclePageQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerDisableCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerEnableCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerUpdateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleChangeOwnerCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleMapEngineCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleCreatePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleMapEnginePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleSetUnactivePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleUpdateOwnerPublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcyclePageQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationConfirmCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationCreateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.query.MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateFinishProcessPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.query.MotorcycleOwnerUpdateStatusByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.biker.command.BikerCreateCommandService;
import pl.grzegorz.motorcycleapplication.services.biker.command.BikerDisableCommandService;
import pl.grzegorz.motorcycleapplication.services.biker.command.BikerEnableCommandService;
import pl.grzegorz.motorcycleapplication.services.biker.command.BikerUpdateCommandService;
import pl.grzegorz.motorcycleapplication.services.motorcycle.commands.MotorcycleCreateCommandService;
import pl.grzegorz.motorcycleapplication.services.motorcycle.commands.MotorcycleMapEngineCommandService;
import pl.grzegorz.motorcycleapplication.services.motorcycle.commands.MotorcycleSetUnactiveCommandCommandService;
import pl.grzegorz.motorcycleapplication.services.motorcycle.query.MotorcycleByIdQueryService;
import pl.grzegorz.motorcycleapplication.services.motorcycle.query.MotorcycleListByBikeIdrByIdQueryService;
import pl.grzegorz.motorcycleapplication.services.motorcycle.query.MotorcyclePageQueryService;
import pl.grzegorz.motorcycleapplication.services.motorcycle_owner_update_confirmation.command.MotorcycleOwnerUpdateConfirmationConfirmService;
import pl.grzegorz.motorcycleapplication.services.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessService;

@Configuration
public class MotorcycleBeans {

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public MotorcycleCreateCommandUseCase motorcycleCreateCommandUseCase(pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleCreateCommandPort
                                                                                 motorcycleCreateCommandPort,
                                                                         BikerByIdQueryPort bikerByIdQueryPort,
                                                                         MotorcycleCreatePublisherPort
                                                                                 motorcycleCreatePublisherPort) {
        return new MotorcycleCreateCommandService(motorcycleCreateCommandPort, bikerByIdQueryPort,
                motorcycleCreatePublisherPort);
    }

    @Bean
    MotorcycleSetUnactiveCommandUseCase motorcycleSetUnActiveUseCase(pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleSetUnactiveCommandPort
                                                                             motorcycleSetUnActiveCommandPort,
                                                                     MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                     MotorcycleSetUnactivePublisherPort
                                                                             motorcycleSetUnactivePublisherPort) {
        return new MotorcycleSetUnactiveCommandCommandService(motorcycleSetUnActiveCommandPort, motorcycleByIdQueryPort,
                motorcycleSetUnactivePublisherPort);
    }

    @Bean
    MotorcycleByIdQueryUseCase motorcycleByIdQueryUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                          BikerByIdQueryPort bikerByIdQueryPort) {
        return new MotorcycleByIdQueryService(motorcycleByIdQueryPort, bikerByIdQueryPort);
    }

    @Bean
    MotorcyclePageQueryUseCase motorcyclePageQueryUseCase(MotorcyclePageQueryPort motorcyclePageQueryPort) {
        return new MotorcyclePageQueryService(motorcyclePageQueryPort);
    }

    @Bean
    public MotorcycleListByBikerByIdQueryUseCase motorcycleListByBikerQueryUseCase(
            MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort) {
        return new MotorcycleListByBikeIdrByIdQueryService(motorcycleListByBikerIdQueryPort);
    }

    @Bean
    public BikerCreateCommandUseCase bikerCreateCommandUseCase(BikerCreateCommandPort bikerCreateCommandPort) {
        return new BikerCreateCommandService(bikerCreateCommandPort);
    }

    @Bean
    public BikerDisableCommandUseCase bikerDisableCommandUseCase(BikerByIdQueryPort bikerByIdQueryPort,
                                                                 BikerDisableCommandPort bikerDisableCommandPort) {
        return new BikerDisableCommandService(bikerByIdQueryPort, bikerDisableCommandPort);
    }

    @Bean
    public BikerEnableCommandUseCase bikerEnableCommandUseCase(BikerByIdQueryPort bikerByIdQueryPort,
                                                               BikerEnableCommandPort bikerEnableCommandPort) {
        return new BikerEnableCommandService(bikerByIdQueryPort, bikerEnableCommandPort);
    }

    @Bean
    public BikerUpdateCommandUseCase bikerUpdateCommandUseCase(BikerByIdQueryPort bikerByIdQueryPort,
                                                               BikerUpdateCommandPort bikerUpdateCommandPort) {
        return new BikerUpdateCommandService(bikerByIdQueryPort, bikerUpdateCommandPort);
    }

    @Bean
    public MotorcycleMapEngineUseCase motorcycleMapEngineUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                 MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort,
                                                                 MotorcycleMapEnginePublisherPort motorcycleMapEnginePublisherPort,
                                                                 BikerByIdQueryPort bikerByIdQueryPort) {
        return new MotorcycleMapEngineCommandService(motorcycleByIdQueryPort, motorcycleMapEngineCommandPort,
                motorcycleMapEnginePublisherPort, bikerByIdQueryPort);
    }

    @Bean
    public MotorcycleOwnerUpdateInitProcessUseCase motorcycleOwnershipExchangeProcessInitCommandUseCase(
            MotorcycleOwnerUpdateInitProcessPort motorcycleOwnerUpdateInitProcessPort,
            BikerByIdQueryPort bikerByIdQueryPort, MotorcycleByIdQueryPort motorcycleByIdQueryPort,
            MotorcycleOwnerUpdateConfirmationCreateCommandPort motorcycleOwnerUpdateConfirmationCreateCommandPort) {
        return new MotorcycleOwnerUpdateInitProcessService(
                motorcycleOwnerUpdateInitProcessPort, bikerByIdQueryPort, motorcycleByIdQueryPort,
                motorcycleOwnerUpdateConfirmationCreateCommandPort);
    }

    @Bean
    public MotorcycleOwnerUpdateConfirmationConfirmUseCase motorcycleExchangeConfirmationConfirmCommandUseCase(
            MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort motorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort,
            MotorcycleOwnerUpdateConfirmationConfirmCommandPort motorcycleOwnerUpdateConfirmationConfirmCommandPort,
            MotorcycleOwnerUpdateStatusByIdQueryPort motorcycleOwnerUpdateStatusByIdQueryPort,
            MotorcycleOwnerUpdateFinishProcessPort motorcycleOwnerUpdateFinishProcessPort,
            MotorcycleByIdQueryPort motorcycleByIdQueryPort, BikerByIdQueryPort bikerByIdQueryPort,
            MotorcycleChangeOwnerCommandPort motorcycleChangeOwnerCommandPort, MotorcycleUpdateOwnerPublisherPort motorcycleUpdateOwnerPublisherPort) {
        return new MotorcycleOwnerUpdateConfirmationConfirmService(motorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort,
                motorcycleOwnerUpdateConfirmationConfirmCommandPort, motorcycleOwnerUpdateStatusByIdQueryPort,
                motorcycleOwnerUpdateFinishProcessPort, motorcycleByIdQueryPort, bikerByIdQueryPort,
                motorcycleChangeOwnerCommandPort, motorcycleUpdateOwnerPublisherPort);
    }
}