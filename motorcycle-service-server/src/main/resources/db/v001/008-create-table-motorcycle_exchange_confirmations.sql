CREATE TABLE motorcycle_db.motorcycles.motorcycle_exchange_confirmations
(
    id                  uuid primary key not null unique,
    exchange_process_id uuid             not null,
    recipient_id        uuid             not null,
    confirmation_token       varchar(74)      not null unique,
    is_confirm          boolean
);