CREATE OR REPLACE VIEW motorcycle_db.motorcycles.motorcycle_ownership_history_view AS
SELECT id,
       motorcycle_id,
       owner_transferor_id,
       recipient_id,
       process_completed
FROM motorcycle_db.motorcycles.motorcycle_ownership_history;