CREATE TABLE motorcycle_db.motorcycles.motorcycle_ownership_history
(
    id                  uuid primary key not null unique,
    motorcycle_id       uuid             not null,
    owner_transferor_id uuid             not null,
    recipient_id        uuid             not null,
    process_completed   boolean
);