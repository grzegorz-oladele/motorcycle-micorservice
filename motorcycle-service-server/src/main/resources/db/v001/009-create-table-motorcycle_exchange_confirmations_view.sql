CREATE OR REPLACE VIEW motorcycle_db.motorcycles.motorcycle_exchange_confirmations_view AS
SELECT id,
       exchange_process_id,
       recipient_id,
       confirmation_token,
       is_confirm
FROM motorcycle_db.motorcycles.motorcycle_exchange_confirmations;