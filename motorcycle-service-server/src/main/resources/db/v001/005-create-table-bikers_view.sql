CREATE OR REPLACE VIEW motorcycle_db.motorcycles.bikers_view AS
SELECT id,
       first_name,
       last_name,
       user_name,
       email,
       phone_number,
       date_of_birth,
       created_at,
       modified_at,
       enable
FROM motorcycle_db.motorcycles.bikers;