CREATE TABLE motorcycle_db.motorcycles.bikers
(
    id            uuid primary key not null unique,
    first_name    varchar(56),
    last_name     varchar(56),
    user_name     varchar(56) not null unique,
    email         varchar(56),
    phone_number  varchar(11),
    date_of_birth date,
    created_at    timestamp,
    modified_at   timestamp,
    enable     boolean
);