package pl.grzegorz.motorcycleserviceserver;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MapEngineDto;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MotorcycleDto;
import pl.grzegorz.motorcycleadapters.out.persistence.biker.command.BikerEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.biker.command.BikerRepository;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleRepository;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command.MotorcycleOwnerUpdateConfirmationRepository;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command.MotorcycleOwnerUpdateConfirmationStatusEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateStatusEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateStatusRepository;
import pl.grzegorz.motorcycleapplication.ports.view.BikerView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.grzegorz.motorcycleserviceserver.MotorcycleFixtures.*;

class MotorcycleControllerTest extends AbstractIntegrationTest {

    private static final String URL = "/motorcycles";
    private static final String MOTORCYCLE_BY_ID_CONTEXT_PATH = URL + "/%s/bikers/%s";
    private static final String BIKER_API_URL = "/bikers/%s/exists";
    private static final String BIKER_CONTEXT_PATH = URL + "/%s/bikers";
    private static final String ASSIGN_MOTORCYCLE_UNACTIVE_CONTEXT_PATH = URL + "/%s/bikers/%s";
    private static final String MAP_ENGINE_CONTEXT_PATH = URL + "/%s/bikers/%s/map-engine";
    private static final String MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS = URL + "/%s/owners/%s/recipients/%s/exchange-init";

    @Autowired
    private MotorcycleRepository motorcycleRepository;
    @Autowired
    private BikerRepository bikerRepository;
    @Autowired
    private MotorcycleOwnerUpdateStatusRepository motorcycleOwnerUpdateStatusRepository;
    @Autowired
    private MotorcycleOwnerUpdateConfirmationRepository motorcycleOwnerUpdateConfirmationRepository;

    private MotorcycleEntity firstMotorcycleEntity;
    private MotorcycleEntity secondMotorcycleEntity;
    private BikerEntity firstBikerEntity;
    private BikerEntity secondBikerEntity;
    private BikerEntity unactiveBikerEntity;
    private MotorcycleDto motorcycleDto;
    private MotorcycleDto motorcycleDtoWithInvalidMotorcycleClass;
    private ResourceFilters filter;
    private BikerView bikerView;
    private MapEngineDto mapEngineDto;
    private BikerEntity thirdBikerEntity;
    private MotorcycleEntity thirdMotorcycleEntity;
    private MotorcycleOwnerUpdateStatusEntity motorcycleOwnerUpdateStatusEntity;
    private MotorcycleOwnerUpdateConfirmationStatusEntity motorcycleOwnerUpdateConfirmationStatusEntity;

    @BeforeEach
    void setup() {
        firstMotorcycleEntity = motorcycleRepository.save(firstMotorcycleEntity());
        secondMotorcycleEntity = motorcycleRepository.save(secondMotorcycleEntity());
        motorcycleDto = motorcycleDto();
        filter = new ResourceFilters(null, null);
        bikerView = bikerView();
        firstBikerEntity = bikerRepository.save(firstBikerEntity());
        secondBikerEntity = bikerRepository.save(secondBikerEntity());
        unactiveBikerEntity = bikerRepository.save(unactiveBikerEntity());
        mapEngineDto = mapEngineDto();
        thirdBikerEntity = bikerRepository.save(thirdBikerEntity());
        thirdMotorcycleEntity = motorcycleRepository.save(thirdMotorcycleEntity());
        motorcycleOwnerUpdateStatusEntity = motorcycleOwnerUpdateStatusRepository.save(motorcycleOwnerUpdateStatusEntity());
        motorcycleOwnerUpdateConfirmationStatusEntity = motorcycleOwnerUpdateConfirmationRepository.save(motorcycleOwnerUpdateConfirmationStatusEntity());
    }

    @AfterEach
    void teardown() {
        motorcycleRepository.deleteAll();
        bikerRepository.deleteAll();
        motorcycleOwnerUpdateStatusRepository.deleteAll();
        motorcycleOwnerUpdateConfirmationRepository.deleteAll();
    }

    @Test
    void shouldReturnMotorcycleById() throws Exception {
        var motorcycleId = FIRST_ENTITY_ID.toString();
        var bikerId = FIRST_BIKER_ID.toString();
        mockMvc.perform(get(String.format(MOTORCYCLE_BY_ID_CONTEXT_PATH, motorcycleId, bikerId)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand", is("Ducati")))
                .andExpect(jsonPath("$.model", is("Panigale V4S")))
                .andExpect(jsonPath("$.capacity", is(1199)))
                .andExpect(jsonPath("$.horsePower", is(225)))
                .andExpect(jsonPath("$.vintage", is(2022)))
                .andExpect(jsonPath("$.motorcycleClass", is("SUPERSPORT")));
    }

    @Test
    void shouldThrowExceptionWhenBikerWillBeNotHavePermissionToBiker() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var bikerId = SECOND_BIKER_ID.toString();
        mockMvc.perform(get(String.format(MOTORCYCLE_BY_ID_CONTEXT_PATH, motorcycleId, bikerId)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldThrowExceptionWhenMotorcycleWillBeNotExistsInTheDatabase() throws Exception {
        var motorcycleId = UUID.randomUUID();
        var bikerId = firstMotorcycleEntity.bikerId();
        mockMvc.perform(get(String.format(MOTORCYCLE_BY_ID_CONTEXT_PATH, motorcycleId, bikerId)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldAddMotorcycleToDb() throws Exception {
        var mockDto = mapper.writeValueAsString(motorcycleDto);
        var bikerId = secondMotorcycleEntity.bikerId();
        mockMvc.perform(MockMvcRequestBuilders.post(String.format(BIKER_CONTEXT_PATH, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
        List<MotorcycleEntity> motorcycles = motorcycleRepository.findAll();
        assertAll(
                () -> assertThat(motorcycles.size(), is(4))
        );
    }

    @Test
    void shouldThrowInvalidMotorcycleClassExceptionWhenMotorcycleClassWillBeNotCorrect() throws Exception {
        var mockDto = mapper.writeValueAsString(motorcycleDtoWithInvalidMotorcycleClass);
        var bikerId = firstBikerEntity.id();
        mockMvc.perform(MockMvcRequestBuilders.post(String.format(BIKER_CONTEXT_PATH, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenBikerWillBeNotActive() throws Exception {
        var bikerId = unactiveBikerEntity.id();
        var mockDto = mapper.writeValueAsString(motorcycleDto);
        mockMvc.perform(MockMvcRequestBuilders.post(String.format(BIKER_CONTEXT_PATH, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(String
                        .format("You cannot execute this command because biker using id -> [%s] is unactive", bikerId))))
                .andExpect(jsonPath("$.responseCode", is(422)));
    }

    @Test
    void shouldThrowExceptionWhenBikerWillBeNotExistsInTheDatabase() throws Exception {
        var mockDto = mapper.writeValueAsString(motorcycleDto);
        var bikerId = UUID.randomUUID();
        mockMvc.perform(MockMvcRequestBuilders.post(String.format(BIKER_CONTEXT_PATH, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnPageOfMotorcycles() throws Exception {
        var content = mapper.writeValueAsString(filter);
        var bikerId = FIRST_BIKER_ID;
        mockMvc.perform(get(URL)
                        .content(content))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageInfo.pageSize", is(10)))
                .andExpect(jsonPath("$.pageInfo.actualPage", is(0)))
                .andExpect(jsonPath("$.pageInfo.totalPages", is(1)))
                .andExpect(jsonPath("$.pageInfo.totalRecordCount", is(3)))
                .andExpect(jsonPath("$.results.size()", is(3)))
                .andExpect(jsonPath("$.results[0].brand", is("Ducati")))
                .andExpect(jsonPath("$.results[0].model", is("Panigale V4S")))
                .andExpect(jsonPath("$.results[0].capacity", is(1199)))
                .andExpect(jsonPath("$.results[0].horsePower", is(225)))
                .andExpect(jsonPath("$.results[0].vintage", is(2022)))
                .andExpect(jsonPath("$.results[0].motorcycleClass", is("SUPERSPORT")))
                .andExpect(jsonPath("$.results[1].brand", is("BMW")))
                .andExpect(jsonPath("$.results[1].model", is("S1000RR")))
                .andExpect(jsonPath("$.results[1].capacity", is(999)))
                .andExpect(jsonPath("$.results[1].horsePower", is(208)))
                .andExpect(jsonPath("$.results[1].vintage", is(2021)))
                .andExpect(jsonPath("$.results[1].motorcycleClass", is("SUPERSPORT")));
    }

    @Test
    void shouldReturnMotorcycleListByBikerId() throws Exception {
        var bikerId = FIRST_BIKER_ID.toString();
        var content = mapper.writeValueAsString(filter);
        mockMvc.perform(get(String.format(BIKER_CONTEXT_PATH, bikerId))
                        .content(content))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.owner.id", is(FIRST_BIKER_ID.toString())))
                .andExpect(jsonPath("$.owner.username", is("tomasz_123")))
                .andExpect(jsonPath("$.owner.firstName", is("Tomasz")))
                .andExpect(jsonPath("$.owner.email", is("tomasz@123.pl")))
                .andExpect(jsonPath("$.owner.phoneNumber", is("123-456-789")))
                .andExpect(jsonPath("$.owner.dateOfBirth", is("1989-02-23")))
                .andExpect(jsonPath("$.results.pageInfo.pageSize", is(10)))
                .andExpect(jsonPath("$.results.pageInfo.actualPage", is(0)))
                .andExpect(jsonPath("$.results.pageInfo.totalPages", is(1)))
                .andExpect(jsonPath("$.results.pageInfo.totalRecordCount", is(1)))
                .andExpect(jsonPath("$.results.results[0].brand", is("Ducati")))
                .andExpect(jsonPath("$.results.results[0].model", is("Panigale V4S")))
                .andExpect(jsonPath("$.results.results[0].capacity", is(1199)))
                .andExpect(jsonPath("$.results.results[0].horsePower", is(225)))
                .andExpect(jsonPath("$.results.results[0].vintage", is(2022)))
                .andExpect(jsonPath("$.results.results[0].motorcycleClass", is("SUPERSPORT")));
    }

    @Test
    void shouldThrowExceptionWhenBikerWillBeNotExists() throws Exception {
        var bikerId = UUID.randomUUID();
        var content = mapper.writeValueAsString(filter);
        mockMvc.perform(get(String.format(BIKER_CONTEXT_PATH, bikerId))
                        .content(content))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldSetMotorcycleAsUnactive() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var bikerId = firstMotorcycleEntity.bikerId();
        mockMvc.perform(MockMvcRequestBuilders.put(String.format(ASSIGN_MOTORCYCLE_UNACTIVE_CONTEXT_PATH, motorcycleId,
                        bikerId)))
                .andDo(print())
                .andExpect(status().isNoContent());
        var ducati = motorcycleRepository.findById(motorcycleId)
                .orElse(null);
        assertThat(Objects.requireNonNull(ducati).isActive(), is(Boolean.FALSE));
    }

    @Test
    void shouldThrowBikerPermissionExceptionExceptionWhenBikerWillBeNotHavePermissionToMotorcycle() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var bikerId = SECOND_BIKER_ID.toString();
        mockMvc.perform(MockMvcRequestBuilders.put(String.format(ASSIGN_MOTORCYCLE_UNACTIVE_CONTEXT_PATH, motorcycleId,
                        bikerId)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldThrowMotorcycleAlreadyUnactiveExceptionWhenMotorcycleIsAlreadyUnactive() throws Exception {
        var motorcycleId = secondMotorcycleEntity.id();
        var bikerId = secondMotorcycleEntity.bikerId();
        mockMvc.perform(MockMvcRequestBuilders.put(String.format(ASSIGN_MOTORCYCLE_UNACTIVE_CONTEXT_PATH, motorcycleId,
                        bikerId)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void shouldMapEngine() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var bikerId = firstMotorcycleEntity.bikerId();
        var mockDto = mapper.writeValueAsString(mapEngineDto);
        mockMvc.perform(MockMvcRequestBuilders.patch(String.format(MAP_ENGINE_CONTEXT_PATH, motorcycleId, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());
        var motorcycleEntity = motorcycleRepository.findById(motorcycleId).orElse(null);
        assertAll(
                () -> assertThat(motorcycleEntity.capacity(), is(mapEngineDto.capacity())),
                () -> assertThat(motorcycleEntity.horsePower(), is(mapEngineDto.horsePower()))
        );
    }

    @Test
    void shouldThrowMotorcycleNotFoundExceptionWhenMotorcycleWillBeNotFound() throws Exception {
        var motorcycleId = UUID.randomUUID();
        var bikerId = firstMotorcycleEntity.bikerId();
        var mockDto = mapper.writeValueAsString(mapEngineDto);
        mockMvc.perform(MockMvcRequestBuilders.patch(String.format(MAP_ENGINE_CONTEXT_PATH, motorcycleId, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldThrowBikerPermissionExceptionWhenBikerWillBeNotHavePermissionToMapEngine() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var bikerId = secondMotorcycleEntity.bikerId();
        var mockDto = mapper.writeValueAsString(mapEngineDto);
        mockMvc.perform(MockMvcRequestBuilders.patch(String.format(MAP_ENGINE_CONTEXT_PATH, motorcycleId, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldThrowMotorcycleUnactiveExceptionWhenMotorcycleWillBeUnactive() throws Exception {
        var motorcycleId = secondMotorcycleEntity.id();
        var bikerId = secondMotorcycleEntity.bikerId();
        var mockDto = mapper.writeValueAsString(mapEngineDto);
        mockMvc.perform(MockMvcRequestBuilders.patch(String.format(MAP_ENGINE_CONTEXT_PATH, motorcycleId, bikerId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void shouldInitMotorcycleOwnerExchangeProcess() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var ownerId = firstMotorcycleEntity.bikerId();
        var recipientId = secondBikerEntity.id();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void shouldThrowMotorcycleUnactiveExceptionWhenMotorcycleToExchangeWillBeUnactive() throws Exception {
        var motorcycleId = secondMotorcycleEntity.id();
        var ownerId = secondBikerEntity.id();
        var recipientId = firstBikerEntity.id();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void shouldThrowMotorcycleNotFoundExceptionWhenMotorcycleWillBenOTExistsInTheDatabase() throws Exception {
        var motorcycleId = UUID.randomUUID();
        var ownerId = secondBikerEntity.id();
        var recipientId = firstBikerEntity.id();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldThrowBikerPermissionExceptionWhenOwnerWillNotHavePermissionToTheMotorcycle() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var ownerId = secondBikerEntity.id();
        var recipientId = secondBikerEntity.id();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenOwnerOfTheMotorcycleWillBeUnactive() throws Exception {
        var motorcycleId = thirdMotorcycleEntity.id();
        var ownerId = thirdBikerEntity.id();
        var recipientId = firstBikerEntity.id();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void shouldThrowBikerNotFoundExceptionWhenBikerWillBeNotExistsInTheDatabase() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var ownerId = firstBikerEntity.id();
        var recipientId = UUID.randomUUID();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldThrowBikerAlreadyOwnerExceptionWhenRecipientWillBeAlreadyOwnerOfTheMotorcycle() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var ownerId = firstBikerEntity.id();
        var recipientId = ownerId;
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenRecipientWillBeUnactive() throws Exception {
        var motorcycleId = firstMotorcycleEntity.id();
        var ownerId = firstBikerEntity.id();
        var recipientId = thirdBikerEntity.id();
        mockMvc.perform(post(String.format(MOTORCYCLE_EXCHANGE_OWNER_INIT_PROCESS, motorcycleId,
                        ownerId, recipientId)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void shouldConfirmMotorcycleOwnerUpdateProcess() throws Exception {
        var confirmationToken = motorcycleOwnerUpdateConfirmationStatusEntity.confirmationToken();
        mockMvc.perform(get(String.format(URL + "/%s/confirm", confirmationToken)))
                .andDo(print())
                .andExpect(status().isNoContent());
        var updateConfirmationStatusEntity =
                motorcycleOwnerUpdateConfirmationRepository.findById(motorcycleOwnerUpdateConfirmationStatusEntity.id())
                        .orElse(null);
        var updateStatusEntity = motorcycleOwnerUpdateStatusRepository.findById(motorcycleOwnerUpdateStatusEntity.id())
                .orElse(null);
        assertAll(
                () -> assertThat(updateConfirmationStatusEntity.isConfirm(), is(Boolean.TRUE)),
                () -> assertThat(updateStatusEntity.processCompleted(), is(Boolean.TRUE))
        );
    }
}