package pl.grzegorz.motorcycleserviceserver;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MapEngineDto;
import pl.grzegorz.motorcycleadapters.in.rest.dto.MotorcycleDto;
import pl.grzegorz.motorcycleadapters.out.persistence.biker.command.BikerEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle.adapters.command.MotorcycleEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_exchange_confirmation.command.MotorcycleOwnerUpdateConfirmationStatusEntity;
import pl.grzegorz.motorcycleadapters.out.persistence.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateStatusEntity;
import pl.grzegorz.motorcycleapplication.ports.view.BikerView;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class MotorcycleFixtures {

    public static final UUID FIRST_ENTITY_ID = UUID.fromString("8be1825b-52de-45e3-a950-f856797c1278");
    public static final UUID SECOND_ENTITY_ID = UUID.fromString("66c33890-c321-4c3d-a2ec-a50d95a6a8ae");
    public static final UUID FIRST_BIKER_ID = UUID.fromString("a12b492e-1641-4cc4-a8c7-e2c4bab6c8ba");
    public static final UUID SECOND_BIKER_ID = UUID.fromString("4010dec0-eeaf-477b-9079-453463c2246f");
    public static final UUID UNACTIVE_BIKER_ID = UUID.fromString("725a6648-0fae-48bc-994e-e8ef753ecc65");
    private static final UUID THIRD_MOTORCYCLE_ENTITY_ID = UUID.fromString("353f84ca-c63d-4c5a-a699-b9a2bea48d37");
    private static final UUID THIRD_BIKER_ENTITY_ID = UUID.fromString("72b74fa0-4aea-4cad-af28-c3fa8997bf1b");
    private static final UUID MOTORCYCLE_OWNER_UPDATE_STATUS_ID = UUID.fromString("8cb22dda-9ae0-4990-baeb-451cb0412e31");
    private static final UUID MOTORCYCLE_OWNER_UPDATE_CONFIRMATION_STATUS_ID = UUID.fromString("018e5389-7899-4241-a9d8-d54497394343");

    static MotorcycleEntity firstMotorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(FIRST_ENTITY_ID)
                .withBikerId(FIRST_BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withSerialNumber("UIHN73BJKCW89NOJKLD2")
                .withCapacity(1199)
                .withVintage(2022)
                .withHorsePower(225)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    static MotorcycleEntity secondMotorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(SECOND_ENTITY_ID)
                .withBikerId(SECOND_BIKER_ID)
                .withBrand("BMW")
                .withModel("S1000RR")
                .withSerialNumber("BICHEJD982JKNVEJIOC")
                .withCapacity(999)
                .withVintage(2021)
                .withHorsePower(208)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.FALSE)
                .build();
    }

    static MotorcycleEntity thirdMotorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(THIRD_MOTORCYCLE_ENTITY_ID)
                .withBikerId(THIRD_BIKER_ENTITY_ID)
                .withBrand("BMW")
                .withModel("S1000RR")
                .withSerialNumber("BIVE7893BIJOURE7INO")
                .withCapacity(999)
                .withVintage(2021)
                .withHorsePower(208)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    static MotorcycleDto motorcycleDto() {
        return MotorcycleDto.builder()
                .withBrand("BMW")
                .withModel("S1000RR")
                .withCapacity(999)
                .withSerialNumber("UJNCEW8934BUHEVC")
                .withHorsePower(208)
                .withVintage(2021)
                .withMotorcycleClass("SUPERSPORT")
                .build();
    }

    static MotorcycleDto motorcycleDtoWithInvalidMotorcycleClass() {
        return MotorcycleDto.builder()
                .withBrand("BMW")
                .withModel("S1000RR")
                .withCapacity(999)
                .withSerialNumber("UJNCEW8934BUHEVC")
                .withHorsePower(208)
                .withVintage(2021)
                .withMotorcycleClass("xyz")
                .build();
    }

    static BikerView bikerView() {
        return BikerView.builder()
                .withId(FIRST_BIKER_ID)
                .withEmail("test email")
                .withFirstName("test first name")
                .withUserName("test username")
                .build();
    }

    public static BikerEntity firstBikerEntity() {
        return BikerEntity.builder()
                .withId(FIRST_BIKER_ID)
                .withUserName("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withPhoneNumber("123-456-789")
                .withEnable(Boolean.TRUE)
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .build();
    }

    public static BikerEntity secondBikerEntity() {
        return BikerEntity.builder()
                .withId(SECOND_BIKER_ID)
                .withUserName("adrian_123")
                .withFirstName("Adrian")
                .withLastName("Adrianowski")
                .withEmail("adrian@123.pl")
                .withPhoneNumber("987-654-321")
                .withEnable(Boolean.TRUE)
                .withDateOfBirth(LocalDate.of(1982, 2, 23))
                .build();
    }

    public static BikerEntity thirdBikerEntity() {
        return BikerEntity.builder()
                .withId(THIRD_BIKER_ENTITY_ID)
                .withUserName("adrian_1234")
                .withFirstName("Adrian")
                .withLastName("Adrianowski")
                .withEmail("adrian@123.pl")
                .withPhoneNumber("987-654-321")
                .withEnable(Boolean.FALSE)
                .withDateOfBirth(LocalDate.of(1982, 2, 23))
                .build();
    }

    public static BikerEntity unactiveBikerEntity() {
        return BikerEntity.builder()
                .withId(UNACTIVE_BIKER_ID)
                .withUserName("TYMON_123")
                .withFirstName("Tymon")
                .withLastName("Tymanowski")
                .withEmail("tymon@123.pl")
                .withPhoneNumber("184-302-293")
                .withEnable(Boolean.FALSE)
                .withDateOfBirth(LocalDate.of(1984, 9, 11))
                .build();
    }

    public static MapEngineDto mapEngineDto() {
        return MapEngineDto.builder()
                .withCapacity(599)
                .withHorsePower(123)
                .build();
    }

    public static MotorcycleOwnerUpdateStatusEntity motorcycleOwnerUpdateStatusEntity() {
        return MotorcycleOwnerUpdateStatusEntity.builder()
                .withId(MOTORCYCLE_OWNER_UPDATE_STATUS_ID)
                .withMotorcycleId(FIRST_ENTITY_ID)
                .withRecipientId(SECOND_BIKER_ID)
                .withOwnerTransferorId(FIRST_BIKER_ID)
                .withProcessCompleted(Boolean.FALSE)
                .build();
    }

    public static MotorcycleOwnerUpdateConfirmationStatusEntity motorcycleOwnerUpdateConfirmationStatusEntity() {
        return MotorcycleOwnerUpdateConfirmationStatusEntity.builder()
                .withId(MOTORCYCLE_OWNER_UPDATE_CONFIRMATION_STATUS_ID)
                .withExchangeProcessId(MOTORCYCLE_OWNER_UPDATE_STATUS_ID)
                .withRecipientId(SECOND_BIKER_ID)
                .withConfirmationToken(String.format("%s_%s", UUID.randomUUID(), UUID.randomUUID()))
                .withIsConfirm(Boolean.FALSE)
                .build();
    }
}