package pl.grzegorz.motorcycleserviceserver;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Map;
import java.util.Set;

@ActiveProfiles("test")
@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
abstract class AbstractIntegrationTest {

    private static final Network INTEGRATION_TEST_NETWORK = Network.newNetwork();
    private static final String POSTGRES_IMAGE = "postgres:14-alpine";
    private static final String RABBITMQ_IMAGE = "rabbitmq:3.12.0-rc.3-management-alpine";
    private static final String MOTORCYCLE_DB = "motorcycle_db";
    private static final String DB_USERNAME = "motorcycles";
    private static final String DB_PASSWORD = "motorcycles";
    private static final String RABBIT_USER = "rabbit";
    private static final String RABBIT_PASSWORD = "rabbit";
    private static final String MOTORCYCLE_DB_ALIAS = "motorcycle-db";
    private static final String RABBITMQ_ALIAS = "rabbitmq";
    private static final int RABBITMQ_DEFAULT_PORT = 5672;
    private static final int POSTGRESQL_DEFAULT_PORT = 5432;
    private static final String BIKER_DISABLE_QUEUE = "bikers_motorcycle_disable";
    private static final String BIKER_ENABLE_QUEUE = "bikers_motorcycle_enable";
    private static final String BIKER_UPDATE_QUEUE = "bikers_motorcycle_update";
    private static final String BIKER_PERSIST_QUEUE = "bikers_motorcycle_persist";
    private static final String MOTORCYCLE_PERSIST_ROUTING_KEY = "motorcycle_create_routing_key";
    private static final String MOTORCYCLE_UPDATE_ROUTING_KEY = "motorcycle_update_routing_key";
    private static final String MOTORCYCLE_UNACTIVE_ROUTING_KEY = "motorcycle_unactive_routing_key";
    private static final String MOTORCYCLE_PERSIST_EXCHANGE = "motorcycles.persist";
    private static final String MOTORCYCLE_UPDATE_EXCHANGE = "motorcycles.update";
    private static final String MOTORCYCLE_UNACTIVE_EXCHANGE = "motorcycles.unactive";
    private static final String RABBITMQ_DEFAULT_V_HOST = "/";
    private static final String RABBITMQ_PERMISSIONS = ".*";
    private static final String EXCHANGE_TYPE = "topic";
    private static final String RABBITMQ_DESTINATION_TYPE = "queue";
    private static final String RABBITMQ_DEFAULT_USER_TAG = "management";
    private static final PostgreSQLContainer<?> POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_IMAGE))
            .withDatabaseName(MOTORCYCLE_DB)
            .withPassword(DB_PASSWORD)
            .withUsername(DB_USERNAME)
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(MOTORCYCLE_DB_ALIAS);

    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer(
            DockerImageName.parse(RABBITMQ_IMAGE))
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(RABBITMQ_ALIAS)
            .withExposedPorts(RABBITMQ_DEFAULT_PORT)
            .withUser(RABBIT_USER, RABBIT_PASSWORD, Set.of(RABBITMQ_DEFAULT_USER_TAG))
            .withPermission(RABBITMQ_DEFAULT_V_HOST, RABBIT_USER, RABBITMQ_PERMISSIONS, RABBITMQ_PERMISSIONS, RABBITMQ_PERMISSIONS)
            .withExchange(RABBITMQ_DEFAULT_V_HOST, MOTORCYCLE_PERSIST_EXCHANGE, EXCHANGE_TYPE, false, false, true, Map.of())
            .withExchange(RABBITMQ_DEFAULT_V_HOST, MOTORCYCLE_UPDATE_EXCHANGE, EXCHANGE_TYPE, false, false, true, Map.of())
            .withExchange(RABBITMQ_DEFAULT_V_HOST, MOTORCYCLE_UNACTIVE_EXCHANGE, EXCHANGE_TYPE, false, false, true, Map.of())
            .withQueue(BIKER_DISABLE_QUEUE)
            .withQueue(BIKER_ENABLE_QUEUE)
            .withQueue(BIKER_UPDATE_QUEUE)
            .withQueue(BIKER_PERSIST_QUEUE);

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper mapper;


    static {
        POSTGRE_SQL_CONTAINER.start();
        RABBIT_MQ_CONTAINER.start();
        System.setProperty("spring.rabbitmq.port",
                String.valueOf(RABBIT_MQ_CONTAINER.getMappedPort(RABBITMQ_DEFAULT_PORT)));
    }

    @DynamicPropertySource
    public static void postgresContainerConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", POSTGRE_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.password", POSTGRE_SQL_CONTAINER::getPassword);
        registry.add("spring.datasource.username", POSTGRE_SQL_CONTAINER::getUsername);
    }
}