package pl.grzegorz.motorcycleapplication.services.motorcycle_owner_update_status.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationCreateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessPort;
import pl.grzegorz.motorcycleapplication.services.validator.MotorcycleValidator;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

@RequiredArgsConstructor
public class MotorcycleOwnerUpdateInitProcessService implements MotorcycleOwnerUpdateInitProcessUseCase {

    private static final String CONFIRMATION_URL = "http://localhost:9000/motorcycles/%s/confirm";

    private final MotorcycleOwnerUpdateInitProcessPort motorcycleOwnerUpdateInitProcessPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleOwnerUpdateConfirmationCreateCommandPort motorcycleOwnerUpdateConfirmationCreateCommandPort;

    @Override
    public String init(String motorcycleId, String ownerId, String recipientId) {
        validateProcess(motorcycleId, ownerId, recipientId);
        var motorcycleOwnershipExchangeProcessAggregate =
                MotorcycleOwnerUpdateStatusAggregate.initUpdateProcess(motorcycleId, ownerId, recipientId);
        motorcycleOwnerUpdateInitProcessPort.init(motorcycleOwnershipExchangeProcessAggregate);
        var confirmationAggregate =
                motorcycleOwnerUpdateConfirmationCreateCommandPort.create(motorcycleOwnershipExchangeProcessAggregate.id(),
                motorcycleOwnershipExchangeProcessAggregate.recipientId());
        return String.format(CONFIRMATION_URL, confirmationAggregate.confirmationToken());
    }

    private void validateProcess(String motorcycleId, String ownerId, String recipientId) {
        var motorcycle = motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId);
        MotorcycleValidator.validateMotorcycleActiveStatus(motorcycle);
        MotorcycleValidator.validateNewOwner(motorcycle, recipientId);
        MotorcycleValidator.validateBikerPermission(motorcycle, ownerId);
        var owner = bikerByIdQueryPort.getById(ownerId);
        MotorcycleValidator.validateBikerActiveStatus(owner);
        var recipient = bikerByIdQueryPort.getById(recipientId);
        MotorcycleValidator.validateBikerActiveStatus(recipient);
    }
}