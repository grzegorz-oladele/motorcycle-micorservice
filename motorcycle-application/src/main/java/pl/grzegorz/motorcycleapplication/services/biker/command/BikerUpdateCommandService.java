package pl.grzegorz.motorcycleapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerUpdateCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerUpdateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.vo.ConstBikerUpdateValue;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

import static pl.grzegorz.motorcycleapplication.vo.ConstBikerUpdateValue.toConstBikerUpdateValue;

@RequiredArgsConstructor
public class BikerUpdateCommandService implements BikerUpdateCommandUseCase {

    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final BikerUpdateCommandPort bikerUpdateCommandPort;

    @Override
    public void update(BikerUpdateCommand updateCommand) {
        ConstBikerUpdateValue constBikerUpdateValue = toConstUpdateValue(updateCommand.id().toString());
        var bikerAggregate = BikerAggregate.update(updateCommand.toUpdateData(constBikerUpdateValue));
        bikerUpdateCommandPort.update(bikerAggregate);
    }

    private ConstBikerUpdateValue toConstUpdateValue(String bikerId) {
        BikerAggregate bikerAggregate = bikerByIdQueryPort.getById(bikerId);
        return toConstBikerUpdateValue(bikerAggregate);
    }
}