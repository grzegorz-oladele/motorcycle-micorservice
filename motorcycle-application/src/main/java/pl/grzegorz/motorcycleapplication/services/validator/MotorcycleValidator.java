package pl.grzegorz.motorcycleapplication.services.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.exception.*;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MotorcycleValidator {

    public static void validateBikerPermission(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        if (!motorcycleAggregate.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(String.format(
                    ExceptionMessage.BIKER_PERMISSION_EXCEPTION_MESSAGE.getMessage(), bikerId, motorcycleAggregate.id()));
        }
    }

    public static void validateMotorcycleActiveStatus(MotorcycleAggregate motorcycleAggregate) {
        if (motorcycleAggregate.isActive().equals(Boolean.FALSE)) {
            throw new MotorcycleAlreadyUnactiveException(String.format(
                    ExceptionMessage.MOTORCYCLE_ALREADY_UNACTIVE_EXCEPTION_MESSAGE.getMessage(), motorcycleAggregate.id()
            ));
        }
    }

    public static void validateBikerActiveStatus(BikerAggregate bikerAggregate) {
        if (bikerAggregate.isActive().equals(Boolean.FALSE)) {
            throw new BikerActiveException(String.format(
                    ExceptionMessage.BIKER_ACTIVE_EXCEPTION_MESSAGE.getMessage(), bikerAggregate.id()));
        }
    }

    public static void validateNewOwner(MotorcycleAggregate motorcycleAggregate, String recipientId) {
        if (motorcycleAggregate.bikerId().toString().equals(recipientId)) {
            throw new BikerAlreadyOwnerException(String.format(
                    ExceptionMessage.BIKER_ALREADY_OWNER_EXCEPTION_MESSAGE.getMessage(), recipientId,
                    motorcycleAggregate.id()));
        }
    }

    public static void validateOwnershipTransferStatus(MotorcycleOwnerUpdateConfirmationStatusAggregate confirmationAggregate,
                                                       MotorcycleAggregate motorcycleAggregate) {
        if (confirmationAggregate.isConfirm().equals(Boolean.TRUE)) {
            throw new ConfirmationProcessAlreadyFinishException(String.format(
                    ExceptionMessage.CONFIRMATION_PROCESS_ALREADY_FINISH_EXCEPTION_MESSAGE.getMessage(),
                    motorcycleAggregate.id(), confirmationAggregate.recipientId()));
        }
    }
}