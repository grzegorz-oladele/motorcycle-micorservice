package pl.grzegorz.motorcycleapplication.services.motorcycle.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcyclePageQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcyclePageQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;

@RequiredArgsConstructor
public class MotorcyclePageQueryService implements MotorcyclePageQueryUseCase {

    private final MotorcyclePageQueryPort motorcyclePageQueryPort;

    @Override
    public ResultViewInfo<MotorcycleView> findAll(ResourceFilters filter) {
        return motorcyclePageQueryPort.findAll(filter);
    }
}