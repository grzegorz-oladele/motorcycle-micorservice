package pl.grzegorz.motorcycleapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerDisableCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerDisableCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;

import java.util.UUID;

@RequiredArgsConstructor
public class BikerDisableCommandService implements BikerDisableCommandUseCase {

    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final BikerDisableCommandPort bikerDisableCommandPort;

    @Override
    public void disable(UUID bikerId) {
        var bikerAggregate = bikerByIdQueryPort.getById(bikerId.toString());
        bikerDisableCommandPort.disable(bikerAggregate);
    }
}