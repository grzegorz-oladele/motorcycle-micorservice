package pl.grzegorz.motorcycleapplication.services.motorcycle.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcycleByIdQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.validator.MotorcycleValidator;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;

@RequiredArgsConstructor
public class MotorcycleByIdQueryService implements MotorcycleByIdQueryUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;

    @Override
    public MotorcycleView findById(String motorcycleId, String bikerId) {
        var aggregate = motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId);
        MotorcycleValidator.validateBikerPermission(aggregate, bikerId);
        var bikerAggregate = bikerByIdQueryPort.getById(bikerId);
        return MotorcycleView.toView(aggregate, bikerAggregate);
    }
}