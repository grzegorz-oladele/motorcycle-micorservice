package pl.grzegorz.motorcycleapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerCreateCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

@RequiredArgsConstructor
public class BikerCreateCommandService implements BikerCreateCommandUseCase {

    private final BikerCreateCommandPort bikerCreateCommandPort;

    @Override
    public void create(BikerCreateCommand createCommand) {
        var bikerAggregate = BikerAggregate.create(createCommand.toCreateData());
        bikerCreateCommandPort.create(bikerAggregate);
    }
}