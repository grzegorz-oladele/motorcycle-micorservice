package pl.grzegorz.motorcycleapplication.services.motorcycle.commands;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleMapEngineUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleMapEngineCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleMapEnginePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.validator.MotorcycleValidator;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerActiveException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

@RequiredArgsConstructor
public class MotorcycleMapEngineCommandService implements MotorcycleMapEngineUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort;
    private final MotorcycleMapEnginePublisherPort motorcycleMapEnginePublisherPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;

    @Override
    public void map(String motorcycleId, String bikerId, MotorcycleMapEngineCommand mapEngineCommand) {
        validateBikerActive(bikerId);
        var motorcycleAggregate = motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId);
        validateMotorcycle(motorcycleAggregate, bikerId);
        motorcycleAggregate.mapEngine(mapEngineCommand.toMapEngineData());
        motorcycleMapEngineCommandPort.map(motorcycleAggregate);
        motorcycleMapEnginePublisherPort.publish(motorcycleAggregate);
    }

    private void validateMotorcycle(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        MotorcycleValidator.validateBikerPermission(motorcycleAggregate, bikerId);
        MotorcycleValidator.validateMotorcycleActiveStatus(motorcycleAggregate);
    }

    private void validateBikerActive(String bikerId) {
        var bikerAggregate = bikerByIdQueryPort.getById(bikerId);
        if (bikerAggregate.isActive().equals(Boolean.FALSE)) {
            throw new BikerActiveException(String.format(
                    ExceptionMessage.BIKER_ACTIVE_EXCEPTION_MESSAGE.getMessage(), bikerId));
        }
    }
}