package pl.grzegorz.motorcycleapplication.services.motorcycle.commands;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleSetUnactiveCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleSetUnactivePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerPermissionException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

@RequiredArgsConstructor
public class MotorcycleSetUnactiveCommandCommandService implements MotorcycleSetUnactiveCommandUseCase {

    private final pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleSetUnactiveCommandPort motorcycleSetUnactiveCommandPort;
    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleSetUnactivePublisherPort motorcycleSetUnactivePublisherPort;

    @Override
    public void setUnactive(String motorcycleId, String bikerId) {
        var motorcycleAggregate = motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId);
        checkBikerPermissionAndThrowExceptionIfHasNot(motorcycleAggregate, bikerId);
        motorcycleSetUnactiveCommandPort.setUnactive(motorcycleAggregate);
        motorcycleSetUnactivePublisherPort.publish(motorcycleAggregate);
    }

    private void checkBikerPermissionAndThrowExceptionIfHasNot(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        if (!motorcycleAggregate.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(String.format(
                    ExceptionMessage.BIKER_PERMISSION_EXCEPTION_MESSAGE.getMessage(), bikerId, motorcycleAggregate.id()));
        }
    }
}