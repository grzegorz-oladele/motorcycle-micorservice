package pl.grzegorz.motorcycleapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.biker.command.BikerEnableCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.command.BikerEnableCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;

import java.util.UUID;

@RequiredArgsConstructor
public class BikerEnableCommandService implements BikerEnableCommandUseCase {

    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final BikerEnableCommandPort bikerEnableCommandPort;

    @Override
    public void enable(UUID bikerId) {
        var bikerAggregate = bikerByIdQueryPort.getById(bikerId.toString());
        bikerEnableCommandPort.enable(bikerAggregate);
    }
}