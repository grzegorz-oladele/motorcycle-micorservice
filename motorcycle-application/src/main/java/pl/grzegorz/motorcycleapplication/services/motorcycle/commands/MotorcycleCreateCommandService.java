package pl.grzegorz.motorcycleapplication.services.motorcycle.commands;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleCreateCommandUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleCreateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleCreatePublisherPort;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerActiveException;
import pl.grzegorz.motorcycledomain.exception.InvalidMotorcycleClassException;
import pl.grzegorz.motorcycledomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@RequiredArgsConstructor
public class MotorcycleCreateCommandService implements MotorcycleCreateCommandUseCase {

    private final MotorcycleCreateCommandPort motorcycleCreateCommandPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleCreatePublisherPort motorcycleCreatePublisherPort;

    @Override
    public UUID create(MotorcycleCreateCommand command) {
        validateBikerIsActive(command.bikerId().toString());
        try {
            var motorcycleAggregate = MotorcycleAggregate.create(command.toCreateData());
            motorcycleCreateCommandPort.save(motorcycleAggregate);
            motorcycleCreatePublisherPort.publish(motorcycleAggregate);
            return motorcycleAggregate.id();
        } catch (IllegalArgumentException e) {
            throw new InvalidMotorcycleClassException(String.format(
                    ExceptionMessage.INVALID_MOTORCYCLE_CLASS_EXCEPTION_MESSAGE.getMessage()
            ));
        }
    }

    private void validateBikerIsActive(String bikerId) {
        var biker = bikerByIdQueryPort.getById(bikerId);
        if (Boolean.FALSE.equals(biker.isActive())) {
            throw new BikerActiveException(String.format(
                    ExceptionMessage.BIKER_ACTIVE_EXCEPTION_MESSAGE.getMessage(), bikerId));
        }
    }
}