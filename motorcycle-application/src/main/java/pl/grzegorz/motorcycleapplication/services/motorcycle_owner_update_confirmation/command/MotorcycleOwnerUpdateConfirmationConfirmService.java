package pl.grzegorz.motorcycleapplication.services.motorcycle_owner_update_confirmation.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_confirmation.MotorcycleOwnerUpdateConfirmationConfirmUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleChangeOwnerCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleUpdateOwnerPublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationConfirmCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.query.MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateFinishProcessPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.query.MotorcycleOwnerUpdateStatusByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.validator.MotorcycleValidator;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

@RequiredArgsConstructor
public class MotorcycleOwnerUpdateConfirmationConfirmService implements MotorcycleOwnerUpdateConfirmationConfirmUseCase {

    private final MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort confirmationByConfirmTokenQueryPort;
    private final MotorcycleOwnerUpdateConfirmationConfirmCommandPort confirmationConfirmCommandPort;
    private final MotorcycleOwnerUpdateStatusByIdQueryPort exchangeProcessByProcessIdQueryPort;
    private final MotorcycleOwnerUpdateFinishProcessPort exchangeProcessFinishCommandPort;
    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleChangeOwnerCommandPort motorcycleChangeOwnerCommandPort;
    private final MotorcycleUpdateOwnerPublisherPort motorcycleUpdateOwnerPublisherPort;

    @Override
    public void confirm(String confirmationToken) {
        var confirmationAggregate = confirmationByConfirmTokenQueryPort.findByToken(confirmationToken);
        var exchangeProcessAggregate =
                exchangeProcessByProcessIdQueryPort.findByProcessId(confirmationAggregate.exchangeProcessId().toString());
        var motorcycleAggregate =
                motorcycleByIdQueryPort.getMotorcycleById(exchangeProcessAggregate.motorcycleId().toString(),
                        exchangeProcessAggregate.ownerTransferorId().toString());
        validateProcess(confirmationAggregate, motorcycleAggregate);
        confirmationAggregate.confirm();
        confirmationConfirmCommandPort.confirm(confirmationAggregate);
        exchangeProcessAggregate.finishUpdateProcess(exchangeProcessAggregate.id().toString());
        exchangeProcessFinishCommandPort.finish(exchangeProcessAggregate);
        motorcycleAggregate.changeOwner(exchangeProcessAggregate.recipientId());
        motorcycleChangeOwnerCommandPort.changeOwner(motorcycleAggregate);
        motorcycleUpdateOwnerPublisherPort.publish(motorcycleAggregate, exchangeProcessAggregate.ownerTransferorId());
    }

    private void validateProcess(MotorcycleOwnerUpdateConfirmationStatusAggregate confirmationAggregate,
                                 MotorcycleAggregate motorcycleAggregate) {
        var recipient = bikerByIdQueryPort.getById(confirmationAggregate.recipientId().toString());
        MotorcycleValidator.validateBikerActiveStatus(recipient);
        MotorcycleValidator.validateMotorcycleActiveStatus(motorcycleAggregate);
        MotorcycleValidator.validateOwnershipTransferStatus(confirmationAggregate, motorcycleAggregate);
    }
}