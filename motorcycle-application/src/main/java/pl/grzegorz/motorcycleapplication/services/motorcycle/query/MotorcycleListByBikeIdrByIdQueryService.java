package pl.grzegorz.motorcycleapplication.services.motorcycle.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query.MotorcycleListByBikerByIdQueryUseCase;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

@RequiredArgsConstructor
public class MotorcycleListByBikeIdrByIdQueryService implements MotorcycleListByBikerByIdQueryUseCase {

    private final MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort;

    @Override
    public MotorcycleListByBikerIdView findAllByBikerId(String bikerId, ResourceFilters filter) {
        return motorcycleListByBikerIdQueryPort.findAllByBiker(bikerId, filter);
    }
}