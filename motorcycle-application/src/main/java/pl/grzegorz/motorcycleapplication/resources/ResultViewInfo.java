package pl.grzegorz.motorcycleapplication.resources;

import lombok.Builder;

import java.util.List;

@Builder
public record ResultViewInfo<T>(
        PageInfo pageInfo,
        List<T> results
) {
}