package pl.grzegorz.motorcycleapplication.resources;

import lombok.Builder;

@Builder(toBuilder = true)
public record PageInfo(
        int pageSize,
        int actualPage,
        int totalPages,
        long totalRecordCount
) {
}
