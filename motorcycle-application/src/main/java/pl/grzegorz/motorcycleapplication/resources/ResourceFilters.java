package pl.grzegorz.motorcycleapplication.resources;

public record ResourceFilters(
        Integer page,
        Integer size
) {

    private static final int DEFAULT_PAGE_SIZE = 10;

    public static ResourceFilters empty() {
        return new ResourceFilters(0, DEFAULT_PAGE_SIZE);
    }
}

