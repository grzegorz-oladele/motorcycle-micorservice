package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

public interface MotorcycleSetUnactiveCommandPort {

    void setUnactive(MotorcycleAggregate motorcycleAggregate);
}