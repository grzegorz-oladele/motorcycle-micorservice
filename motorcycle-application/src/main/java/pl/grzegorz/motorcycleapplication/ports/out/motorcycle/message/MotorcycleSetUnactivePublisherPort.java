package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

public interface MotorcycleSetUnactivePublisherPort {

    void publish(MotorcycleAggregate motorcycleAggregate);
}