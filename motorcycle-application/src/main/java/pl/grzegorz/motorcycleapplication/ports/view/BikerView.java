package pl.grzegorz.motorcycleapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with", toBuilder = true)
public record BikerView(
        UUID id,
        String firstName,
        String userName,
        String email
) {

    public static BikerView toView(BikerAggregate bikerAggregate) {
        return BikerView.builder()
                .withId(bikerAggregate.id())
                .withFirstName(bikerAggregate.firstName())
                .withUserName(bikerAggregate.userName())
                .withEmail(bikerAggregate.email())
                .build();
    }
}
