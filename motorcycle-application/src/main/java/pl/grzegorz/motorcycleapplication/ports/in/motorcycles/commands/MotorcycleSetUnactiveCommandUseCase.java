package pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands;

public interface MotorcycleSetUnactiveCommandUseCase {

    void setUnactive(String motorcycleId, String bikerId);
}