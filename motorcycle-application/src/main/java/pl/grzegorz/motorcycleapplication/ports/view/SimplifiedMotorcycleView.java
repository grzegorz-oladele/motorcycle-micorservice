package pl.grzegorz.motorcycleapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record SimplifiedMotorcycleView(
        UUID id,
        String brand,
        String model,
        int capacity,
        int horsePower,
        int vintage,
        String serialNumber,
        MotorcycleClass motorcycleClass
) {
}