package pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.query;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

public interface MotorcycleOwnerUpdateStatusByIdQueryPort {

    MotorcycleOwnerUpdateStatusAggregate findByProcessId(String processId);
}