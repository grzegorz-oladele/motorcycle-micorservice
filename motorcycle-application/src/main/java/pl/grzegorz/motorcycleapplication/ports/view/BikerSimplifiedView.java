package pl.grzegorz.motorcycleapplication.ports.view;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

import java.time.LocalDate;
import java.util.UUID;

@Builder(access = AccessLevel.PRIVATE, setterPrefix = "with")
public record BikerSimplifiedView(
        UUID id,
        String username,
        String firstName,
        String email,
        String phoneNumber,
        LocalDate dateOfBirth
) {

    public static BikerSimplifiedView toView(BikerAggregate bikerAggregate) {
        return BikerSimplifiedView.builder()
                .withId(bikerAggregate.id())
                .withUsername(bikerAggregate.userName())
                .withFirstName(bikerAggregate.firstName())
                .withEmail(bikerAggregate.email())
                .withPhoneNumber(bikerAggregate.phoneNumber())
                .withDateOfBirth(bikerAggregate.dateOfBirth())
                .build();
    }
}
