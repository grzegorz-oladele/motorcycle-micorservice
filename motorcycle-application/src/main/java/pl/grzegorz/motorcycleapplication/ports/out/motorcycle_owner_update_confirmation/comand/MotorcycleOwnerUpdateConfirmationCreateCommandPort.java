package pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;

import java.util.UUID;

public interface MotorcycleOwnerUpdateConfirmationCreateCommandPort {

    MotorcycleOwnerUpdateConfirmationStatusAggregate create(UUID exchangeProcessId, UUID recipientId);
}