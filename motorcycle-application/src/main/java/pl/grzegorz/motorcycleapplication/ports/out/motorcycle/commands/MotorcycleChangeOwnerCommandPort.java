package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

public interface MotorcycleChangeOwnerCommandPort {

    void changeOwner(MotorcycleAggregate motorcycleAggregate);
}