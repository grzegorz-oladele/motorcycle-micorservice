package pl.grzegorz.motorcycleapplication.ports.in.biker.command;

import lombok.Builder;
import pl.grzegorz.motorcycleapplication.vo.ConstBikerUpdateValue;
import pl.grzegorz.motorcycledomain.data.biker.BikerUpdateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface BikerUpdateCommandUseCase {

    void update(BikerUpdateCommand updateCommand);

    @Builder(setterPrefix = "with")
    record BikerUpdateCommand(
            UUID id,
            String firstName,
            String lastName,
            String email,
            Boolean enable,
            String phoneNumber
    ) {

        public BikerUpdateData toUpdateData(ConstBikerUpdateValue constUpdateValue) {
            return BikerUpdateData.builder()
                    .withId(id)
                    .withFirstName(firstName)
                    .withLastName(lastName)
                    .withUsername(constUpdateValue.username())
                    .withEmail(email)
                    .withEnable(enable)
                    .withDateOfBirth(constUpdateValue.dateOfBirth())
                    .withPhoneNumber(phoneNumber)
                    .withCreatedAt(constUpdateValue.createdAt())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}