package pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_confirmation;

public interface MotorcycleOwnerUpdateConfirmationConfirmUseCase {

    void confirm(String confirmationToken);
}