package pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands;

import lombok.Builder;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleCreateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface MotorcycleCreateCommandUseCase {

    UUID create(MotorcycleCreateCommand command);

    @Builder(setterPrefix = "with")
    record MotorcycleCreateCommand(
            UUID bikerId,
            String brand,
            String model,
            int capacity,
            int horsePower,
            int vintage,
            String motorcycleClass,
            String serialNumber
    ) {
        public MotorcycleCreateData toCreateData() {
            return MotorcycleCreateData.builder()
                    .withBikerId(bikerId)
                    .withBrand(brand)
                    .withModel(model)
                    .withCapacity(capacity)
                    .withHorsePower(horsePower)
                    .withVintage(vintage)
                    .withMotorcycleClass(motorcycleClass)
                    .withSerialNumber(serialNumber)
                    .withCreatedAt(LocalDateTime.now())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}