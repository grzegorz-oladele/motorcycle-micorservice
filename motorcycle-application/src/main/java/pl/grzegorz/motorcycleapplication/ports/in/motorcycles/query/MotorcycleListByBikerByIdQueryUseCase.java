package pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query;

import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

public interface MotorcycleListByBikerByIdQueryUseCase {

    MotorcycleListByBikerIdView findAllByBikerId(String bikerId, ResourceFilters filter);
}
