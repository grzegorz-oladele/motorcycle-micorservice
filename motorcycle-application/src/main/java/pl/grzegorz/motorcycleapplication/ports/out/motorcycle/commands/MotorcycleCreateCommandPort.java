package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

public interface MotorcycleCreateCommandPort {

    UUID save(MotorcycleAggregate motorcycleAggregate);
}
