package pl.grzegorz.motorcycleapplication.ports.in.motorcycle_owner_update_status.command;

public interface MotorcycleOwnerUpdateInitProcessUseCase {

    String init(String motorcycleId, String ownerId, String recipientId);
}