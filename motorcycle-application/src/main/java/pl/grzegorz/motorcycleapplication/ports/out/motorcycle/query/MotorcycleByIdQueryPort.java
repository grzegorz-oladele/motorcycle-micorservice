package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

public interface MotorcycleByIdQueryPort {

    MotorcycleAggregate getMotorcycleById(String motorcycleId, String bikerId);
}