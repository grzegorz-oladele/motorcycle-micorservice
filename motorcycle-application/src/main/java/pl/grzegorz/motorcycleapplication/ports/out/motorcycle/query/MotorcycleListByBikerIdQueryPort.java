package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query;

import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

public interface MotorcycleListByBikerIdQueryPort {

    MotorcycleListByBikerIdView findAllByBiker(String bikerId, ResourceFilters filters);
}