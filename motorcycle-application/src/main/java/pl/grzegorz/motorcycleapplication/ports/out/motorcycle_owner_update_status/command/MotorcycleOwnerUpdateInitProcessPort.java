package pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;

public interface MotorcycleOwnerUpdateInitProcessPort {

    void init(MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate);
}