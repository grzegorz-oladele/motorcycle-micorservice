package pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query;

import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

public interface MotorcyclePageQueryUseCase {

    ResultViewInfo<MotorcycleView> findAll(ResourceFilters filter);
}