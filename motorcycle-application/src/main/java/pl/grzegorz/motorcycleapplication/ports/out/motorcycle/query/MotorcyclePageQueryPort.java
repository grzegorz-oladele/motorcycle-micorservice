package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query;

import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;

public interface MotorcyclePageQueryPort {

    ResultViewInfo<MotorcycleView> findAll(ResourceFilters filters);
}