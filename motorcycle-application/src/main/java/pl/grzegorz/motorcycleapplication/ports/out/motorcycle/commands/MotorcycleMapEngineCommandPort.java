package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

public interface MotorcycleMapEngineCommandPort {

    void map(MotorcycleAggregate motorcycleAggregate);
}