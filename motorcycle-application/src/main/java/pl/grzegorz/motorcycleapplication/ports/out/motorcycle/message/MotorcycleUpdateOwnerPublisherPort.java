package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

public interface MotorcycleUpdateOwnerPublisherPort {

    void publish(MotorcycleAggregate motorcycleAggregate, UUID ownerTransferorId);
}