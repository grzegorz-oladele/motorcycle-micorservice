package pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message;

import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

public interface MotorcycleCreatePublisherPort {

    void publish(MotorcycleAggregate motorcycleAggregate);
}