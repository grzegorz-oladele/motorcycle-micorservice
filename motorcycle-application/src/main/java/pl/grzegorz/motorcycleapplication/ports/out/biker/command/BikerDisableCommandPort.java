package pl.grzegorz.motorcycleapplication.ports.out.biker.command;

import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

public interface BikerDisableCommandPort {

    void disable(BikerAggregate bikerAggregate);
}