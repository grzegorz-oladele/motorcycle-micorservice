package pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands;

import lombok.Builder;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleMapEngineData;

public interface MotorcycleMapEngineUseCase {

    void map(String motorcycleId, String bikerId, MotorcycleMapEngineCommand mapEngineCommand);

    @Builder(setterPrefix = "with")
    record MotorcycleMapEngineCommand(
            int capacity,
            int horsePower

    ) {
        public MotorcycleMapEngineData toMapEngineData() {
            return MotorcycleMapEngineData.builder()
                    .withCapacity(capacity)
                    .withHorsePower(horsePower)
                    .build();
        }
    }
}