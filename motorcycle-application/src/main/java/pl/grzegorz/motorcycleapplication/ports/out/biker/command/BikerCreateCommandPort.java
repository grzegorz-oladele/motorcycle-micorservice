package pl.grzegorz.motorcycleapplication.ports.out.biker.command;

import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

public interface BikerCreateCommandPort {

    void create(BikerAggregate bikerAggregate);
}
