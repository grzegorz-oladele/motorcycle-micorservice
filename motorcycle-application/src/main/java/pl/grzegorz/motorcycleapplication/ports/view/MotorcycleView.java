package pl.grzegorz.motorcycleapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with", toBuilder = true)
public record MotorcycleView(
        UUID id,
        BikerView owner,
        String brand,
        String model,
        int capacity,
        int horsePower,
        int vintage,
        String serialNumber,
        MotorcycleClass motorcycleClass,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {

    public static MotorcycleView toView(MotorcycleAggregate aggregate, BikerAggregate bikerAggregate) {
        return MotorcycleView.builder()
                .withId(aggregate.id())
                .withBrand(aggregate.brand())
                .withOwner(bikerView(bikerAggregate))
                .withModel(aggregate.model())
                .withCapacity(aggregate.capacity())
                .withHorsePower(aggregate.horsePower())
                .withVintage(aggregate.vintage())
                .withSerialNumber(aggregate.serialNumber())
                .withMotorcycleClass(aggregate.motorcycleClass())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .build();
    }

    private static BikerView bikerView(BikerAggregate bikerAggregate) {
        return BikerView.builder()
                .withId(bikerAggregate.id())
                .withUserName(bikerAggregate.userName())
                .withFirstName(bikerAggregate.firstName())
                .withEmail(bikerAggregate.email())
                .build();
    }
}