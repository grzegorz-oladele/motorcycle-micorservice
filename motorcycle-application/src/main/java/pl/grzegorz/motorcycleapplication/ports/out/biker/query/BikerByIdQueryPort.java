package pl.grzegorz.motorcycleapplication.ports.out.biker.query;


import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

public interface BikerByIdQueryPort {

    BikerAggregate getById(String bikerId);
}
