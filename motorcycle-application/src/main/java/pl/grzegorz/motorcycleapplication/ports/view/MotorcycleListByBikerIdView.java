package pl.grzegorz.motorcycleapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;

@Builder(setterPrefix = "with")
public record MotorcycleListByBikerIdView(
        BikerSimplifiedView owner,
        ResultViewInfo<SimplifiedMotorcycleView> results
) {
}