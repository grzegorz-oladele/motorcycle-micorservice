package pl.grzegorz.motorcycleapplication.ports.in.motorcycles.query;

import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;

public interface MotorcycleByIdQueryUseCase {

    MotorcycleView findById(String motorcycleId, String bikerId);
}
