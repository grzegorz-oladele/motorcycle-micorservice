package pl.grzegorz.motorcycleapplication.ports.in.biker.command;

import java.util.UUID;

public interface BikerEnableCommandUseCase {

    void enable(UUID bikerId);
}