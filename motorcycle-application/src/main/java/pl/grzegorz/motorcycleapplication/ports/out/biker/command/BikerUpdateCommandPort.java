package pl.grzegorz.motorcycleapplication.ports.out.biker.command;

import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;

public interface BikerUpdateCommandPort {

    void update(BikerAggregate bikerAggregate);
}