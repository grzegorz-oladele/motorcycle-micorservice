package pl.grzegorz.motorcycleapplication.vo;

import lombok.Builder;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with")
public record ConstMotorcycleUpdateValue(
        UUID id,
        UUID bikerId,
        String brand,
        String model,
        Integer vintage,
        String serialNumber,
        MotorcycleClass motorcycleClass,
        LocalDateTime createdAt
) {

    public static ConstMotorcycleUpdateValue toConstMotorcycleUpdateValue(MotorcycleAggregate aggregate) {
        return ConstMotorcycleUpdateValue.builder()
                .withId(aggregate.id())
                .withBikerId(aggregate.bikerId())
                .withBrand(aggregate.brand())
                .withModel(aggregate.model())
                .withMotorcycleClass(aggregate.motorcycleClass())
                .withSerialNumber(aggregate.serialNumber())
                .withVintage(aggregate.vintage())
                .withCreatedAt(aggregate.createdAt())
                .build();
    }
}