package pl.grzegorz.motorcycleapplication.services.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerPermissionException;
import pl.grzegorz.motorcycledomain.exception.MotorcycleAlreadyUnactiveException;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class MotorcycleValidatorTest {

    @InjectMocks
    private MotorcycleValidator motorcycleValidator;

    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = MotorcycleFixtures.unactiveMotorcycleAggregate();
    }

    @Test
    void shouldThrowBikerPermissionExceptionWhenBikerIsNotTheOwnerOfTheMotorcycle() {
//        given
        var bikerId = UUID.randomUUID().toString();
//        when + then
        assertThrows(BikerPermissionException.class,
                () -> motorcycleValidator.validateBikerPermission(motorcycleAggregate, bikerId));
    }

    @Test
    void shouldThrowMotorcycleAlreadyUnactiveExceptionWhenMotorcycleWillBeNotActive() {
        assertThrows(MotorcycleAlreadyUnactiveException.class,
                () -> motorcycleValidator.validateMotorcycleActiveStatus(motorcycleAggregate));
    }
}