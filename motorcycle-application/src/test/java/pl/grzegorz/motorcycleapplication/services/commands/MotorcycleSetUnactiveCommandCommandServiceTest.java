package pl.grzegorz.motorcycleapplication.services.commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleSetUnactivePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures;
import pl.grzegorz.motorcycleapplication.services.motorcycle.commands.MotorcycleSetUnactiveCommandCommandService;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.*;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.motorcycleAggregate;

@ExtendWith(MockitoExtension.class)
class MotorcycleSetUnactiveCommandCommandServiceTest {

    @InjectMocks
    private MotorcycleSetUnactiveCommandCommandService motorcycleSetUnactiveCommandCommandService;
    @Mock
    private pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleSetUnactiveCommandPort motorcycleSetUnactiveCommandPort;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private MotorcycleSetUnactivePublisherPort motorcycleSetUnactivePublisherPort;

    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallSetUnactiveMethod() {
//        given
        var motorcycleId = FIRST_MOTORCYCLE_AGGREGATE_ID.toString();
        var bikerId = BIKER_ID.toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId)).thenReturn(motorcycleAggregate);
//        when
        motorcycleSetUnactiveCommandCommandService.setUnactive(motorcycleId, bikerId);
//        then
        assertAll(
                () -> verify(motorcycleSetUnactiveCommandPort).setUnactive(motorcycleAggregate),
                () -> verify(motorcycleSetUnactivePublisherPort).publish(motorcycleAggregate)
        );
    }
}