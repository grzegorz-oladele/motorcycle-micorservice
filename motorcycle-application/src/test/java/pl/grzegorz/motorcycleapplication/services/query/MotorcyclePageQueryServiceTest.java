package pl.grzegorz.motorcycleapplication.services.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcyclePageQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;
import pl.grzegorz.motorcycleapplication.services.motorcycle.query.MotorcyclePageQueryService;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.motorcyclePage;

@ExtendWith(MockitoExtension.class)
class MotorcyclePageQueryServiceTest {

    @InjectMocks
    private MotorcyclePageQueryService motorcyclePageQueryService;
    @Mock
    private MotorcyclePageQueryPort motorcyclePageQueryPort;

    private ResultViewInfo<MotorcycleView> motorcyclePage;

    @BeforeEach
    void setup() {
        motorcyclePage = motorcyclePage();
    }

    @Test
    void shouldReturnPageOfMotorcycleView() {
//        given
        var filter = new ResourceFilters(null, null);
        when(motorcyclePageQueryPort.findAll(filter)).thenReturn(motorcyclePage);
//        when
        var motorcycleViewPage = motorcyclePageQueryService.findAll(filter);
//        then
        assertAll(
                () -> assertThat(motorcycleViewPage.pageInfo().pageSize(), is(10)),
                () -> assertThat(motorcycleViewPage.pageInfo().totalPages(), is(1)),
                () -> assertThat(motorcycleViewPage.pageInfo().totalRecordCount(), is(2L)),
                () -> assertThat(motorcycleViewPage.pageInfo().actualPage(), is(0)),
                () -> assertThat(motorcycleViewPage.results().size(), is(2)),
                () -> assertThat(motorcycleViewPage.results().get(0).brand(), is("Ducati")),
                () -> assertThat(motorcycleViewPage.results().get(0).model(), is("Panigale V4S")),
                () -> assertThat(motorcycleViewPage.results().get(0).capacity(), is(1199)),
                () -> assertThat(motorcycleViewPage.results().get(0).vintage(), is(2022)),
                () -> assertThat(motorcycleViewPage.results().get(0).horsePower(), is(225)),
                () -> assertThat(motorcycleViewPage.results().get(0).motorcycleClass(), is(MotorcycleClass.SUPERSPORT)),
                () -> assertThat(motorcycleViewPage.results().get(1).brand(), is("BMW")),
                () -> assertThat(motorcycleViewPage.results().get(1).model(), is("S1000RR")),
                () -> assertThat(motorcycleViewPage.results().get(1).capacity(), is(999)),
                () -> assertThat(motorcycleViewPage.results().get(1).vintage(), is(2021)),
                () -> assertThat(motorcycleViewPage.results().get(1).horsePower(), is(208)),
                () -> assertThat(motorcycleViewPage.results().get(1).motorcycleClass(), is(MotorcycleClass.SUPERSPORT))
        );
    }
}