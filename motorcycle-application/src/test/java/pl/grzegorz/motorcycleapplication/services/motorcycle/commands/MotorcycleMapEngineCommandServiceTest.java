package pl.grzegorz.motorcycleapplication.services.motorcycle.commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleMapEngineUseCase.MotorcycleMapEngineCommand;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleMapEngineCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleMapEnginePublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures;
import pl.grzegorz.motorcycleapplication.services.validator.MotorcycleValidator;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerActiveException;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.unactiveBikerAggregate;

@ExtendWith(MockitoExtension.class)
class MotorcycleMapEngineCommandServiceTest {

    @InjectMocks
    private MotorcycleMapEngineCommandService motorcycleMapEngineCommandService;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort;
    @Mock
    private MotorcycleMapEnginePublisherPort motorcycleMapEnginePublisherPort;
    @Mock
    private MotorcycleValidator motorcycleValidator;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleAggregate unactiveMotorcycleAggregate;
    private MotorcycleMapEngineCommand motorcycleMapEngineCommand;
    private BikerAggregate bikerAggregate;
    private BikerAggregate unactiveBikerAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = MotorcycleFixtures.motorcycleAggregate();
        motorcycleMapEngineCommand = MotorcycleFixtures.motorcycleMapEngineCommand();
        bikerAggregate = MotorcycleFixtures.bikerAggregate();
        unactiveBikerAggregate = unactiveBikerAggregate();
    }

    @Test
    void shouldCallMapMethodOnMotorcycleMapEngineCommandPortAndPublishMethodOnMotorcycleMapEnginePublisherPort() {
        var motorcycleId = motorcycleAggregate.id().toString();
        var bikerId = motorcycleAggregate.bikerId().toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId)).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
//        when
        motorcycleMapEngineCommandService.map(motorcycleId, bikerId, motorcycleMapEngineCommand);
//        then
        assertAll(
                () -> motorcycleMapEngineCommandPort.map(motorcycleAggregate),
                () -> motorcycleMapEnginePublisherPort.publish(motorcycleAggregate)
        );
    }

    @Test
    void shouldThrowActiveExceptionExceptionWhenBikerWillBeUnactive() {
        var motorcycleId = motorcycleAggregate.id().toString();
        var bikerId = motorcycleAggregate.bikerId().toString();
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(unactiveBikerAggregate);
//        when + then
        assertThrows(BikerActiveException.class,
                () -> motorcycleMapEngineCommandService.map(motorcycleId, bikerId, motorcycleMapEngineCommand));
    }
}