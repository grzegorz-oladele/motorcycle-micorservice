package pl.grzegorz.motorcycleapplication.services.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.resources.ResourceFilters;
import pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures;
import pl.grzegorz.motorcycleapplication.services.motorcycle.query.MotorcycleListByBikeIdrByIdQueryService;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleListByBikeIdrByIdQueryServiceTest {

    @InjectMocks
    private MotorcycleListByBikeIdrByIdQueryService motorcycleListByBikeIdrByIdQueryService;
    @Mock
    private MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort;

    private MotorcycleListByBikerIdView motorcycleListByBikerIdView;

    @BeforeEach
    void setup() {
        motorcycleListByBikerIdView = motorcycleListByBikerIdView();

    }

    @Test
    void shouldReturnListOfMotorcycles() {
//        given
        var bikerId = BIKER_ID.toString();
        var filter = new ResourceFilters(null, null);
        when(motorcycleListByBikerIdQueryPort.findAllByBiker(bikerId, filter)).thenReturn(motorcycleListByBikerIdView);
//        when
        var motorcyclesByBikerId =
                motorcycleListByBikeIdrByIdQueryService.findAllByBikerId(bikerId, filter);
//        then
        assertAll(
                () -> assertThat(motorcyclesByBikerId.owner().id().toString(), is(bikerId)),
                () -> assertThat(motorcyclesByBikerId.owner().email(), is("test email")),
                () -> assertThat(motorcyclesByBikerId.owner().firstName(), is("test first name")),
                () -> assertThat(motorcyclesByBikerId.owner().username(), is("test username")),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().actualPage(), is(0)),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().pageSize(), is(10)),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().totalPages(), is(1)),
                () -> assertThat(motorcyclesByBikerId.results().pageInfo().totalRecordCount(), is(2L)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).brand(), is("Ducati")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).model(), is("Panigale V4S")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).vintage(), is(2022)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).horsePower(), is(225)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).capacity(), is(1199)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(0).motorcycleClass(), is(MotorcycleClass.SUPERSPORT)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).brand(), is("BMW")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).model(), is("S1000RR")),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).vintage(), is(2021)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).horsePower(), is(208)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).capacity(), is(999)),
                () -> assertThat(motorcyclesByBikerId.results().results().get(1).motorcycleClass(), is(MotorcycleClass.SUPERSPORT))
        );
    }
}