package pl.grzegorz.motorcycleapplication.services.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.services.motorcycle.query.MotorcycleByIdQueryService;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerPermissionException;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleByIdQueryServiceTest {

    @InjectMocks
    private MotorcycleByIdQueryService motorcycleByIdQueryService;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;

    private MotorcycleAggregate motorcycleAggregate;
    private BikerAggregate bikerAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        bikerAggregate = bikerAggregate();
    }

    @Test
    void shouldReturnMotorcycleWhenMotorcycleWillBeExistedInDb() {
//        given
        var motorcycleId = FIRST_MOTORCYCLE_AGGREGATE_ID.toString();
        var bikerId = BIKER_ID.toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId)).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
//        when
        var motorcycleView = motorcycleByIdQueryService.findById(motorcycleId, bikerId);
//        then
        assertAll(
                () -> assertThat(motorcycleAggregate.id(), is(motorcycleView.id())),
                () -> assertThat(motorcycleAggregate.brand(), is(motorcycleView.brand())),
                () -> assertThat(motorcycleAggregate.model(), is(motorcycleView.model())),
                () -> assertThat(motorcycleAggregate.vintage(), is(motorcycleView.vintage())),
                () -> assertThat(motorcycleAggregate.capacity(), is(motorcycleView.capacity())),
                () -> assertThat(motorcycleAggregate.horsePower(), is(motorcycleView.horsePower())),
                () -> assertThat(motorcycleAggregate.bikerId(), is(motorcycleView.owner().id()))
        );
    }

    @Test
    void shouldThrowBikerPermissionExceptionWhenBikerWillHasNotPermissionToMotorcycle() {
//        given
        var motorcycleId = FIRST_MOTORCYCLE_AGGREGATE_ID.toString();
        var bikerId = UUID.randomUUID().toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, bikerId)).thenReturn(motorcycleAggregate);
//        when + then
        assertThrows(BikerPermissionException.class, () -> motorcycleByIdQueryService.findById(motorcycleId, bikerId));
    }
}