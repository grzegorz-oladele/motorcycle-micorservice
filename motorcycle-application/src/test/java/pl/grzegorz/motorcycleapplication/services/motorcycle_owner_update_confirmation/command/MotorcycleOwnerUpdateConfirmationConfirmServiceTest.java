package pl.grzegorz.motorcycleapplication.services.motorcycle_owner_update_confirmation.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleChangeOwnerCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleUpdateOwnerPublisherPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationConfirmCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.query.MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateFinishProcessPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.query.MotorcycleOwnerUpdateStatusByIdQueryPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerActiveException;
import pl.grzegorz.motorcycledomain.exception.ConfirmationProcessAlreadyFinishException;
import pl.grzegorz.motorcycledomain.exception.MotorcycleAlreadyUnactiveException;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateConfirmationConfirmServiceTest {

    @InjectMocks
    private MotorcycleOwnerUpdateConfirmationConfirmService motorcycleOwnerUpdateConfirmationConfirmService;
    @Mock
    private MotorcycleOwnerUpdateConfirmationByConfirmTokenQueryPort confirmationByConfirmTokenQueryPort;
    @Mock
    private MotorcycleOwnerUpdateConfirmationConfirmCommandPort confirmationConfirmCommandPort;
    @Mock
    private MotorcycleOwnerUpdateStatusByIdQueryPort exchangeProcessByProcessIdQueryPort;
    @Mock
    private MotorcycleOwnerUpdateFinishProcessPort exchangeProcessFinishCommandPort;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private MotorcycleChangeOwnerCommandPort motorcycleChangeOwnerCommandPort;
    @Mock
    private MotorcycleUpdateOwnerPublisherPort motorcycleUpdateOwnerPublisherPort;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleAggregate unactiveMotorcycleAggregate;
    private BikerAggregate ownerAggregate;
    private BikerAggregate recipientAggregate;
    private BikerAggregate unactiveRecipientAggregate;
    private MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate;
    private MotorcycleOwnerUpdateConfirmationStatusAggregate finishMotorcycleOwnerUpdateConfirmationStatusAggregate;
    private MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate;


    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        unactiveMotorcycleAggregate = unactiveMotorcycleAggregate();
        ownerAggregate = bikerAggregate();
        recipientAggregate = secondBikerAggregate();
        unactiveRecipientAggregate = unactiveSecondBikerAggregate();
        motorcycleOwnerUpdateConfirmationStatusAggregate = motorcycleOwnerUpdateConfirmationStatusAggregate();
        finishMotorcycleOwnerUpdateConfirmationStatusAggregate = finishMotorcycleOwnerUpdateConfirmationStatusAggregate();
        motorcycleOwnerUpdateStatusAggregate = motorcycleOwnerUpdateStatusAggregate();
    }

    @Test
    void shouldConfirmUpdateMotorcycleOwnerProcessAndExecuteOtherIntegrationProcess() {
//        given
        var confirmationToken = motorcycleOwnerUpdateConfirmationStatusAggregate.confirmationToken();
        when(confirmationByConfirmTokenQueryPort.findByToken(confirmationToken))
                .thenReturn(motorcycleOwnerUpdateConfirmationStatusAggregate);
        when(exchangeProcessByProcessIdQueryPort.findByProcessId(motorcycleOwnerUpdateConfirmationStatusAggregate.exchangeProcessId().toString()))
                .thenReturn(motorcycleOwnerUpdateStatusAggregate);
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleOwnerUpdateStatusAggregate.motorcycleId().toString(),
                motorcycleOwnerUpdateStatusAggregate.ownerTransferorId().toString())).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(motorcycleOwnerUpdateStatusAggregate.recipientId().toString()))
                .thenReturn(recipientAggregate);
//        when
        motorcycleOwnerUpdateConfirmationConfirmService.confirm(confirmationToken);
//        then
        assertAll(
                () -> verify(confirmationConfirmCommandPort).confirm(motorcycleOwnerUpdateConfirmationStatusAggregate),
                () -> verify(exchangeProcessFinishCommandPort).finish(motorcycleOwnerUpdateStatusAggregate),
                () -> verify(motorcycleChangeOwnerCommandPort).changeOwner(motorcycleAggregate),
                () -> verify(motorcycleUpdateOwnerPublisherPort).publish(motorcycleAggregate, motorcycleOwnerUpdateStatusAggregate.ownerTransferorId())
        );
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenRecipientWillBeNotActive() {
//        given
        var confirmationToken = motorcycleOwnerUpdateConfirmationStatusAggregate.confirmationToken();
        when(confirmationByConfirmTokenQueryPort.findByToken(confirmationToken))
                .thenReturn(motorcycleOwnerUpdateConfirmationStatusAggregate);
        when(exchangeProcessByProcessIdQueryPort.findByProcessId(motorcycleOwnerUpdateConfirmationStatusAggregate.exchangeProcessId().toString()))
                .thenReturn(motorcycleOwnerUpdateStatusAggregate);
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleOwnerUpdateStatusAggregate.motorcycleId().toString(),
                motorcycleOwnerUpdateStatusAggregate.ownerTransferorId().toString())).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(motorcycleOwnerUpdateStatusAggregate.recipientId().toString()))
                .thenReturn(unactiveRecipientAggregate);
//        when + then
        assertThrows(BikerActiveException.class,
                () -> motorcycleOwnerUpdateConfirmationConfirmService.confirm(confirmationToken));
    }

    @Test
    void shouldThrowMotorcycleActiveExceptionWhenMotorcycleWillBeNotActive() {
//        given
        var confirmationToken = motorcycleOwnerUpdateConfirmationStatusAggregate.confirmationToken();
        when(confirmationByConfirmTokenQueryPort.findByToken(confirmationToken))
                .thenReturn(motorcycleOwnerUpdateConfirmationStatusAggregate);
        when(exchangeProcessByProcessIdQueryPort.findByProcessId(motorcycleOwnerUpdateConfirmationStatusAggregate.exchangeProcessId().toString()))
                .thenReturn(motorcycleOwnerUpdateStatusAggregate);
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleOwnerUpdateStatusAggregate.motorcycleId().toString(),
                motorcycleOwnerUpdateStatusAggregate.ownerTransferorId().toString())).thenReturn(unactiveMotorcycleAggregate);
        when(bikerByIdQueryPort.getById(motorcycleOwnerUpdateStatusAggregate.recipientId().toString()))
                .thenReturn(recipientAggregate);
//        when + then
        assertThrows(MotorcycleAlreadyUnactiveException.class,
                () -> motorcycleOwnerUpdateConfirmationConfirmService.confirm(confirmationToken));
    }

    @Test
    void shouldThrowConfirmationProcessAlreadyFinishExceptionWhenUpdatingProcessWillBeFinish() {
//        given
        var confirmationToken = motorcycleOwnerUpdateConfirmationStatusAggregate.confirmationToken();
        when(confirmationByConfirmTokenQueryPort.findByToken(confirmationToken))
                .thenReturn(finishMotorcycleOwnerUpdateConfirmationStatusAggregate);
        when(exchangeProcessByProcessIdQueryPort.findByProcessId(motorcycleOwnerUpdateConfirmationStatusAggregate.exchangeProcessId().toString()))
                .thenReturn(motorcycleOwnerUpdateStatusAggregate);
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleOwnerUpdateStatusAggregate.motorcycleId().toString(),
                motorcycleOwnerUpdateStatusAggregate.ownerTransferorId().toString())).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(motorcycleOwnerUpdateStatusAggregate.recipientId().toString()))
                .thenReturn(recipientAggregate);
//        when + then
        assertThrows(ConfirmationProcessAlreadyFinishException.class,
                () -> motorcycleOwnerUpdateConfirmationConfirmService.confirm(confirmationToken));
    }
}