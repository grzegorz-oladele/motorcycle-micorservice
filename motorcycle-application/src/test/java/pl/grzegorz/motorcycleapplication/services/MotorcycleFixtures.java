package pl.grzegorz.motorcycleapplication.services;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleCreateCommandUseCase.MotorcycleCreateCommand;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleMapEngineUseCase.MotorcycleMapEngineCommand;
import pl.grzegorz.motorcycleapplication.ports.view.BikerSimplifiedView;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleListByBikerIdView;
import pl.grzegorz.motorcycleapplication.ports.view.MotorcycleView;
import pl.grzegorz.motorcycleapplication.ports.view.SimplifiedMotorcycleView;
import pl.grzegorz.motorcycleapplication.resources.PageInfo;
import pl.grzegorz.motorcycleapplication.resources.ResultViewInfo;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;
import pl.grzegorz.motorcycledomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MotorcycleFixtures {

    public static final UUID FIRST_MOTORCYCLE_AGGREGATE_ID = UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6");
    public static final UUID SECOND_MOTORCYCLE_AGGREGATE_ID = UUID.fromString("ac9d82be-2571-42bf-b380-3aea6b2223bb");
    public static final UUID BIKER_ID = UUID.fromString("0ba31ff3-8fd4-4c8b-9c56-9d3c938b24b4");
    public static final UUID UNACTIVE_BIKER_ID = UUID.fromString("725a6648-0fae-48bc-994e-e8ef753ecc65");
    private static final UUID SECOND_BIKER_ID = UUID.fromString("395a0b0d-9203-46db-b241-c254e0823638");
    private static final UUID CONFIRMATION_STATUS_ID = UUID.fromString("3716dfe6-c443-46af-a2b7-3ed2c2027738");
    private static final UUID EXCHANGE_PROCESS_ID = UUID.fromString("935374ad-6863-4d46-96f3-353fe1c15af2");
    private static final UUID OWNER_UPDATE_STATUS_ID = UUID.fromString("d381310b-a7f1-402f-a4b9-eb3a9f6adff3");

    public static MotorcycleCreateCommand createCommand() {
        return MotorcycleCreateCommand.builder()
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(Short.parseShort("999"))
                .withVintage(Short.parseShort("2022"))
                .withHorsePower(Short.parseShort("225"))
                .withMotorcycleClass("supersport")
                .build();
    }

    public static MotorcycleCreateCommand createCommandWithUnactiveBiker() {
        return MotorcycleCreateCommand.builder()
                .withBikerId(UNACTIVE_BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(Short.parseShort("999"))
                .withVintage(Short.parseShort("2022"))
                .withHorsePower(Short.parseShort("225"))
                .withMotorcycleClass("supersport")
                .build();
    }

    public static MotorcycleCreateCommand createCommandWithInvalidMotorcycleClass() {
        return MotorcycleCreateCommand.builder()
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(Short.parseShort("999"))
                .withVintage(Short.parseShort("2022"))
                .withHorsePower(Short.parseShort("225"))
                .withMotorcycleClass("xyz")
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBikerId(BIKER_ID)
                .withModel("Ducati")
                .withBrand("Panigale V4S")
                .withCapacity(1999)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static MotorcycleAggregate unactiveMotorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(UUID.fromString("4a1e3094-69f8-4307-9d52-49b4141d4bc6"))
                .withBikerId(BIKER_ID)
                .withModel("Ducati")
                .withBrand("Panigale V4S")
                .withCapacity(1999)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static ResultViewInfo<MotorcycleView> motorcyclePage() {
        return ResultViewInfo.<MotorcycleView>builder()
                .pageInfo(PageInfo.builder()
                        .totalRecordCount(2)
                        .totalPages(1)
                        .pageSize(10)
                        .actualPage(0)
                        .build())
                .results(Arrays.asList(firstMotorcycleView(), secondMotorcycleView()))
                .build();
    }

    private static MotorcycleView firstMotorcycleView() {
        return MotorcycleView.builder()
                .withId(FIRST_MOTORCYCLE_AGGREGATE_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    private static MotorcycleView secondMotorcycleView() {
        return MotorcycleView.builder()
                .withId(FIRST_MOTORCYCLE_AGGREGATE_ID)
                .withBrand("BMW")
                .withModel("S1000RR")
                .withCapacity(999)
                .withHorsePower(208)
                .withVintage(2021)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    private static SimplifiedMotorcycleView firstMotorcycle() {
        return SimplifiedMotorcycleView.builder()
                .withId(FIRST_MOTORCYCLE_AGGREGATE_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    private static SimplifiedMotorcycleView secondMotorcycle() {
        return SimplifiedMotorcycleView.builder()
                .withId(SECOND_MOTORCYCLE_AGGREGATE_ID)
                .withBrand("BMW")
                .withModel("S1000RR")
                .withCapacity(999)
                .withHorsePower(208)
                .withVintage(2021)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .build();
    }

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withEmail("test email")
                .withFirstName("test first name")
                .withUserName("test username")
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static BikerAggregate unactiveOwnerBikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withEmail("test email")
                .withFirstName("test first name")
                .withUserName("test username")
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerAggregate secondBikerAggregate() {
        return BikerAggregate.builder()
                .withId(SECOND_BIKER_ID)
                .withEmail("test 2email")
                .withFirstName("test 2first name")
                .withUserName("test 2username")
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static BikerAggregate unactiveSecondBikerAggregate() {
        return BikerAggregate.builder()
                .withId(SECOND_BIKER_ID)
                .withEmail("test 2email")
                .withFirstName("test 2first name")
                .withUserName("test 2username")
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerAggregate unactiveBikerAggregate() {
        return BikerAggregate.builder()
                .withId(UNACTIVE_BIKER_ID)
                .withEmail("uanctive test email")
                .withFirstName("unactive test first name")
                .withUserName("unactive test username")
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerSimplifiedView bikerSimplifiedView() {
        return BikerSimplifiedView.toView(bikerAggregate());
    }

    public static MotorcycleListByBikerIdView motorcycleListByBikerIdView() {
        return MotorcycleListByBikerIdView.builder()
                .withOwner(bikerSimplifiedView())
                .withResults(toResultInfo())
                .build();
    }

    private static ResultViewInfo<SimplifiedMotorcycleView> toResultInfo() {
        return ResultViewInfo.<SimplifiedMotorcycleView>builder()
                .pageInfo(toPageInfo())
                .results(List.of(firstMotorcycle(), secondMotorcycle()))
                .build();
    }

    private static PageInfo toPageInfo() {
        return PageInfo.builder()
                .pageSize(10)
                .actualPage(0)
                .totalRecordCount(2)
                .totalPages(1)
                .build();
    }

    public static MotorcycleMapEngineCommand motorcycleMapEngineCommand() {
        return MotorcycleMapEngineCommand.builder()
                .withCapacity(599)
                .withHorsePower(123)
                .build();
    }

    public static MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate() {
        return MotorcycleOwnerUpdateConfirmationStatusAggregate.builder()
                .withId(CONFIRMATION_STATUS_ID)
                .withExchangeProcessId(EXCHANGE_PROCESS_ID)
                .withRecipientId(SECOND_BIKER_ID)
                .withIsConfirm(Boolean.FALSE)
                .withConfirmationToken(UUID.randomUUID().toString())
                .build();
    }

    public static MotorcycleOwnerUpdateConfirmationStatusAggregate finishMotorcycleOwnerUpdateConfirmationStatusAggregate() {
        return MotorcycleOwnerUpdateConfirmationStatusAggregate.builder()
                .withId(CONFIRMATION_STATUS_ID)
                .withExchangeProcessId(EXCHANGE_PROCESS_ID)
                .withRecipientId(SECOND_BIKER_ID)
                .withIsConfirm(Boolean.TRUE)
                .withConfirmationToken(UUID.randomUUID().toString())
                .build();
    }

    public static MotorcycleOwnerUpdateStatusAggregate motorcycleOwnerUpdateStatusAggregate() {
        return MotorcycleOwnerUpdateStatusAggregate.builder()
                .withId(OWNER_UPDATE_STATUS_ID)
                .withMotorcycleId(FIRST_MOTORCYCLE_AGGREGATE_ID)
                .withOwnerTransferorId(BIKER_ID)
                .withRecipientId(SECOND_BIKER_ID)
                .withProcessCompleted(Boolean.FALSE)
                .build();
    }
}