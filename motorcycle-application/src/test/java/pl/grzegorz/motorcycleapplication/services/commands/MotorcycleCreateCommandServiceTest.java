package pl.grzegorz.motorcycleapplication.services.commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.in.motorcycles.commands.MotorcycleCreateCommandUseCase.MotorcycleCreateCommand;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.message.MotorcycleCreatePublisherPort;
import pl.grzegorz.motorcycleapplication.services.motorcycle.commands.MotorcycleCreateCommandService;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerActiveException;
import pl.grzegorz.motorcycledomain.exception.InvalidMotorcycleClassException;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleCreateCommandServiceTest {

    @InjectMocks
    private MotorcycleCreateCommandService motorcycleCreateCommandService;
    @Mock
    private pl.grzegorz.motorcycleapplication.ports.out.motorcycle.commands.MotorcycleCreateCommandPort motorcycleCreateCommandPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private MotorcycleCreatePublisherPort motorcycleCreatePublisherPort;

    private MotorcycleCreateCommand createCommand;
    private MotorcycleCreateCommand createCommandWithUnactiveBiker;
    private MotorcycleCreateCommand createCommandWithInvalidMotorcycleCLass;
    private BikerAggregate bikerAggregate;
    private BikerAggregate unactiveBikerAggregate;

    @BeforeEach
    void setup() {
        createCommand = createCommand();
        createCommandWithUnactiveBiker = createCommandWithUnactiveBiker();
        createCommandWithInvalidMotorcycleCLass = createCommandWithInvalidMotorcycleClass();
        bikerAggregate = bikerAggregate();
        unactiveBikerAggregate = unactiveBikerAggregate();
    }

    @Test
    void shouldCallSaveMethodAndReturnId() {
//        given
        var bikerId = BIKER_ID.toString();
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
//        when
        motorcycleCreateCommandService.create(createCommand);
//        then
        assertAll(
                () -> verify(motorcycleCreateCommandPort).save((any(MotorcycleAggregate.class))),
                () -> verify(motorcycleCreatePublisherPort).publish(any(MotorcycleAggregate.class))
        );
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenBikerWillBeNotActive() {
//        given
        when(bikerByIdQueryPort.getById(UNACTIVE_BIKER_ID.toString())).thenReturn(unactiveBikerAggregate);
//        when + then
        assertThrows(BikerActiveException.class,
                () -> motorcycleCreateCommandService.create(createCommandWithUnactiveBiker));
    }

    @Test
    void shouldThrowInvalidMotorcycleClassExceptionWhenMotorcycleClassWillBeNotCorrect() {
//        given
        when(bikerByIdQueryPort.getById(BIKER_ID.toString())).thenReturn(bikerAggregate);
//        when + then
        assertThrows(InvalidMotorcycleClassException.class,
                () -> motorcycleCreateCommandService.create(createCommandWithInvalidMotorcycleCLass));
    }
}