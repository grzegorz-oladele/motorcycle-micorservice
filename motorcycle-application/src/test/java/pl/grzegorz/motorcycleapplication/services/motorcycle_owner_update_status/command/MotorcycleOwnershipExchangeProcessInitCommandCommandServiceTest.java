package pl.grzegorz.motorcycleapplication.services.motorcycle_owner_update_status.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.motorcycleapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_confirmation.comand.MotorcycleOwnerUpdateConfirmationCreateCommandPort;
import pl.grzegorz.motorcycleapplication.ports.out.motorcycle_owner_update_status.command.MotorcycleOwnerUpdateInitProcessPort;
import pl.grzegorz.motorcycledomain.aggregates.BikerAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateConfirmationStatusAggregate;
import pl.grzegorz.motorcycledomain.aggregates.MotorcycleOwnerUpdateStatusAggregate;
import pl.grzegorz.motorcycledomain.exception.BikerActiveException;
import pl.grzegorz.motorcycledomain.exception.BikerAlreadyOwnerException;
import pl.grzegorz.motorcycledomain.exception.BikerPermissionException;
import pl.grzegorz.motorcycledomain.exception.MotorcycleAlreadyUnactiveException;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.motorcycleapplication.services.MotorcycleFixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnershipExchangeProcessInitCommandCommandServiceTest {

    @InjectMocks
    private MotorcycleOwnerUpdateInitProcessService motorcycleOwnershipExchangeProcessInitCommandCommandService;
    @Mock
    private MotorcycleOwnerUpdateInitProcessPort motorcycleOwnerUpdateInitProcessPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private MotorcycleOwnerUpdateConfirmationCreateCommandPort motorcycleOwnerUpdateConfirmationCreateCommandPort;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleAggregate unactiveMotorcycleAggregate;
    private BikerAggregate ownerBikerAggregate;
    private BikerAggregate unactiveOwnerBikerAggregate;
    private BikerAggregate recipientBikerAggregate;
    private BikerAggregate unactiveRecipientBikerAggregate;
    private MotorcycleOwnerUpdateConfirmationStatusAggregate motorcycleOwnerUpdateConfirmationStatusAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        unactiveMotorcycleAggregate = unactiveMotorcycleAggregate();
        ownerBikerAggregate = bikerAggregate();
        recipientBikerAggregate = secondBikerAggregate();
        unactiveOwnerBikerAggregate = unactiveOwnerBikerAggregate();
        unactiveRecipientBikerAggregate = unactiveSecondBikerAggregate();
        motorcycleOwnerUpdateConfirmationStatusAggregate = motorcycleOwnerUpdateConfirmationStatusAggregate();
    }

    @Test
    @Disabled
    void shouldCallInitMethodOnMotorcycleOwnershipExchangeProcessInitCommandPortInterface() {
//        given
        var motorcycleId = motorcycleAggregate.id().toString();
        var ownerId = ownerBikerAggregate.id().toString();
        var recipientId = recipientBikerAggregate.id().toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId)).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(ownerId)).thenReturn(ownerBikerAggregate);
        when(bikerByIdQueryPort.getById(recipientId)).thenReturn(recipientBikerAggregate);
        when(motorcycleOwnerUpdateConfirmationCreateCommandPort.create(any(), UUID.fromString(recipientId)))
                .thenReturn(motorcycleOwnerUpdateConfirmationStatusAggregate);
//        when
        motorcycleOwnershipExchangeProcessInitCommandCommandService.init(motorcycleId, ownerId, recipientId);
//        then
        verify(motorcycleOwnerUpdateInitProcessPort).init(any(MotorcycleOwnerUpdateStatusAggregate.class));
    }

    @Test
    void shouldThrowMotorcycleUnactiveExceptionWhenMotorcycleWillBeUnactive() {
//        given
        var motorcycleId = unactiveMotorcycleAggregate.id().toString();
        var ownerId = ownerBikerAggregate.id().toString();
        var recipientId = recipientBikerAggregate.id().toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId)).thenReturn(unactiveMotorcycleAggregate);
//        when + then
        assertThrows(MotorcycleAlreadyUnactiveException.class,
                () -> motorcycleOwnershipExchangeProcessInitCommandCommandService.init(motorcycleId, ownerId, recipientId));
    }

    @Test
    void shouldThrowBikerPermissionExceptionWhenOwnerWillHasNotPermissionToMotorcycle() {
//        given
        var motorcycleId = motorcycleAggregate.id().toString();
        var recipientId = recipientBikerAggregate.id().toString();
        var ownerId = recipientId;
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId)).thenReturn(motorcycleAggregate);
//        when + then
        assertThrows(BikerPermissionException.class,
                () -> motorcycleOwnershipExchangeProcessInitCommandCommandService.init(motorcycleId, recipientId, recipientId));
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenOwnerOfTheMotorcycleWillBeNotActive() {
//        given
        var motorcycleId = motorcycleAggregate.id().toString();
        var ownerId = unactiveOwnerBikerAggregate.id().toString();
        var recipientId = recipientBikerAggregate.id().toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId)).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(ownerId)).thenReturn(unactiveOwnerBikerAggregate);
        assertThrows(BikerActiveException.class,
                () -> motorcycleOwnershipExchangeProcessInitCommandCommandService.init(motorcycleId, ownerId, recipientId));
    }

    @Test
    void shouldThrowBikerAlreadyOwnerExceptionWhenRecipientWillBeOwnerOfMotorcycle() {
//        given
        var motorcycleId = motorcycleAggregate.id().toString();
        var ownerId = ownerBikerAggregate.id().toString();
        var recipientId = ownerId;
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId)).thenReturn(motorcycleAggregate);
//        when + then
        assertThrows(BikerAlreadyOwnerException.class,
                () -> motorcycleOwnershipExchangeProcessInitCommandCommandService.init(motorcycleId, ownerId, recipientId));
    }

    @Test
    void shouldThrowBikerActiveExceptionWhenRecipientWillBeNotActive() {
//        given
        var motorcycleId = motorcycleAggregate.id().toString();
        var ownerId = ownerBikerAggregate.id().toString();
        var recipientId = unactiveRecipientBikerAggregate.id().toString();
        when(motorcycleByIdQueryPort.getMotorcycleById(motorcycleId, ownerId)).thenReturn(motorcycleAggregate);
        when(bikerByIdQueryPort.getById(ownerId)).thenReturn(ownerBikerAggregate);
        when(bikerByIdQueryPort.getById(recipientId)).thenReturn(unactiveRecipientBikerAggregate);
//        when + then
        assertThrows(BikerActiveException.class,
                () -> motorcycleOwnershipExchangeProcessInitCommandCommandService.init(motorcycleId, ownerId, recipientId));
    }
}